//
// Created by Ruben Götz on 20.12.2021.
//

#include "SCC_compute.h"
#include "Stack.h"
//#include "../unit_tests/Logs.h"
#include "iostream"
#include <vector>
#include <map>
using namespace std;

SCC_compute::SCC_compute(Graph_Instance &instance)
                :   ids(instance.graph.size(),-1), marked(instance.graph.size(), 0),
                    lows(instance.graph.size(), 0), id(0), scc_count(0), amount(instance.graph.size()),
                    graph(instance.graph) {}

//berechnet SCC und gibt Knotenfärbung in coloration zurück
void SCC_compute::getSCCcoloration(vector<int> &coloration){
    //++Logs::getSCCcoloration;
    for (int v = 0; v < amount; ++v) {
        if(ids[v] == -1) dfs(v, coloration);
    }
}

void SCC_compute::dfs(int v, vector<int> &coloration){
    stack.push_back(v);
    marked[v] = 1;
    ids[v] = id;
    lows[v] = id++;
    // u ist ausgehender Nachbar von v: (v,u)
    for ( int u : graph[v] ) {
        if ( ids[u] == -1 ) {
            dfs(u, coloration);
            lows[v] = min(lows[v], lows[u]);
        }
        else if ( marked[u] ) lows[v] = min(lows[v], ids[u]);
    }
    if(ids[v] == lows[v]){
        while (1){
            int node = stack.back();
            marked[node] = 0;
            // lows[node] = ids[v];
            coloration[node] = scc_count;
            stack.pop_back();
            if (node == v) break;
        }
        ++scc_count;
    }
}

void SCC_compute::callback(Stack &context, vector<int> &visited, vector<int> &coloration) {
    int v = get<0>(context.get());
    int u = get<1>(context.get());
    if ( marked[u] ) coloration[v] = min(coloration[u],coloration[v]);
    context.pop();
}

void SCC_compute::visit(int v, vector<int> &visited, vector<int> &coloration) {
    stack.push_back(v);
    marked[v] = 1;
    ids[v] = id;
    coloration[v] = id++;

    visited[v] = 1;
}

void SCC_compute::all_children_visited(Stack &context, vector<int> &coloration) {
    int v = get<1>(context.get());

    if ( ids[v] == coloration[v] ) {
        while ( true ){
            int node = stack.back();
            marked[node] = 0;
            coloration[node] = ids[v];
            stack.pop_back();
            if (node == v) break;
        }
    }
    if (get<0>(context.get()) == -1) context.pop();
}

//returns amount of colors in coloration, renames colors to increasing integers starting from 0
int SCC_compute::clean_coloration(vector<int> &coloration){
    map<int,int> cleaned_colors;
    int color_count = 0;
    for (int i = 0; i < coloration.size(); ++i) {
        if ( cleaned_colors.count(coloration[i]) == 0 )
            cleaned_colors.insert({coloration[i],color_count++});
        coloration[i] = cleaned_colors.at(coloration[i]);
    }
    return color_count;
}