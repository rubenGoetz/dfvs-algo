//
// Created by Ruben Götz on 19.01.2022.
//

#include "Graph_reduce.h"
#include "Vertex_op.h"
#include <vector>
#include <ctime>
#include <chrono>
#include <stack>

#ifndef DFVS_BRANCH_AND_BOUND_H
#define DFVS_BRANCH_AND_BOUND_H

using namespace std;

namespace Branch_and_bound {
    void solve(vector<int> current_dfvs, Graph_Instance instance,
               vector<int> &upper_bound, chrono::steady_clock::time_point &timeout);
    void solve(vector<int> current_dfvs, Graph_Instance instance,
            vector<int> &upper_bound);
    int get_branching_index(Graph_Instance &instance);
    int in_times_out(Graph_Instance &instance);
    int in_times_out(vector<int> &alive,Graph_Instance &instance);
    int get_highest_degree_vertex(Graph_Instance &instance);
    int get_highest_degree_vertex(vector<int> &alive, Graph_Instance &instance);
    int get_branching_index(Graph_Instance &instance);
    int get_branching_index(vector<int> &alive, Graph_Instance &instance);
    void append_vector(vector<int> *v1, vector<int> *v2);

    void initial_solution(Graph_Instance &instance, vector<int> &dfvs);
    void solve_greedy(Graph_Instance &instance, vector<int> &dfvs, chrono::steady_clock::time_point &timeout);
    void solve_greedy(Graph_Instance &instance, vector<int> &dfvs);

    int is_acyclic(vector<int> &alive, Graph_Instance &instance);
    int is_acyclic_util(int v, vector<int> &visited, vector<int> &s,vector<int> on_stack , vector<int> &alive,
                        Graph_Instance &instance);
};


#endif //DFVS_BRANCH_AND_BOUND_H
