#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <numeric>
#include <iterator>
#include <assert.h>
#include <queue>
#include <csignal>

using namespace std;

int isin(vector<int> &vertex, int e) {
    assert( is_sorted(begin(vertex),end(vertex)) );
    return binary_search(begin(vertex),end(vertex),e);
}

// --------------------
// Structs
// --------------------

struct Graph_Instance {
    int N;
    vector<vector<int>> graph;
    vector<int> map;
    Graph_Instance(int n) : graph(n), N(n) {};
};

struct DSF {
    vector<int> parent;
    DSF(int n) : parent(n) { iota(begin(parent),end(parent),0); }
    void join(int u, int v) { parent[find(u)] = find(v); }
    int find(int v) {
        if(parent[v] == v) return v;
        return parent[v] = find(parent[v]);
    }
};

class Stack {
private:
    vector<tuple<int,int>> stack;
    int top;
public:
    Stack(int max_size);
    tuple<int,int> pop();
    tuple<int,int> get();
    void push(tuple<int,int> t);
    int not_empty();
};

class SCC_compute {
private:
    vector<vector<int>> graph;
    int amount;
    int id;
    vector<int> lows;
    vector<int> ids;
    vector<int> marked;
    vector<int> stack;

public:
    int scc_count;
    SCC_compute(Graph_Instance &instance);
    void getSCCcoloration(vector<int> &coloration);
    void dfs(int v, vector<int> &coloration);
    void visit(int v, vector<int> &visited,vector<int> &coloration);
    void all_children_visited(Stack &context, vector<int> &coloration);
    void callback(Stack &context, vector<int> &visited,vector<int> &coloration);
    static int clean_coloration(vector<int> &coloration);
};

// --------------------
// Vertex_op
// --------------------

void delete_vertex(int v, Graph_Instance &instance) {
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
    instance.graph.erase(instance.graph.begin() + v);
    if ( !instance.map.empty() ) instance.map.erase(instance.map.begin() + v);
    --instance.N;
    for (int i = 0; i < instance.graph.size(); ++i) {
        auto &curr_vertex = instance.graph[i];
        if ( curr_vertex.empty() ) continue;
        // find first element >= v
        auto it = lower_bound(begin(curr_vertex), end(curr_vertex),v);
        //get_pos(v,begin(curr_vertex), end(curr_vertex));
        if ( it == end(curr_vertex) ) continue;
        if ( *it == v ) it = curr_vertex.erase(it);
        while ( it != end(curr_vertex) ) --(*it++);
    }
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
}

void delete_vertex_set(vector<int> &set, Graph_Instance &instance) {
    //++Logs::delete_vertex_set;
    assert(is_sorted(begin(set),end(set)));
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));

    if ( instance.N <= 0 ) return;
    if ( set.size() <= 0 ) return;

    vector<int> alive(instance.N,1);
    for (int i = 0; i < set.size(); ++i)
        alive[set[i]] = 0;

    vector pref(instance.N+1, 0); // pref[i] is the num of alive nodes <i
    partial_sum(begin(alive),end(alive),begin(pref)+1);

    vector<vector<int>> new_graph(instance.N - set.size());
    for (int i = 0; i < instance.N; ++i) {
        if ( !alive[i] ) continue;
        auto &v = instance.graph[i];
        for (int j = 0; j < v.size(); ++j) {
            if ( !alive[v[j]] ) continue;
            new_graph[pref[i]].push_back(pref[v[j]]);
        }
    }

    if ( instance.map.size() > 0 ) {
        vector<int> new_map;
        for (int i = 0; i < instance.N; ++i)
            if (alive[i]) new_map.push_back(instance.map[i]);

        instance.map = new_map;
    }

    instance.graph = new_graph;
    instance.N -= set.size();

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
}

void merge_vertices(const vector<pair<int, int>> &merges, Graph_Instance &instance) {
    //++Logs::merge_vertices_set;
    DSF co(instance.N);
    for(auto [u,v] : merges) co.join(u,v);
    // calc new ids
    vector alive(instance.N,0);
    for(int i=0; i<instance.N; ++i)
        alive[i] = (co.find(i)==i);
    vector pref(instance.N+1, 0); // pref[i] is the num of alive nodes <i
    partial_sum(begin(alive),end(alive),begin(pref)+1);

    // rebuild edges
    // build vector with all edges
    vector<pair<int,int>> edges;
    for(int v=0; v<instance.N; ++v)
        for(auto u : instance.graph[v])
            edges.emplace_back(pref[co.find(v)],pref[co.find(u)]);

    // sort vector
    sort(begin(edges),end(edges)); // TODO maybe c-sort
    // remove duplicates
    edges.erase(unique(begin(edges),end(edges)),end(edges));

    // build adj list again
    auto newN = pref.back();
    vector<vector<int>> adj(newN);
    for(auto [u,v] : edges)
        adj[u].push_back(v);
    instance.graph = adj;

    // update map
    vector<int> new_map;
    for(int i=0; i<instance.N; ++i)
        if(alive[i])
            new_map.push_back(instance.map[i]);
    instance.map = new_map;
    instance.N = newN;

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
}

void merge(vector<int> &v1, vector<int> &v2) {
    auto v1_original = v1;
    auto v2_original = v2;
    assert(is_sorted(begin(v1),end(v1)));
    assert(is_sorted(begin(v2),end(v2)));

    vector<int> new_v(v1.size() + v2.size());
    auto it1 = begin(v1);
    auto it2 = begin(v2);
    for (int i = 0; i < new_v.size(); ++i) {
        if ( it1 == end(v1) ) new_v[i] = *(it2++);
        else if ( it2 == end(v2) ) new_v[i] = *(it1++);

        else if ( *it1 > *it2 ) new_v[i] = *(it2++);
        else if ( *it1 < *it2 ) new_v[i] = *(it1++);

        else {      // does not generate parallel edges
            new_v[i] = *(it1++);
            ++it2;
            new_v.resize(new_v.size() - 1);
        }
    }

    v1 = new_v;

    assert(is_sorted(begin(v1),end(v1)));
    assert(unique(begin(v1), end(v1)) == end(v1));
    for ( int i : v1_original ) assert(isin(v1,i));
    for ( int i : v2_original ) assert(isin(v1,i));
    assert(v2 == v2_original);
}

void replace_vertex(int v, Graph_Instance &instance) {
    //++Logs::replace_vertex;
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    vector<int> predecessors(0);
    // find every predecessor
    for (int i = 0; i < instance.graph.size(); ++i) {
        auto it = begin(instance.graph[i]);
        while ( it != end(instance.graph[i]) ) {
            if ( *it == v ) {
                predecessors.push_back(i);
                it = instance.graph[i].erase(it);
            }
            else {
                if ( *it > v ) --(*it);
                ++it;
            }
        }
    }

    // add every successor to predecessor
    vector<int> &successors = instance.graph[v];
    for ( int p : predecessors ) merge(instance.graph[p],successors);

    // delete v, no selfloops allowed
    instance.graph.erase(begin(instance.graph) + v);
    if (!instance.map.empty()) instance.map.erase(begin(instance.map) + v);
    --instance.N;

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
}

// --------------------
// SCC
// --------------------
Stack::Stack(int max_size) {
    vector<tuple<int,int>> s(max_size);
    stack = s;
    top = -1;
}

tuple<int,int> Stack::pop() {
    --top;
    return stack[top + 1];
}

tuple<int,int> Stack::get() {
    return stack[top];
}

void Stack::push(tuple<int,int> t) {
    ++top;
    stack[top] = t;
}

int Stack::not_empty() {
    return top >= 0;
}

SCC_compute::SCC_compute(Graph_Instance &instance)
        :   ids(instance.graph.size(),-1), marked(instance.graph.size(), 0),
            lows(instance.graph.size(), 0), id(0), scc_count(0), amount(instance.graph.size()),
            graph(instance.graph) {}

//berechnet SCC und gibt Knotenfärbung in coloration zurück
void SCC_compute::getSCCcoloration(vector<int> &coloration){
    //++Logs::getSCCcoloration;
    for (int v = 0; v < amount; ++v) {
        if(ids[v] == -1) dfs(v, coloration);
    }
}

void SCC_compute::dfs(int v, vector<int> &coloration){
    stack.push_back(v);
    marked[v] = 1;
    ids[v] = id;
    lows[v] = id++;
    // u ist ausgehender Nachbar von v: (v,u)
    for ( int u : graph[v] ) {
        if ( ids[u] == -1 ) {
            dfs(u, coloration);
            lows[v] = min(lows[v], lows[u]);
        }
        else if ( marked[u] ) lows[v] = min(lows[v], ids[u]);
    }
    if(ids[v] == lows[v]){
        while (1){
            int node = stack.back();
            marked[node] = 0;
            // lows[node] = ids[v];
            coloration[node] = scc_count;
            stack.pop_back();
            if (node == v) break;
        }
        ++scc_count;
    }
}

// --------------------
// Graph_reduce
// --------------------

// set & subset have to be sorted
int is_subset(vector<int> &set, vector<int> &subset) {
    assert( is_sorted(begin(set),end(set)) );
    assert( is_sorted(begin(subset),end(subset)) );
    if ( subset.size() > set.size() ) return 0;
    if ( subset.empty() ) return 1;
    auto sub_it = begin(subset);
    auto set_it = begin(set);
    while ( set_it != end(set) ) {
        if ( *sub_it == *set_it++ ) ++sub_it;
        if ( sub_it == end(subset) ) return 1;
    }
    return 0;
}

int is_clique(int v, Graph_Instance &g_PIE) {
    //++Logs::is_clique;
    for ( auto &vertex : g_PIE.graph ) assert(is_sorted(begin(vertex), end(vertex)));

    vector<int> &neighbors = g_PIE.graph[v];

    for ( int u : neighbors ) {
        // inserts u into a copy of graph[u]
        vector<int> set(g_PIE.graph[u].size() + 1);
        int inserted = 0;
        for (int i = 0; i < set.size(); ++i) {
            if ( !inserted ) {
                if ( i >= (set.size() - 1) || g_PIE.graph[u][i] >= u ) {
                    set[i] = u;
                    inserted = 1;
                }
                else set[i] = g_PIE.graph[u][i];
            }
            else set[i] = g_PIE.graph[u][i - 1];
        }

        if ( !is_subset(set,neighbors) ) return 0;
    }

    return 1;
}

int isin_util(vector<int> &v, int low, int high, int e) {
    if ( v[low] == e || v[high] == e ) return 1;
    int mid = (low + high) / 2;
    if ( mid <= low || mid >= high ) return 0;
    if ( v[mid] >= e ) return isin_util(v,low,mid,e);
    else return isin_util(v,mid,high,e);
}

void remove_duplicates(vector<int> &v) {
    //++Logs::remove_duplicates;
    sort(v.begin(),v.end());
    // way more elegant solution by Chris
    v.erase(unique(begin(v),end(v)),end(v));
}

void calc_common_neighbor(Graph_Instance &undirected, vector<pair<int,int>> &edges,
                                        vector<int> &comm_neigh) {
    comm_neigh = vector<int>(0);

    for ( auto [i,j] : edges ) {
        auto iit = begin(undirected.graph[i]);
        auto jit = begin(undirected.graph[j]);
        int amount = 0;
        while ( iit != end(undirected.graph[i]) && jit != end(undirected.graph[j]) ) {
            if ( *iit == * jit ) {
                ++amount;
                ++iit;
                ++jit;
            }
            else if ( *iit < *jit ) ++iit;
            else ++jit;
        }
        comm_neigh.push_back(amount);
    }
}

// i < j
void update_node_list(vector<vector<int>> &node_list, Graph_Instance &undirected, int i, int j) {
    int iindex = -1;
    int jindex = -1;
    for (int k = 0; k < node_list.size(); ++k) {
        if ( binary_search(begin(node_list[k]),end(node_list[k]),i) ) iindex = k;
        if ( binary_search(begin(node_list[k]),end(node_list[k]),j) ) jindex = k;
        //if ( node_list[k][0] == i ) iindex = k;
        //if ( node_list[k][0] == j ) jindex = k;
    }
    assert ( iindex != -1 );
    assert ( jindex != -1 );
    merge(node_list[iindex],node_list[jindex]);
    node_list.erase(begin(node_list) + jindex);
    for ( auto &v : undirected.graph ) {
        v.erase(remove(begin(v), end(v),j),end(v));
    }
}

void update_edges(vector<pair<int,int>> &edges, int index) {
    int i = edges[index].first;
    int j = edges[index].second;
    auto it = begin(edges);
    while ( it != end(edges) ) {
        int u = (*it).first;
        int v = (*it).second;
        if      ( v == j || u == j ) it = edges.erase(it);
        else if ( u == i &&
                  !binary_search(edges.begin(), edges.end(), pair<int,int>(v,j)) &&
                  !binary_search(edges.begin(), edges.end(), pair<int,int>(j,v)) ) it = edges.erase(it);
        else if ( v == i &&
                  !binary_search(edges.begin(), edges.end(), pair<int,int>(u,j)) &&
                  !binary_search(edges.begin(), edges.end(), pair<int,int>(j,u)) ) it = edges.erase(it);
        else ++it;
    }
    for ( auto e : edges ) assert ( e.first != j && e.second != j );
}

// https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=1270207
int remove_max_clique(Graph_Instance &instance, Graph_Instance &g_PIE) {
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    // 0 - build data-structures
    // =========================
    for ( auto &v : g_PIE.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : g_PIE.graph ) assert(unique(begin(v), end(v)) == end(v));

    vector<pair<int,int>> edges(0);
    for (int i = 0; i < g_PIE.N; ++i) {
        auto &v = g_PIE.graph[i];
        if ( v.empty() ) continue;
        auto it = lower_bound(begin(v),end(v),i);
        //get_pos(i, begin(v), end(v));
        while ( it != end(v) ) {
            edges.emplace_back(i,*(it++));
        }
    }
    assert(is_sorted(begin(edges), end(edges)));
    if ( edges.empty() ) return 0;

    vector<vector<int>> node_list(instance.N);
    for (int i = 0; i < instance.N; ++i)
        node_list[i] = {i};
    // =========================

    // Step 1
    // =========================
    vector<int> comm_neigh;
    calc_common_neighbor(g_PIE, edges, comm_neigh);
    // =========================

    while (true) {
        // Step 2
        // =========================
        // find edge with max common neighbors
        int index = 0;
        for (int i = 0; i < comm_neigh.size(); ++i)
            if (comm_neigh[i] > comm_neigh[index]) index = i;
        // TODO: 2 Edges have same amount
        int i = edges[index].first;
        int j = edges[index].second;
        // update data structures

        assert( i < j );

        update_node_list(node_list, g_PIE, i, j);
        update_edges(edges, index);

        // if finished
        if (edges.empty()) {
            int index = 0;
            for (int k = 0; k < node_list.size(); ++k) {
                if (node_list[k].size() > node_list[index].size()) index = k;
            }
            delete_vertex_set(node_list[index], instance);
            delete_vertex_set(node_list[index], g_PIE);
            return 1;
        }
        calc_common_neighbor(g_PIE, edges, comm_neigh);
        // =========================

        int head = i;
        while ( true ) {
            // Step 3
            // =========================
            index = -1;
            for (int k = 0; k < comm_neigh.size(); ++k) {
                if (edges[k].first == head || edges[k].second == head) {
                    index = k;
                    break;
                }
            }
            for (int k = index; k < comm_neigh.size(); ++k) {
                if (comm_neigh[k] > comm_neigh[index] &&
                    (edges[k].first == head || edges[k].second == head))
                    index = k;
            }
            if (index == -1) break;  // goto Step 2

            // update data structures
            head = edges[index].first;
            update_node_list(node_list, g_PIE, edges[index].first, edges[index].second);
            update_edges(edges, index);
            calc_common_neighbor(g_PIE, edges, comm_neigh);
            // repeat 3
            // =========================
        }
    }
}

// if a cycle with length <= k exists return 1 and write vertices in cycle
int has_k_cycle(Graph_Instance &instance, int start, int k, vector<int> &cycle) {
    vector<int> visited(instance.N,0);
    vector<int> parent(instance.N,-1);
    vector<int> layer(instance.N,-1);
    layer[start] = 0;
    queue<int> q;
    q.push(start);
    int run = 1;
    while ( !q.empty() && run ) {
        int v = q.front();
        q.pop();
        if ( layer[v] >= k ) continue;
        for ( int u : instance.graph[v] ) {
            if ( visited[u] ) continue;
            layer[u] = layer[v] + 1;
            parent[u] = v;
            if ( u == start ) {
                run = 0;
                break; // return cycle
            }
            visited[u] = 1;
            q.push(u);
        }
    }

    if ( run ) return 0;

    cycle.push_back(start);
    int i = parent[start];
    while ( i != start ) {
        cycle.push_back(i);
        i = parent[i];
    }
    return 1;
}

// deletes all vertices with in-degree 0
int in0(Graph_Instance &instance){
    //++Logs::in0;
    int reduced = 0;
    // find vertices
    vector<int> not_found(instance.N, 1);
    for (vector<int> v : instance.graph) {
        for (int u : v) {
            not_found[u] = 0;
        }
    }
    // delete vertices
    vector<int> set;
    for (int i = 0; i < not_found.size(); ++i) {
        if ( not_found[i] ) {
            reduced = 1;
            set.push_back(i);
        }
    }
    if ( reduced ) {
        //++Logs::in0_red;
        assert(is_sorted(begin(set),end(set)));
        delete_vertex_set(set,instance);
    }
    return reduced;
}

// deletes all vertices with out-degree 0
int out0(Graph_Instance &instance){
    //++Logs::out0;
    int reduced = 0;
    vector<int> set(0);
    for ( int i = 0; i < instance.N; ++i ) {
        if ( instance.graph[i].empty() ) {
            set.push_back(i);
            reduced = 1;
        }
    }

    if ( reduced ) {
        assert(is_sorted(begin(set),end(set)));
        delete_vertex_set(set,instance);
        //++Logs::out0_red;
    }
    return reduced;
}

// adds all vertices with self-loop do dfvs
int loop(Graph_Instance &instance, vector<int> &dfvs){
    //++Logs::loop;
    int reduced = 0;
    vector<int> set;

    for (int i = 0; i < instance.N; ++i) {
        if (isin(instance.graph[i], i)) {
            reduced = 1;
            set.push_back(i);
            dfvs.push_back(instance.map[i]);
        }
    }

    if ( reduced ) {
        assert(is_sorted(begin(set),end(set)));
        delete_vertex_set(set, instance);
        //++Logs::loop_red;
    }
    return reduced;
}

// merges all vertices with in-degree 1 into their predecessor
int in1(Graph_Instance &instance){
    //++Logs::in1;
    int reduced = 0;
    vector<int> indegree(instance.N, 0);
    vector<int> predecessor(instance.N);

    // finds in-degree to every vertex
    for ( int i = 0; i < instance.N; ++i ) {
        for ( int u : instance.graph[i] ) {
            ++indegree[u];
            predecessor[u] = i;        // contains predecessor if indegree == 1
        }
    }

    // finds edges to be merged and deletes edges in instance
    vector<pair<int, int>> merges;
    vector<int> parent(instance.N,0);
    for (int i = 0; i < instance.N; ++i)
        parent[i] = i;
    vector<int> gets_merged(instance.N,0);
    for (int i = 0; i < instance.N; ++i) {
        if (indegree[i] == 1 && predecessor[i] != i ) {
            reduced = 1;
            int p = predecessor[i];
            vector<int> &v = instance.graph[p];

            gets_merged[i] = 1;
            if ( gets_merged[p] ) {
                parent[i] = parent[p];
                // produces self-loop in cycles
                if ( parent[i] == i ) instance.graph[i].push_back(i);
            }
            else parent[i] = p;
            merges.push_back({i,parent[i]});

            // deletes all edges (p,i)
            assert(unique(begin(v),end(v)) == end(v));
            v.erase(find(begin(v),end(v),i));
        }
    }

    merge_vertices(merges,instance);
    //if ( reduced ) ++Logs::in1;

    return reduced;
}

void out1_rec(int i, vector<pair<int,int>> &merges, vector<int> &found, vector<int> &gets_merged,
              Graph_Instance &instance) {
    vector<int> &v = instance.graph[i];
    if ( !gets_merged[i] && v.size() == 1 && v[0] != i ) {
        found[i] = 1;
        gets_merged[i] = 1;
        merges.push_back({i,v[0]});
        if ( found[v[0]] ) v.push_back(i);
        int j = v[0];
        v.erase(begin(v));
        out1_rec(j,merges,found,gets_merged,instance);
    }
}

// merges all vertices with out-degree 1 into their successor
int out1(Graph_Instance &instance){
    //++Logs::out1;
    int reduced = 0;
    // vector<int> has_out1(instance.N, 0);

    vector<int> gets_merged(instance.N,0);
    vector<int> found;
    vector<pair<int,int>> merges;
    for (int i = 0; i < instance.N; ++i) {
        auto &v = instance.graph[i];
        if ( !gets_merged[i] && v.size() == 1 && v[0] != i ) {
            reduced = 1;
            found = vector<int>(instance.N,0);
            out1_rec(i,merges,found,gets_merged,instance);
        }
    }

    merge_vertices(merges,instance);
    //if ( reduced ) ++Logs::out1_red;

    return reduced;
}

// sorts edges in ascending order in O(|E|+r)
void counting_sort(Graph_Instance &instance) {
    //++Logs::counting_sort;
    vector<vector<int>> g = instance.graph;

    // initialize count
    vector<vector<int>> count(instance.N);
    for ( int i = 0; i < instance.N; ++i ) {
        // maybe instance.N instead of find_max
        auto &v =  instance.graph[i];
        int max = -1;
        if ( !v.empty() ) max = *max_element(v.begin(), v.end());
        count[i] = vector<int>(max + 1, 0);
    }

    // count numbers
    for ( int i = 0; i < instance.N; ++i ) {
        for (int j = 0; j < instance.graph[i].size(); ++j) {
            ++count[i][instance.graph[i][j]];
        }
    }

    // calculate addresses
    for ( int i = 0; i < instance.N; ++i ) {
        for ( int j = 1; j < count[i].size(); ++j ) {
            count[i][j] += count[i][j-1];
        }
    }

    for ( int i = 0; i < instance.N; ++i ) {
        for ( int j = 0; j < g[i].size(); ++j ) {
            instance.graph[i][count[i][g[i][j]] - 1] = g[i][j];
            --count[i][g[i][j]];
        }
    }
}

// calculates the PIE of instance and saves it into g_PIE
// g_PIE has to have instance.N empty vertices
// does not check for double edges
void calculate_pie(Graph_Instance &instance, Graph_Instance &g_PIE) {
    //++Logs::calculate_pie;
    // countingsort
    // counting_sort(instance);

    vector<int> pointer(instance.N,0);

    for ( int v = 0; v < instance.N; ++v ) {
        for ( int u : instance.graph[v] ) {
            while ( v != u &&
                    pointer[u] < instance.graph[u].size() &&
                    instance.graph[u][pointer[u]] < v ) {

                ++pointer[u];
            }

            if ( v != u &&
                 pointer[u] < instance.graph[u].size() &&
                 instance.graph[u][pointer[u]] == v )
                g_PIE.graph[v].push_back(u);
        }
    }
}

void calculate_notpie(Graph_Instance &instance, Graph_Instance &g_not_PIE) {
    //++Logs::calculate_notpie;
    // countingsort
    // counting_sort(instance);

    vector<int> pointer(instance.N,0);

    for ( int v = 0; v < instance.N; ++v ) {
        for ( int u : instance.graph[v] ) {
            while ( v != u &&
                    pointer[u] < instance.graph[u].size() &&
                    instance.graph[u][pointer[u]] < v ) {

                ++pointer[u];
            }

            if ( !( v != u &&
                    pointer[u] < instance.graph[u].size() &&
                    instance.graph[u][pointer[u]] == v ) )
                g_not_PIE.graph[v].push_back(u);
        }
    }
}

void calculate_pie_notpie(Graph_Instance &instance, Graph_Instance &g_PIE, Graph_Instance &g_not_PIE) {
    //++Logs::calculate_pie_notpie;
    // countingsort
    // counting_sort(instance);

    vector<int> pointer(instance.N,0);

    for ( int v = 0; v < instance.N; ++v ) {
        for ( int u : instance.graph[v] ) {
            while ( v != u &&
                    pointer[u] < instance.graph[u].size() &&
                    instance.graph[u][pointer[u]] < v ) {

                ++pointer[u];
            }

            if ( v != u &&
                 pointer[u] < instance.graph[u].size() &&
                 instance.graph[u][pointer[u]] == v )
                g_PIE.graph[v].push_back(u);
            else g_not_PIE.graph[v].push_back(u);
        }
    }
}

// from https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=833199
// possible in O(|V|+|E|)
int pie_operation(Graph_Instance &instance){
    //++Logs::pie_op;
    int reduced = 0;

    //vector<vector<int>> g_without_piEdges = instance.graph;
    Graph_Instance g_without_piEdges = Graph_Instance(instance.N);
    calculate_notpie(instance,g_without_piEdges);

    // calculates SCCs of G\PIE
    SCC_compute sc = SCC_compute(g_without_piEdges);
    vector<int> coloration(g_without_piEdges.N, 0);
    sc.getSCCcoloration(coloration);

    // deletes all edges between SCCs of G\PIE in instance
    vector<vector<int>> tbd(instance.N);
    for (int i = 0; i < g_without_piEdges.N; ++i) {
        for (int u : g_without_piEdges.graph[i]) {
            // if the color of vertex i and its successor u differs: delete the edge (i,u)
            if (coloration[i] != coloration[u]) {
                tbd[i].push_back(u);
                reduced = 1;
            }
        }
    }

    if ( !reduced ) return reduced;

    for ( int i = 0; i < instance.N; ++i ) {
        auto it_begin = begin(instance.graph[i]);
        auto it_end = end(instance.graph[i]);
        for ( int e : tbd[i] ) {
            it_end = remove(it_begin,it_end,e);
        }
        // auto &v = instance.graph[i];
        instance.graph[i].resize(instance.graph[i].size() - tbd[i].size());
    }
    //++Logs::pie_op_red;

    return reduced;
}

// from https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=833199
// possible in O(|E|+|V|log|V|)
int core_operation(Graph_Instance &instance, vector<int> &dfvs){
    //++Logs::core_op;
    int reduced = 0;

    // get sorted PIV
    Graph_Instance g_PIE(instance.N);
    calculate_pie(instance,g_PIE);

    vector<int> degree(instance.N, 0);
    for (int i = 0; i < instance.N; ++i) {
        degree[i] += instance.graph[i].size();
        for (int j = 0; j < instance.graph[i].size(); ++j)
            ++degree[instance.graph[i][j]];
    }

    // PIV is a set of pairs (d,v) with v in PIV and d = degree(v)
    vector<pair<int,int>> PIV(0);
    for (int i = 0; i < instance.N; ++i)
        if ( degree[i] == (2 * g_PIE.graph[i].size()) && degree[i] != 0 )
            PIV.push_back({degree[i],i});

    sort(begin(PIV),end(PIV));

    // foreach v in PIV
    vector<int> valid(instance.N,1);
    vector<int> tbd(0);
    vector<int> new_dfvs(0);

    for ( auto [d,v] : PIV ) {
        // if clique
        if ( !valid[v] ) continue;
        if ( is_clique(v,g_PIE) ) {
            reduced = 1;
            tbd.push_back(v);
            valid[v] = 0;
            for ( int u : g_PIE.graph[v] ) {
                new_dfvs.push_back(instance.map[u]);
                tbd.push_back(u);
                valid[u] = 0;
            }
        }
        else {
            valid[v] = 0;
            for ( int u : g_PIE.graph[v] ) {
                valid[u] = 0;
            }
        }
    }

    if (reduced) {
        remove_duplicates(tbd);
        delete_vertex_set(tbd, instance);

        remove_duplicates(new_dfvs);
        move(new_dfvs.begin(), new_dfvs.end(), back_inserter(dfvs));

        //++Logs::core_op_red;
    }

    return reduced;
}

// from https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=833199
// possible in O(|E||V|)
int dome_operation(Graph_Instance &instance){
    //++Logs::dome_op;
    int reduced = 0;
    // berechne g_PIV, G\PIV
    Graph_Instance g_piV = Graph_Instance(instance.N);
    Graph_Instance g_notpi = Graph_Instance(instance.N);

    calculate_pie_notpie(instance,g_piV,g_notpi);

    int condition1_satisfied = 0;
    vector<vector<int>> tbd(instance.N);
    // for each edge (u,v)
    // should be (v,u)
    for ( int v = 0; v < instance.N; ++v ) {
        for ( int j = 0; j < instance.graph[v].size(); ++j ) {
            condition1_satisfied = 0;
            int u = instance.graph[v][j];
            // not in original paper but has to be...
            if ( !isin(g_piV.graph[v], u) ) {
                // check for condition 1, delete edge if necessary
                vector<int> p_u(0);
                vector<int> p_v(0);
                for ( int i = 0; i < instance.N; ++i ) {
                    if ( isin(g_notpi.graph[i], v) ) p_u.push_back(i);
                    if ( isin(instance.graph[i], u) ) p_v.push_back(i);
                }

                if ( is_subset(p_v,p_u) ) {
                    tbd[v].push_back(instance.graph[v][j]);
                    condition1_satisfied = 1;
                    reduced = 1;
                }

                // check for condition 2, delete edge if necessary
                if ( !condition1_satisfied ) {
                    vector<int> s_v = g_notpi.graph[u];
                    vector<int> s_u = instance.graph[v];

                    if ( is_subset(s_u,s_v) ) {
                        tbd[v].push_back(instance.graph[v][j]);
                        reduced = 1;
                    }
                }
            }
        }
    }

    if ( !reduced ) return reduced;

    for ( int i = 0; i < instance.N; ++i ) {
        auto it_begin = begin(instance.graph[i]);
        auto it_end = end(instance.graph[i]);
        for ( int e : tbd[i] ) {
            it_end = remove(it_begin,it_end,e);
        }
        // auto &v = instance.graph[i];
        instance.graph[i].resize(instance.graph[i].size() - tbd[i].size());
    }
    //++Logs::dome_op_red;

    return reduced;
}

int path_finder(Graph_Instance &instance, int s, int t, vector<int> &parent) {
    //++Logs::path_finder;
    vector<int> found(instance.N,0);
    queue<int> q;
    q.push(s);
    while ( !q.empty() ) {
        int v = q.front();
        q.pop();
        for ( int u : instance.graph[v] ) {
            if ( found[u] ) continue;
            parent[u] = v;
            if ( u == t ) return 1;
            found[u] = 1;
            q.push(u);
        }
    }

    return 0;
}

// instance has to be sorted
// s has to be a source, t a sink
// s, t have to be in instance
int has_unique_path(Graph_Instance &instance, int s, int t) {
    //++Logs::has_unique_path;
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    vector<int> parent(instance.N, -1);
    vector<int> parent2(instance.N,-1);
    // find first path (saved in parent)
    // if no path exists return 0

    if ( !path_finder(instance, s, t, parent) ) return 0;

    // build residual graph
    int i = t;
    while ( i != s ) {
        //instance.graph[i].push_back(parent[i]);
        auto it = begin(instance.graph[i]);
        if ( !instance.graph[i].empty() )
            it = lower_bound(it, end(instance.graph[i]), parent[i]);
        //Vertex_op::get_pos(parent[i],it, end(instance.graph[i]));
        instance.graph[i].insert(it,parent[i]);
        auto &v = instance.graph[parent[i]];
        v.erase(remove(begin(v),end(v),i),end(v));
        i = parent[i];
    }
    for ( auto &v : instance.graph )
        assert(is_sorted(begin(v), end(v)));

    // if augmenting path exists return 0, else 1
    int return_value = 1;
    if ( path_finder(instance,s,t,parent2) ) return_value = 0;

    //reverse all changes to instance
    i = t;
    while ( i != s ) {
        auto &v = instance.graph[parent[i]];
        auto &u = instance.graph[i];
        auto it = begin(u);
        if ( !u.empty() ) {
            it = lower_bound(it,end(u),parent[i]);
            //Vertex_op::get_pos(parent[i],it, end(u));
            if ( it != end(u) ) u.erase(it);
        }
        it = begin(v);
        if ( !v.empty() ) it = lower_bound(it,end(v),i);
        //Vertex_op::get_pos(i,it, end(v));
        v.insert(it,i);
        i = parent[i];
    }
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));

    return return_value;
}

// returns 1 if there are at least k edge disjoint paths between s and t, 0 otherwise
// s has to be a source, t a sink
// s, t have to be in instance
int has_k_paths(Graph_Instance &instance, int k, int s, int t) {
    //++Logs::has_k_paths;
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    int return_Value = 0;
    vector<vector<int>> parent(0);
    parent.push_back(vector(instance.N,-1));
    int paths = 0;

    while ( path_finder(instance,s,t,parent[paths]) ) {
        if ( paths >= k - 1 ) {
            return_Value = 1;
            break;
        }

        // build residual graph
        int i = t;
        while ( i != s ) {
            //instance.graph[i].push_back(parent[i]);
            auto it = begin(instance.graph[i]);
            if ( !instance.graph[i].empty() )
                it = lower_bound(it, end(instance.graph[i]), parent[paths][i]);
            instance.graph[i].insert(it,parent[paths][i]);
            auto &v = instance.graph[parent[paths][i]];
            v.erase(remove(begin(v),end(v),i),end(v));
            i = parent[paths][i];
        }

        // reset parent
        ++paths;
        parent.push_back(vector<int>(instance.N,-1));
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    }

    // reverse changes to residual graph
    for (int j = parent.size() - 2; j >= 0; --j) {
        int i = t;
        while ( i != s ) {
            auto &v = instance.graph[parent[j][i]];
            auto &u = instance.graph[i];
            auto it = begin(u);
            if ( !u.empty() ) {
                it = lower_bound(it,end(u),parent[j][i]);
                if ( it != end(u) ) u.erase(it);
            }
            it = begin(v);
            if ( !v.empty() ) it = lower_bound(it,end(v),i);
            v.insert(it,i);
            i = parent[j][i];
        }
    }

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    return return_Value;
}

int shortcut(Graph_Instance &instance) {
    //++Logs::shortcut;
    int reduced = 0;
    // reduce disjoint vertex path to disjoint edge path
    Graph_Instance min_cut(instance.N * 2);
    for (int i = 0; i < instance.N; ++i) {
        min_cut.graph[i] = {instance.N + i};
        min_cut.graph[instance.N + i] = instance.graph[i];
    }

    // check for every vertex v if petal(v)=1
    for (int i = instance.N - 1; i >= 0; --i) {
        if ( isin(instance.graph[i],i) ) continue;
        min_cut.graph[i] = vector<int>(0);
        if ( has_unique_path(min_cut,i + instance.N,i) ) {
            reduced = 1;
            min_cut.graph[i] = min_cut.graph[i + instance.N];
            delete_vertex(i + instance.N,min_cut);
            replace_vertex(i,min_cut);
            replace_vertex(i,instance);
        }
        else min_cut.graph[i] = vector<int>(1,i + instance.N);
    }

    // handle obsolete vertices
    // kann man nicht machen weil gegenbeispiel
    // kann es noch Knoten geben, die auf keinem Kreis liegen?
    /*
    if ( reduced ) {
        assert(is_sorted(begin(set),end(set)));
        for (int i = set.size() - 1; i >= 0; --i) {
            if ( isin(instance.graph[set[i]],set[i]) ) continue;
            replace_vertex(set[i],instance);
        }
    }
    */
    //if ( reduced ) ++Logs::shortcut_red;
    return reduced;
}

int flower(Graph_Instance &instance, int k, vector<int> &dfvs) {
    //++Logs::flower;
    int reduced = 0;
    // reduce disjoint vertex path to disjoint edge path
    Graph_Instance min_cut(instance.N * 2);
    for (int i = 0; i < instance.N; ++i) {
        min_cut.graph[i] = {instance.N + i};
        min_cut.graph[instance.N + i] = instance.graph[i];
    }

    // check for every vertex v if petal(v) >= |k|
    vector<int> set(0);
    for (int i = 0; i < instance.N; ++i) {
        if ( isin(instance.graph[i],i) ) continue;
        min_cut.graph[i] = vector<int>(0);
        if ( has_k_paths(min_cut,k,i + instance.N,i) ) {
            reduced = 1;
            dfvs.push_back(instance.map[i]);
            set.push_back(i);
            // delete_vertex(i + instance.N,min_cut);
            // delete_vertex(i,min_cut);
            // delete_vertex(i,instance);
        }
        else min_cut.graph[i] = vector<int>(1,i + instance.N);
    }

    assert(is_sorted(begin(set),end(set)));
    delete_vertex_set(set,instance);
    //if ( reduced ) ++Logs::flower_red;
    return reduced;
}

int lower_bound_matching(Graph_Instance &instance) {
    //++Logs::lower_bound_matching;
    int lower_bound = 0;

    Graph_Instance g_PIE(instance.N);
    calculate_pie(instance,g_PIE);

    vector<int> alive(g_PIE.N,1);
    for (int i = 0; i < g_PIE.N; ++i)
        if ( g_PIE.graph[i].empty() ) alive[i] = 0;

    for (int i = 0; i < g_PIE.N; ++i) {
        vector<int> &v = g_PIE.graph[i];
        if ( !alive[i] ) continue;
        for (int j = 0; j < v.size(); ++j) {
            if ( !alive[v[j]] ) continue;
            int u = v[j];

            alive[i] = 0;
            alive[u] = 0;

            ++lower_bound;
            break;
        }
        alive[i] = 0;
    }

    return lower_bound;
}

int reduce_graph(Graph_Instance &instance, vector<int> &dfvs, vector<int> &upper_bound){
    //++Logs::reduce_graph;
    int not_yet_fully_reduced = 1;

    // check for lower_bound of unreduced graph
    int lower_bound = lower_bound_matching(instance);
    if ( lower_bound + dfvs.size() >= upper_bound.size() ) return 1;

    while (not_yet_fully_reduced){
        not_yet_fully_reduced = 0;

        // check for naive lower_bound while reducing
        if ( dfvs.size() >= upper_bound.size() ) return 1;

        // Assumption: no selfloops exist at this point
        if ( out0(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && out1(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && loop(instance, dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && in0(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && in1(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && pie_operation(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && core_operation(instance, dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && dome_operation(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && shortcut(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && flower(instance,upper_bound.size() - dfvs.size() + 1,dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
    }

    // check for lower_bound of reduced graph
    lower_bound = lower_bound_matching(instance);
    if ( lower_bound + dfvs.size() >= upper_bound.size() ) return 1;

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    return 0;
}

int reduce_graph(Graph_Instance &instance, vector<int> &dfvs) {
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    int not_yet_fully_reduced = 1;
    while (not_yet_fully_reduced){
        not_yet_fully_reduced = 0;

        // Assumption: no selfloops exist at this point
        if ( out0(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && out1(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && loop(instance, dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && in0(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && in1(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && pie_operation(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && core_operation(instance, dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && dome_operation(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && shortcut(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        // if ( !not_yet_fully_reduced && flower(instance,upper_bound.size() + 1,dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
    }

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    return 0;
}

// --------------------
// Branch&Bound
// --------------------

int get_highest_degree_vertex(Graph_Instance &instance) {
    vector<int> degrees(instance.N, 0);
    for (int i = 0; i < instance.N; ++i) {
        degrees[i] += instance.graph[i].size();
        for (int j = 0; j < instance.graph[i].size(); ++j) {
            degrees[instance.graph[i][j]]++;
        }
    }

    int index_max = 0;
    for (int i = 0; i < degrees.size(); ++i) {
        if ( degrees[i] > degrees[index_max] ) index_max = i;
    }
    return index_max;
}

int in_times_out(vector<int> &alive,Graph_Instance &instance) {
    vector<int> out_degrees(instance.N);
    vector<int> in_degrees(instance.N,0);

    for (int i = 0; i < instance.N; ++i) {
        if ( !alive[i] ) continue;
        out_degrees[i] = instance.graph[i].size();
        for (int j = 0; j < out_degrees[i]; ++j) {
            ++in_degrees[instance.graph[i][j]];
        }
    }

    int index = 0;
    int max_weight = 0;
    for (int i = 0; i < instance.N; ++i) {
        if ( alive[i] && max_weight < in_degrees[i] * out_degrees[i] ) {
            max_weight = in_degrees[i] * out_degrees[i];
            index = i;
        }
    }

    return index;
}

int get_branching_index(Graph_Instance &instance) {
    //++Logs::get_branching_index;
    return get_highest_degree_vertex(instance);
}

int get_branching_index(vector<int> &alive, Graph_Instance &instance) {
    //++Logs::get_branching_index_greedy;
    return in_times_out(alive,instance);
}

int is_acyclic_util(int v, vector<int> &visited, vector<int> &s, vector<int> &alive, Graph_Instance &instance) {
    visited[v] = 1;
    s.push_back(v);
    vector<int> &node = instance.graph[v];
    for (int i = 0; i < node.size(); ++i) {
        if ( !alive[node[i]] ) continue;
        if ( visited[node[i]] )
            for ( int e : s )
                if ( node[i] == e ) return 0;
        if ( !is_acyclic_util(node[i],visited,s,alive,instance) ) return 0;
    }
    s.pop_back();
    return 1;
}

int is_acyclic(vector<int> &alive, Graph_Instance &instance) {
    //++Logs::is_acyclic;
    vector<int> visited(instance.N,0);
    vector<int> s;

    for (int i = 0; i < instance.N; ++i) {
        if ( alive[i] && !visited[i] ) {
            if ( !is_acyclic_util(i,visited,s,alive,instance) ) return 0;
        }
    }

    return 1;
}

void solve_greedy(Graph_Instance &instance, vector<int> &dfvs) {
    //++Logs::solve_greedy;
    vector<int> alive(instance.N, 1);

    while ( true ) {
        // find highest degree vertex and "delete"
        int v = get_branching_index(alive,instance);
        alive[v] = 0;
        dfvs.push_back(instance.map[v]);
        // check if acyclic
        if ( is_acyclic(alive,instance) ) break;
    }
}

void solve(vector<int> current_dfvs, Graph_Instance instance,
                             vector<int> &upper_bound) {
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    // if ( lower_bound + current_dfvs.size() >= upper_bound.size() ) return;
    int lower_bound;
    if ( reduce_graph(instance,current_dfvs,upper_bound) ) return;

    if ( instance.N <= 0 ) {
        if ( current_dfvs.size() < upper_bound.size() ) upper_bound = current_dfvs;
        return;
    }

    // calculate SCC
    SCC_compute sc = SCC_compute(instance);
    vector<int> coloration(instance.graph.size(), 0);
    sc.getSCCcoloration(coloration);

    // biggest SCC selected as last
    vector color_count(sc.scc_count,0);
    for ( int i = 0; i < instance.N; ++i )
        ++color_count[coloration[i]];
    int index_max = 0;
    for ( int i = 0; i < sc.scc_count; ++i )
        if ( color_count[index_max] < color_count[i] ) index_max = i;
    for (int i = 0; i < instance.N; ++i) {
        if ( coloration[i] == index_max ) coloration[i] = sc.scc_count - 1;
        else if ( coloration[i] == sc.scc_count - 1 ) coloration[i] = index_max;
    }

    Graph_Instance component(0);
    vector<int> current_dfvs_component;
    vector<int> upper_bound_component;
    // if multiple SCCs exist solve them independently
    vector<int> tbd_comp;
    vector<int> tbd_inst;
    vector<int> tbd_col;

    // calculates dfvs for each component except for last one
    for ( int i = 0; i < sc.scc_count - 1; ++i ){
        component = instance;
        current_dfvs_component = {};
        tbd_comp = {};
        tbd_inst = {};
        int color_count = 0;
        for (int j = 0; j < coloration.size(); ++j) {
            if (coloration[j] == i) {
                tbd_inst.push_back(j);
                ++color_count;
            } else tbd_comp.push_back(j);
        }

        remove(begin(coloration),end(coloration),i);
        coloration.resize(coloration.size() - color_count);
        delete_vertex_set(tbd_comp, component);
        delete_vertex_set(tbd_inst, instance);
        upper_bound_component = {};
        solve_greedy(component,upper_bound_component);
        solve(current_dfvs_component, component, upper_bound_component);
        if ( current_dfvs.size() + current_dfvs_component.size() >= upper_bound.size() ) break;
        move(begin(upper_bound_component),end(upper_bound_component),back_inserter(current_dfvs));
    }

    // select vertex
    int vertex = get_branching_index(instance);

    // create left branch
    Graph_Instance component_left = instance;
    vector<int> current_dfvs_left = current_dfvs;
    current_dfvs_left.push_back(instance.map[vertex]);

    delete_vertex(vertex,component_left);
    solve(current_dfvs_left,component_left,upper_bound);

    // create right branch
    Graph_Instance &component_right = instance;
    vector<int> current_dfvs_right = current_dfvs;

    replace_vertex(vertex,component_right);
    solve(current_dfvs_right,component_right,upper_bound);
}

// --------------------
// Parser
// --------------------

void read_input(Graph_Instance &instance) {
    string current_line = "";
    string next_number = "";
    int n, e;

    // handle header lines
    getline(cin,current_line);
    while ( current_line[0] == '%' ) {
        getline(cin, current_line);      // skips comments
    }

    // get n and e
    istringstream ss(current_line);
    ss >> n >> e;

    // create instance
    instance = Graph_Instance(n);
    instance.map.resize(n);

    // get edges
    int i = 0;
    while ( i < n ) {
        getline(cin, current_line);
        if ( current_line[0] == '%') continue;      // check for comment
        istringstream ss(current_line);

        instance.graph[i] = vector<int>(istream_iterator<int>{ss}, istream_iterator<int>{});
        for ( int &edge : instance.graph[i] ) --edge;     // correct indizes

        instance.map[i] = i;
        ++i;
    }
}

void write_output(vector<int> &solution) {
    for ( int e : solution )
        cout << ++e << "\n";
}

// --------------------
// Main
// --------------------

vector<int> dfvs_upper_bound(0);

void signalHandler( int signum ) {
    write_output(dfvs_upper_bound);
    exit(signum);
}

int main(int argc, char *argv[]) {
    // register signal SIGINT and signal handler
    signal(SIGINT, signalHandler);

    int setrlimit(int RLIMIT_STACK, const struct rlimit *RLIM_INFINITY);
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    // get Input
    Graph_Instance instance(0);
    read_input(instance);

    // init
    vector<int> current_dfvs(0);
    dfvs_upper_bound = instance.map;

    // solve
    counting_sort(instance);
    solve(current_dfvs,instance,dfvs_upper_bound);

    // print output
    write_output(dfvs_upper_bound);
    return 0;
}