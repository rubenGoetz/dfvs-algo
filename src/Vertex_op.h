//
// Created by Ruben Götz on 20.12.2021.
//

#ifndef DFVS_VERTEX_OP_H
#define DFVS_VERTEX_OP_H
#include "Structs.cpp"
#include <vector>
using namespace std;


namespace Vertex_op {
    void delete_vertex(int v, Graph_Instance &instance);
    void delete_vertex(int v, vector<vector<int>> &graph);
    void delete_vertex_set(vector<int> &set, Graph_Instance &instance);
    void merge_vertices(int v, int u, Graph_Instance &instance);
    void merge_vertices(const vector<pair<int,int>>& merges, Graph_Instance &instance);
    void replace_vertex(int v, Graph_Instance &instance);

    vector<int>::iterator get_pos(int e, vector<int>::iterator low, vector<int>::iterator high);
    void merge(vector<int> &v1, vector<int> &v2);
};


#endif //DFVS_VERTEX_OP_H
