//
// Created by Ruben Götz on 06.01.2022.
//

#ifndef DFVS_PARSER_H
#define DFVS_PARSER_H
#include "Structs.cpp"
#include <vector>
#include <string>

using namespace std;

namespace Parser {
    int extract_graph(string path, Graph_Instance &instance);
    int output_dfvs(string path, vector<int> &g);
    void read_input(Graph_Instance &instance);
    void write_output(vector<int> &solution);
    void get_solution_file(string path, vector<int> &solution);
};


#endif //DFVS_PARSER_H
