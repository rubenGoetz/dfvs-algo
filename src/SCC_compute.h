//
// Created by Ruben Götz on 20.12.2021.
//

#ifndef DFVS_SCC_COMPUTE_H
#define DFVS_SCC_COMPUTE_H
#include "Structs.cpp"
#include "Stack.h"
#include <vector>
using namespace std;


class SCC_compute {
private:
    vector<vector<int>> graph;
    int amount;
    int id;
    vector<int> lows;
    vector<int> ids;
    vector<int> marked;
    vector<int> stack;

public:
    int scc_count;
    SCC_compute(Graph_Instance &instance);
    void getSCCcoloration(vector<int> &coloration);
    void dfs(int v, vector<int> &coloration);
    void visit(int v, vector<int> &visited,vector<int> &coloration);
    void all_children_visited(Stack &context, vector<int> &coloration);
    void callback(Stack &context, vector<int> &visited,vector<int> &coloration);
    static int clean_coloration(vector<int> &coloration);
};


#endif //DFVS_SCC_COMPUTE_H
