//
// Created by Ruben Götz on 12.02.2022.
//

#ifndef DFVS_STACK_H
#define DFVS_STACK_H
#include <vector>
#include <tuple>
using namespace std;


class Stack {
private:
    vector<tuple<int,int>> stack;
    int top;
public:
    Stack(int max_size);
    tuple<int,int> pop();
    tuple<int,int> get();
    void push(tuple<int,int> t);
    int not_empty();
};


#endif //DFVS_STACK_H
