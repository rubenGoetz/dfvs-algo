//
// Created by Ruben Götz on 12.02.2022.
//

#include "Stack.h"
using namespace std;

Stack::Stack(int max_size) {
    vector<tuple<int,int>> s(max_size);
    stack = s;
    top = -1;
}

tuple<int,int> Stack::pop() {
    --top;
    return stack[top + 1];
}

tuple<int,int> Stack::get() {
    return stack[top];
}

void Stack::push(tuple<int,int> t) {
    ++top;
    stack[top] = t;
}

int Stack::not_empty() {
    return top >= 0;
}