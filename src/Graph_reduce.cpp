#include "Graph_reduce.h"
#include "Vertex_op.h"
#include "SCC_compute.h"
//#include "../unit_tests/Logs.h"
#include <assert.h>
#include <queue>
#include <random>
#include <vector>
#include <tuple>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <chrono>
using namespace std;
using namespace Vertex_op;

// Helper functions
// returns 1 in v and its neighbors are a clique, 0 otherwise
// should be bfs
// g_PIE has to be sorted, v in PIV (no edges outside of PIE)
int Graph_reduce::is_clique(int v, Graph_Instance &g_PIE) {
    //++Logs::is_clique;
    for ( auto &vertex : g_PIE.graph ) assert(is_sorted(begin(vertex), end(vertex)));

    vector<int> &neighbors = g_PIE.graph[v];

    for ( int u : neighbors ) {
        // inserts u into a copy of graph[u]
        vector<int> set(g_PIE.graph[u].size() + 1);
        int inserted = 0;
        for (int i = 0; i < set.size(); ++i) {
            if ( !inserted ) {
                if ( i >= (set.size() - 1) || g_PIE.graph[u][i] >= u ) {
                    set[i] = u;
                    inserted = 1;
                }
                else set[i] = g_PIE.graph[u][i];
            }
            else set[i] = g_PIE.graph[u][i - 1];
        }

        if ( !is_subset(set,neighbors) ) return 0;
    }

    return 1;
}

int isin_util(vector<int> &v, int low, int high, int e) {
    if ( v[low] == e || v[high] == e ) return 1;
    int mid = (low + high) / 2;
    if ( mid <= low || mid >= high ) return 0;
    if ( v[mid] >= e ) return isin_util(v,low,mid,e);
    else return isin_util(v,mid,high,e);
}

// vertex has to be sorted!
int Graph_reduce::isin(vector<int> &vertex, int e) {
    assert( is_sorted(begin(vertex),end(vertex)) );
    //++Logs::isin;
    return binary_search(begin(vertex),end(vertex),e);

    /*
    if ( vertex.empty() ) return 0;
    return isin_util(vertex,0,vertex.size() - 1, e);
    */
}

// not used
int Graph_reduce::isin(vector<int> &vertex, int e, int &pos) {
    //++Logs::isin_pos;
    // way more elegant solution by Chris
    auto it = find(begin(vertex), end(vertex), e);
    if( it == end(vertex)) return 0;
    pos = it - begin(vertex);
    return 1;
}

void Graph_reduce::remove_duplicates(vector<int> &v) {
    //++Logs::remove_duplicates;
    sort(v.begin(),v.end());
    // way more elegant solution by Chris
    v.erase(unique(begin(v),end(v)),end(v));
}

int Graph_reduce::lower_bound_matching(Graph_Instance &instance) {
    //++Logs::lower_bound_matching;
    int lower_bound = 0;

    Graph_Instance g_PIE(instance.N);
    calculate_pie(instance,g_PIE);

    vector<int> alive(g_PIE.N,1);
    for (int i = 0; i < g_PIE.N; ++i)
        if ( g_PIE.graph[i].empty() ) alive[i] = 0;

    for (int i = 0; i < g_PIE.N; ++i) {
        vector<int> &v = g_PIE.graph[i];
        if ( !alive[i] ) continue;
        for (int j = 0; j < v.size(); ++j) {
            if ( !alive[v[j]] ) continue;
            int u = v[j];

            alive[i] = 0;
            alive[u] = 0;

            ++lower_bound;
            break;
        }
        alive[i] = 0;
    }

    return lower_bound;
}

void Graph_reduce::calc_common_neighbor(Graph_Instance &undirected, vector<pair<int,int>> &edges,
                                        vector<int> &comm_neigh) {
    comm_neigh = vector<int>(0);

    for ( auto [i,j] : edges ) {
        auto iit = begin(undirected.graph[i]);
        auto jit = begin(undirected.graph[j]);
        int amount = 0;
        while ( iit != end(undirected.graph[i]) && jit != end(undirected.graph[j]) ) {
            if ( *iit == * jit ) {
                ++amount;
                ++iit;
                ++jit;
            }
            else if ( *iit < *jit ) ++iit;
            else ++jit;
        }
        comm_neigh.push_back(amount);
    }
}

// i < j
void Graph_reduce::update_node_list(vector<vector<int>> &node_list, Graph_Instance &undirected, int i, int j) {
    int iindex = -1;
    int jindex = -1;
    for (int k = 0; k < node_list.size(); ++k) {
        if ( binary_search(begin(node_list[k]),end(node_list[k]),i) ) iindex = k;
        if ( binary_search(begin(node_list[k]),end(node_list[k]),j) ) jindex = k;
        //if ( node_list[k][0] == i ) iindex = k;
        //if ( node_list[k][0] == j ) jindex = k;
    }
    assert ( iindex != -1 );
    assert ( jindex != -1 );
    merge(node_list[iindex],node_list[jindex]);
    node_list.erase(begin(node_list) + jindex);
    for ( auto &v : undirected.graph ) {
        v.erase(remove(begin(v), end(v),j),end(v));
    }
}

void Graph_reduce::update_edges(vector<pair<int,int>> &edges, int index) {
    int i = edges[index].first;
    int j = edges[index].second;
    auto it = begin(edges);
    while ( it != end(edges) ) {
        int u = (*it).first;
        int v = (*it).second;
        if      ( v == j || u == j ) it = edges.erase(it);
        else if ( u == i &&
                  !binary_search(edges.begin(), edges.end(), pair<int,int>(v,j)) &&
                  !binary_search(edges.begin(), edges.end(), pair<int,int>(j,v)) ) it = edges.erase(it);
        else if ( v == i &&
                  !binary_search(edges.begin(), edges.end(), pair<int,int>(u,j)) &&
                  !binary_search(edges.begin(), edges.end(), pair<int,int>(j,u)) ) it = edges.erase(it);
        else ++it;
    }
    for ( auto e : edges ) assert ( e.first != j && e.second != j );
}

// https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=1270207
int Graph_reduce::remove_max_clique(Graph_Instance &instance, Graph_Instance &g_PIE) {
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    // 0 - build data-structures
    // =========================
    for ( auto &v : g_PIE.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : g_PIE.graph ) assert(unique(begin(v), end(v)) == end(v));

    vector<pair<int,int>> edges(0);
    for (int i = 0; i < g_PIE.N; ++i) {
        auto &v = g_PIE.graph[i];
        if ( v.empty() ) continue;
        auto it = lower_bound(begin(v),end(v),i);
        //get_pos(i, begin(v), end(v));
        while ( it != end(v) ) {
            edges.emplace_back(i,*(it++));
        }
    }
    assert(is_sorted(begin(edges), end(edges)));
    if ( edges.empty() ) return 0;

    vector<vector<int>> node_list(instance.N);
    for (int i = 0; i < instance.N; ++i)
        node_list[i] = {i};
    // =========================

    // Step 1
    // =========================
    vector<int> comm_neigh;
    calc_common_neighbor(g_PIE, edges, comm_neigh);
    // =========================

    while (true) {
        // Step 2
        // =========================
        // find edge with max common neighbors
        int index = 0;
        for (int i = 0; i < comm_neigh.size(); ++i)
            if (comm_neigh[i] > comm_neigh[index]) index = i;
        // TODO: 2 Edges have same amount
        int i = edges[index].first;
        int j = edges[index].second;
        // update data structures

        assert( i < j );

        update_node_list(node_list, g_PIE, i, j);
        update_edges(edges, index);

        // if finished
        if (edges.empty()) {
            int index = 0;
            for (int k = 0; k < node_list.size(); ++k) {
                if (node_list[k].size() > node_list[index].size()) index = k;
            }
            delete_vertex_set(node_list[index], instance);
            delete_vertex_set(node_list[index], g_PIE);
            return 1;
        }
        calc_common_neighbor(g_PIE, edges, comm_neigh);
        // =========================

        int head = i;
        while ( true ) {
            // Step 3
            // =========================
            index = -1;
            for (int k = 0; k < comm_neigh.size(); ++k) {
                if (edges[k].first == head || edges[k].second == head) {
                    index = k;
                    break;
                }
            }
            for (int k = index; k < comm_neigh.size(); ++k) {
                if (comm_neigh[k] > comm_neigh[index] &&
                    (edges[k].first == head || edges[k].second == head))
                    index = k;
            }
            if (index == -1) break;  // goto Step 2

            // update data structures
            head = edges[index].first;
            update_node_list(node_list, g_PIE, edges[index].first, edges[index].second);
            update_edges(edges, index);
            calc_common_neighbor(g_PIE, edges, comm_neigh);
            // repeat 3
            // =========================
        }
    }
}

// if a cycle with length <= k exists return 1 and write vertices in cycle
int Graph_reduce::has_k_cycle(Graph_Instance &instance, int start, int k, vector<int> &cycle) {
    vector<int> visited(instance.N,0);
    vector<int> parent(instance.N,-1);
    vector<int> layer(instance.N,-1);
    layer[start] = 0;
    queue<int> q;
    q.push(start);
    int run = 1;
    while ( !q.empty() && run ) {
        int v = q.front();
        q.pop();
        if ( layer[v] >= k ) continue;
        for ( int u : instance.graph[v] ) {
            if ( visited[u] ) continue;
            layer[u] = layer[v] + 1;
            parent[u] = v;
            if ( u == start ) {
                run = 0;
                break; // return cycle
            }
            visited[u] = 1;
            q.push(u);
        }
    }

    if ( run ) return 0;

    cycle.push_back(start);
    int i = parent[start];
    while ( i != start ) {
        cycle.push_back(i);
        i = parent[i];
    }
    return 1;
}

// works on copy of instance
int Graph_reduce::lower_bound_reduction(Graph_Instance &instance) {
    Graph_Instance copy = instance;
    int lower_bound = 0;
    while ( copy.N > 0 ) {
        vector<int> dfvs(0);
        reduce_graph(copy, dfvs);
        lower_bound += dfvs.size();
        if ( copy.N <= 0 ) break;
        int N = copy.N;
        // can be kept updated
        Graph_Instance g_PIE(copy.N);
        calculate_pie(copy,g_PIE);
        if ( remove_max_clique(copy,g_PIE) ) {
            lower_bound += (N - copy.N) - 1;
            continue;
        }
        // else remove minimum length circle and 1 to lower_bound
        vector<int> min_cycle(0);
        vector<int> cycle(0);
        int k = copy.N;
        for (int i = 0; i < copy.N; ++i) {
            cycle = {};
            if ( has_k_cycle(copy,i,k,cycle) ) {
                min_cycle = cycle;
                k = cycle.size() - 1;
            }
        }
        sort(begin(min_cycle),end(min_cycle));
        delete_vertex_set(min_cycle,copy);
        ++lower_bound;
    }

    return lower_bound;
}

// map[i] = the original vertex i corresponds to.
// returns 1 if dfvs >= upper_bound
int Graph_reduce::reduce_graph(Graph_Instance &instance, vector<int> &dfvs, vector<int> &upper_bound,
                               chrono::steady_clock::time_point &timeout){
    //++Logs::reduce_graph;
    int not_yet_fully_reduced = 1;

    // check for lower_bound of unreduced graph
    int lower_bound = lower_bound_matching(instance);
    if ( lower_bound + dfvs.size() >= upper_bound.size() ) return 1;

    while (not_yet_fully_reduced){
        not_yet_fully_reduced = 0;

        // check for naive lower_bound while reducing
        if ( dfvs.size() >= upper_bound.size() ) return 1;

        // Assumption: no selfloops exist at this point
        if ( out0(instance) ) not_yet_fully_reduced = 1;
        if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && out1(instance) ) not_yet_fully_reduced = 1;
        if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && loop(instance, dfvs) ) not_yet_fully_reduced = 1;
        if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && in0(instance) ) not_yet_fully_reduced = 1;
        if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && in1(instance) ) not_yet_fully_reduced = 1;
        if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && pie_operation(instance) ) not_yet_fully_reduced = 1;
        if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && core_operation(instance, dfvs) ) not_yet_fully_reduced = 1;
        if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && dome_operation(instance) ) not_yet_fully_reduced = 1;
        if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && shortcut(instance) ) not_yet_fully_reduced = 1;
        if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && flower(instance,upper_bound.size() - dfvs.size() + 1,dfvs) ) not_yet_fully_reduced = 1;
        if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
    }

    // check for lower_bound of reduced graph
    lower_bound = lower_bound_matching(instance);
    if ( lower_bound + dfvs.size() >= upper_bound.size() ) return 1;

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    return 0;
}

// not up-to-date
int Graph_reduce::reduce_graph(Graph_Instance &instance, vector<int> &dfvs, vector<int> &upper_bound){
    //++Logs::reduce_graph;
    int not_yet_fully_reduced = 1;

    // check for lower_bound of unreduced graph
    int lower_bound = lower_bound_matching(instance);
    if ( lower_bound + dfvs.size() >= upper_bound.size() ) return 1;

    while (not_yet_fully_reduced){
        not_yet_fully_reduced = 0;

        // check for naive lower_bound while reducing
        if ( dfvs.size() >= upper_bound.size() ) return 1;

        // Assumption: no selfloops exist at this point
        if ( out0(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && out1(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && loop(instance, dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && in0(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && in1(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && pie_operation(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && core_operation(instance, dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && dome_operation(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && shortcut(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && flower(instance,upper_bound.size() - dfvs.size() + 1,dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
    }

    // check for lower_bound of reduced graph
    lower_bound = lower_bound_matching(instance);
    if ( lower_bound + dfvs.size() >= upper_bound.size() ) return 1;

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    return 0;
}

int Graph_reduce::reduce_graph(Graph_Instance &instance, vector<int> &dfvs) {
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    int not_yet_fully_reduced = 1;
    while (not_yet_fully_reduced){
        not_yet_fully_reduced = 0;

        // Assumption: no selfloops exist at this point
        if ( out0(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && out1(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && loop(instance, dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && in0(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && in1(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && pie_operation(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && core_operation(instance, dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && dome_operation(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        if ( !not_yet_fully_reduced && shortcut(instance) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

        // if ( !not_yet_fully_reduced && flower(instance,upper_bound.size() + 1,dfvs) ) not_yet_fully_reduced = 1;
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
        for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
    }

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    return 0;
}

// deletes all vertices with in-degree 0
int Graph_reduce::in0(Graph_Instance &instance){
    //++Logs::in0;
    int reduced = 0;
    // find vertices
    vector<int> not_found(instance.N, 1);
    for (vector<int> v : instance.graph) {
        for (int u : v) {
            not_found[u] = 0;
        }
    }
    // delete vertices
    vector<int> set;
    for (int i = 0; i < not_found.size(); ++i) {
        if ( not_found[i] ) {
            reduced = 1;
            set.push_back(i);
        }
    }
    if ( reduced ) {
        //++Logs::in0_red;
        assert(is_sorted(begin(set),end(set)));
        delete_vertex_set(set,instance);
    }
    return reduced;
}

// deletes all vertices with out-degree 0
int Graph_reduce::out0(Graph_Instance &instance){
    //++Logs::out0;
    int reduced = 0;
    vector<int> set(0);
    for ( int i = 0; i < instance.N; ++i ) {
        if ( instance.graph[i].empty() ) {
            set.push_back(i);
            reduced = 1;
        }
    }

    if ( reduced ) {
        assert(is_sorted(begin(set),end(set)));
        delete_vertex_set(set,instance);
        //++Logs::out0_red;
    }
    return reduced;
}

// adds all vertices with self-loop do dfvs
int Graph_reduce::loop(Graph_Instance &instance, vector<int> &dfvs){
    //++Logs::loop;
    int reduced = 0;
    vector<int> set;

    for (int i = 0; i < instance.N; ++i) {
        if (isin(instance.graph[i], i)) {
            reduced = 1;
            set.push_back(i);
            dfvs.push_back(instance.map[i]);
        }
    }

    if ( reduced ) {
        assert(is_sorted(begin(set),end(set)));
        delete_vertex_set(set, instance);
        //++Logs::loop_red;
    }
    return reduced;
}

// merges all vertices with in-degree 1 into their predecessor
int Graph_reduce::in1(Graph_Instance &instance){
    //++Logs::in1;
    int reduced = 0;
    vector<int> indegree(instance.N, 0);
    vector<int> predecessor(instance.N);
    vector<pair<int, int>> merges;

    // finds in-degree to every vertex
    for ( int i = 0; i < instance.N; ++i ) {
        for ( int u : instance.graph[i] ) {
            ++indegree[u];
            predecessor[u] = i;        // contains predecessor if indegree == 1
        }
    }

    Graph_Instance tbd(instance.N);
    for (int i = 0; i < instance.N; ++i) {
        if ( indegree[i] != 1 || predecessor[i] == i ) continue;
        reduced = 1;
        int v = i;
        while ( indegree[v] == 1 ) {
            indegree[v] = 0;
            if ( predecessor[v] == i ) break;
            merges.emplace_back(v,predecessor[v]);                  // build merge
            tbd.graph[predecessor[v]].push_back(v);
            v = predecessor[v];
        }
    }

    // delete edges
    counting_sort(tbd);
    for ( int i = 0; i < instance.N; ++i ) {
        auto it_begin = begin(instance.graph[i]);
        auto it_end = end(instance.graph[i]);
        for ( int e : tbd.graph[i] ) {
            it_end = remove(it_begin,it_end,e);
        }
        // auto &v = instance.graph[i];
        instance.graph[i].resize(instance.graph[i].size() - tbd.graph[i].size());
    }

    merge_vertices(merges,instance);
    //if ( reduced ) ++Logs::in1;

    return reduced;
}

void out1_rec(int i, vector<pair<int,int>> &merges, vector<int> &found, vector<int> &gets_merged,
              Graph_Instance &instance) {
    vector<int> &v = instance.graph[i];
    if ( !gets_merged[i] && v.size() == 1 && v[0] != i ) {
        found[i] = 1;
        gets_merged[i] = 1;
        merges.push_back({i,v[0]});
        if ( found[v[0]] ) v.push_back(i);
        int j = v[0];
        v.erase(begin(v));
        out1_rec(j,merges,found,gets_merged,instance);
    }
}

// merges all vertices with out-degree 1 into their successor
int Graph_reduce::out1(Graph_Instance &instance){
    //++Logs::out1;
    int reduced = 0;
    // vector<int> has_out1(instance.N, 0);

    vector<int> gets_merged(instance.N,0);
    vector<int> found;
    vector<pair<int,int>> merges;
    for (int i = 0; i < instance.N; ++i) {
        auto &v = instance.graph[i];
        if ( !gets_merged[i] && v.size() == 1 && v[0] != i ) {
            reduced = 1;
            found = vector<int>(instance.N,0);
            out1_rec(i,merges,found,gets_merged,instance);
        }
    }

    merge_vertices(merges,instance);
    //if ( reduced ) ++Logs::out1_red;

    return reduced;
}

// sorts edges in ascending order in O(|E|+r)
void Graph_reduce::counting_sort(Graph_Instance &instance) {
    //++Logs::counting_sort;
    vector<vector<int>> g = instance.graph;

    // initialize count
    vector<vector<int>> count(instance.N);
    for ( int i = 0; i < instance.N; ++i ) {
        // maybe instance.N instead of find_max
        auto &v =  instance.graph[i];
        int max = -1;
        if ( !v.empty() ) max = *max_element(v.begin(), v.end());
        count[i] = vector<int>(max + 1, 0);
    }

    // count numbers
    for ( int i = 0; i < instance.N; ++i ) {
        for (int j = 0; j < instance.graph[i].size(); ++j) {
            ++count[i][instance.graph[i][j]];
        }
    }

    // calculate addresses
    for ( int i = 0; i < instance.N; ++i ) {
        for ( int j = 1; j < count[i].size(); ++j ) {
            count[i][j] += count[i][j-1];
        }
    }

    for ( int i = 0; i < instance.N; ++i ) {
        for ( int j = 0; j < g[i].size(); ++j ) {
            instance.graph[i][count[i][g[i][j]] - 1] = g[i][j];
            --count[i][g[i][j]];
        }
    }
}

// calculates the PIE of instance and saves it into g_PIE
// g_PIE has to have instance.N empty vertices
// does not check for double edges
void Graph_reduce::calculate_pie(Graph_Instance &instance, Graph_Instance &g_PIE) {
    //++Logs::calculate_pie;
    // countingsort
    // counting_sort(instance);

    vector<int> pointer(instance.N,0);

    for ( int v = 0; v < instance.N; ++v ) {
        for ( int u : instance.graph[v] ) {
            while ( v != u &&
                    pointer[u] < instance.graph[u].size() &&
                    instance.graph[u][pointer[u]] < v ) {

                ++pointer[u];
            }

            if ( v != u &&
                 pointer[u] < instance.graph[u].size() &&
                 instance.graph[u][pointer[u]] == v )
                g_PIE.graph[v].push_back(u);
        }
    }
}

void Graph_reduce::calculate_notpie(Graph_Instance &instance, Graph_Instance &g_not_PIE) {
    //++Logs::calculate_notpie;
    // countingsort
    // counting_sort(instance);

    vector<int> pointer(instance.N,0);

    for ( int v = 0; v < instance.N; ++v ) {
        for ( int u : instance.graph[v] ) {
            while ( v != u &&
                    pointer[u] < instance.graph[u].size() &&
                    instance.graph[u][pointer[u]] < v ) {

                ++pointer[u];
            }

            if ( !( v != u &&
                    pointer[u] < instance.graph[u].size() &&
                    instance.graph[u][pointer[u]] == v ) )
                g_not_PIE.graph[v].push_back(u);
        }
    }
}

void Graph_reduce::calculate_pie_notpie(Graph_Instance &instance, Graph_Instance &g_PIE, Graph_Instance &g_not_PIE) {
    //++Logs::calculate_pie_notpie;
    // countingsort
    // counting_sort(instance);

    vector<int> pointer(instance.N,0);

    for ( int v = 0; v < instance.N; ++v ) {
        for ( int u : instance.graph[v] ) {
            while ( v != u &&
                    pointer[u] < instance.graph[u].size() &&
                    instance.graph[u][pointer[u]] < v ) {

                ++pointer[u];
            }

            if ( v != u &&
                 pointer[u] < instance.graph[u].size() &&
                 instance.graph[u][pointer[u]] == v )
                g_PIE.graph[v].push_back(u);
            else g_not_PIE.graph[v].push_back(u);
        }
    }
}

// from https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=833199
// possible in O(|V|+|E|)
int Graph_reduce::pie_operation(Graph_Instance &instance){
    //++Logs::pie_op;
    int reduced = 0;

    //vector<vector<int>> g_without_piEdges = instance.graph;
    Graph_Instance g_without_piEdges = Graph_Instance(instance.N);
    calculate_notpie(instance,g_without_piEdges);

    // calculates SCCs of G\PIE
    SCC_compute sc = SCC_compute(g_without_piEdges);
    vector<int> coloration(g_without_piEdges.N, 0);
    sc.getSCCcoloration(coloration);

    // deletes all edges between SCCs of G\PIE in instance
    vector<vector<int>> tbd(instance.N);
    for (int i = 0; i < g_without_piEdges.N; ++i) {
        for (int u : g_without_piEdges.graph[i]) {
            // if the color of vertex i and its successor u differs: delete the edge (i,u)
            if (coloration[i] != coloration[u]) {
                tbd[i].push_back(u);
                reduced = 1;
            }
        }
    }

    if ( !reduced ) return reduced;

    for ( int i = 0; i < instance.N; ++i ) {
        auto it_begin = begin(instance.graph[i]);
        auto it_end = end(instance.graph[i]);
        for ( int e : tbd[i] ) {
            it_end = remove(it_begin,it_end,e);
        }
        // auto &v = instance.graph[i];
        instance.graph[i].resize(instance.graph[i].size() - tbd[i].size());
    }
    //++Logs::pie_op_red;

    return reduced;
}

// from https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=833199
// possible in O(|E|+|V|log|V|)
int Graph_reduce::core_operation(Graph_Instance &instance, vector<int> &dfvs){
    //++Logs::core_op;
    int reduced = 0;

    // get sorted PIV
    Graph_Instance g_PIE(instance.N);
    calculate_pie(instance,g_PIE);

    vector<int> degree(instance.N, 0);
    for (int i = 0; i < instance.N; ++i) {
        degree[i] += instance.graph[i].size();
        for (int j = 0; j < instance.graph[i].size(); ++j)
            ++degree[instance.graph[i][j]];
    }

    // PIV is a set of pairs (d,v) with v in PIV and d = degree(v)
    vector<pair<int,int>> PIV(0);
    for (int i = 0; i < instance.N; ++i)
        if ( degree[i] == (2 * g_PIE.graph[i].size()) && degree[i] != 0 )
            PIV.push_back({degree[i],i});

    sort(begin(PIV),end(PIV));

    // foreach v in PIV
    vector<int> valid(instance.N,1);
    vector<int> tbd(0);
    vector<int> new_dfvs(0);

    for ( auto [d,v] : PIV ) {
        // if clique
        if ( !valid[v] ) continue;
        if ( is_clique(v,g_PIE) ) {
            reduced = 1;
            tbd.push_back(v);
            valid[v] = 0;
            for ( int u : g_PIE.graph[v] ) {
                new_dfvs.push_back(instance.map[u]);
                tbd.push_back(u);
                valid[u] = 0;
            }
        }
        else {
            valid[v] = 0;
            for ( int u : g_PIE.graph[v] ) {
                valid[u] = 0;
            }
        }
    }

    if (reduced) {
        remove_duplicates(tbd);
        delete_vertex_set(tbd, instance);

        remove_duplicates(new_dfvs);
        move(new_dfvs.begin(), new_dfvs.end(), back_inserter(dfvs));

        //++Logs::core_op_red;
    }

    return reduced;
}

// set & subset have to be sorted
int Graph_reduce::is_subset(vector<int> &set, vector<int> &subset) {
    assert( is_sorted(begin(set),end(set)) );
    assert( is_sorted(begin(subset),end(subset)) );
    if ( subset.size() > set.size() ) return 0;
    if ( subset.empty() ) return 1;
    auto sub_it = begin(subset);
    auto set_it = begin(set);
    while ( set_it != end(set) ) {
        if ( *sub_it == *set_it++ ) ++sub_it;
        if ( sub_it == end(subset) ) return 1;
    }
    return 0;
}

// from https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=833199
// possible in O(|E||V|)
int Graph_reduce::dome_operation(Graph_Instance &instance){
    //++Logs::dome_op;
    int reduced = 0;
    // berechne g_PIV, G\PIV
    Graph_Instance g_piV = Graph_Instance(instance.N);
    Graph_Instance g_notpi = Graph_Instance(instance.N);

    calculate_pie_notpie(instance,g_piV,g_notpi);

    // int condition2_satisfied = 0;
    vector<vector<int>> tbd(instance.N);
    // for each edge (u,v)
    // should be (v,u)
    for ( int v = 0; v < instance.N; ++v ) {
        for ( int j = 0; j < instance.graph[v].size(); ++j ) {
            // condition2_satisfied = 0;
            int u = instance.graph[v][j];
            // not in original paper but has to be...
            if ( isin(g_piV.graph[v], u) ) continue;

            // check for condition 2, mark edge if necessary
            vector<int> s_v = g_notpi.graph[u];
            vector<int> s_u = instance.graph[v];

            if ( is_subset(s_u,s_v) ) {
                tbd[v].push_back(instance.graph[v][j]);
                // condition2_satisfied = 1;
                reduced = 1;
                continue;
            }

            // check for condition 2, mark edge if necessary
            // if ( condition2_satisfied ) continue;

            vector<int> p_u(0);
            vector<int> p_v(0);
            for ( int i = 0; i < instance.N; ++i ) {
                if ( isin(g_notpi.graph[i], v) ) p_u.push_back(i);
                if ( isin(instance.graph[i], u) ) p_v.push_back(i);
            }

            if ( is_subset(p_v,p_u) ) {
                tbd[v].push_back(instance.graph[v][j]);
                reduced = 1;
            }
        }
    }

    if ( !reduced ) return reduced;

    for ( int i = 0; i < instance.N; ++i ) {
        auto it_begin = begin(instance.graph[i]);
        auto it_end = end(instance.graph[i]);
        for ( int e : tbd[i] ) {
            it_end = remove(it_begin,it_end,e);
        }
        // auto &v = instance.graph[i];
        instance.graph[i].resize(instance.graph[i].size() - tbd[i].size());
    }
    //++Logs::dome_op_red;

    return reduced;
}


int Graph_reduce::path_finder(Graph_Instance &instance, int s, int t, vector<int> &parent) {
    //++Logs::path_finder;
    vector<int> found(instance.N,0);
    queue<int> q;
    q.push(s);
    while ( !q.empty() ) {
        int v = q.front();
        q.pop();
        for ( int u : instance.graph[v] ) {
            if ( found[u] ) continue;
            parent[u] = v;
            if ( u == t ) return 1;
            found[u] = 1;
            q.push(u);
        }
    }

    return 0;
}

// instance has to be sorted
// s has to be a source, t a sink
// s, t have to be in instance
int Graph_reduce::has_unique_path(Graph_Instance &instance, int s, int t) {
    //++Logs::has_unique_path;
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    vector<int> parent(instance.N, -1);
    vector<int> parent2(instance.N,-1);
    // find first path (saved in parent)
    // if no path exists return 0

    if ( !path_finder(instance, s, t, parent) ) return 0;

    // build residual graph
    int i = t;
    while ( i != s ) {
        //instance.graph[i].push_back(parent[i]);
        auto it = begin(instance.graph[i]);
        if ( !instance.graph[i].empty() )
            it = lower_bound(it, end(instance.graph[i]), parent[i]);
            //Vertex_op::get_pos(parent[i],it, end(instance.graph[i]));
        instance.graph[i].insert(it,parent[i]);
        auto &v = instance.graph[parent[i]];
        v.erase(remove(begin(v),end(v),i),end(v));
        i = parent[i];
    }
    for ( auto &v : instance.graph )
        assert(is_sorted(begin(v), end(v)));

    // if augmenting path exists return 0, else 1
    int return_value = 1;
    if ( path_finder(instance,s,t,parent2) ) return_value = 0;

    //reverse all changes to instance
    i = t;
    while ( i != s ) {
        auto &v = instance.graph[parent[i]];
        auto &u = instance.graph[i];
        auto it = begin(u);
        if ( !u.empty() ) {
            it = lower_bound(it,end(u),parent[i]);
            //Vertex_op::get_pos(parent[i],it, end(u));
            if ( it != end(u) ) u.erase(it);
        }
        it = begin(v);
        if ( !v.empty() ) it = lower_bound(it,end(v),i);
        //Vertex_op::get_pos(i,it, end(v));
        v.insert(it,i);
        i = parent[i];
    }
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));

    return return_value;
}

// returns 1 if there are at least k edge disjoint paths between s and t, 0 otherwise
// s has to be a source, t a sink
// s, t have to be in instance
int Graph_reduce::has_k_paths(Graph_Instance &instance, int k, int s, int t) {
    //++Logs::has_k_paths;
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    int return_Value = 0;
    vector<vector<int>> parent(0);
    parent.push_back(vector(instance.N,-1));
    int paths = 0;

    while ( path_finder(instance,s,t,parent[paths]) ) {
        if ( paths >= k - 1 ) {
            return_Value = 1;
            break;
        }

        // build residual graph
        int i = t;
        while ( i != s ) {
            //instance.graph[i].push_back(parent[i]);
            auto it = begin(instance.graph[i]);
            if ( !instance.graph[i].empty() )
                it = lower_bound(it, end(instance.graph[i]), parent[paths][i]);
            instance.graph[i].insert(it,parent[paths][i]);
            auto &v = instance.graph[parent[paths][i]];
            v.erase(remove(begin(v),end(v),i),end(v));
            i = parent[paths][i];
        }

        // reset parent
        ++paths;
        parent.push_back(vector<int>(instance.N,-1));
        for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    }

    // reverse changes to residual graph
    for (int j = parent.size() - 2; j >= 0; --j) {
        int i = t;
        while ( i != s ) {
            auto &v = instance.graph[parent[j][i]];
            auto &u = instance.graph[i];
            auto it = begin(u);
            if ( !u.empty() ) {
                it = lower_bound(it,end(u),parent[j][i]);
                if ( it != end(u) ) u.erase(it);
            }
            it = begin(v);
            if ( !v.empty() ) it = lower_bound(it,end(v),i);
            v.insert(it,i);
            i = parent[j][i];
        }
    }

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    return return_Value;
}

int Graph_reduce::shortcut(Graph_Instance &instance) {
    //++Logs::shortcut;
    int reduced = 0;
    // reduce disjoint vertex path to disjoint edge path
    Graph_Instance min_cut(instance.N * 2);
    for (int i = 0; i < instance.N; ++i) {
        min_cut.graph[i] = {instance.N + i};
        min_cut.graph[instance.N + i] = instance.graph[i];
    }

    // check for every vertex v if petal(v)=1
    for (int i = instance.N - 1; i >= 0; --i) {
        if ( isin(instance.graph[i],i) ) continue;
        min_cut.graph[i] = vector<int>(0);
        if ( has_unique_path(min_cut,i + instance.N,i) ) {
            reduced = 1;
            min_cut.graph[i] = min_cut.graph[i + instance.N];
            delete_vertex(i + instance.N,min_cut);
            replace_vertex(i,min_cut);
            replace_vertex(i,instance);
        }
        else min_cut.graph[i] = vector<int>(1,i + instance.N);
    }

    // handle obsolete vertices
    // kann man nicht machen weil gegenbeispiel
    // kann es noch Knoten geben, die auf keinem Kreis liegen?
    /*
    if ( reduced ) {
        assert(is_sorted(begin(set),end(set)));
        for (int i = set.size() - 1; i >= 0; --i) {
            if ( isin(instance.graph[set[i]],set[i]) ) continue;
            replace_vertex(set[i],instance);
        }
    }
    */
    //if ( reduced ) ++Logs::shortcut_red;
    return reduced;
}

int Graph_reduce::flower(Graph_Instance &instance, int k, vector<int> &dfvs) {
    //++Logs::flower;
    int reduced = 0;
    // reduce disjoint vertex path to disjoint edge path
    Graph_Instance min_cut(instance.N * 2);
    for (int i = 0; i < instance.N; ++i) {
        min_cut.graph[i] = {instance.N + i};
        min_cut.graph[instance.N + i] = instance.graph[i];
    }

    // check for every vertex v if petal(v) >= |k|
    vector<int> set(0);
    for (int i = 0; i < instance.N; ++i) {
        if ( isin(instance.graph[i],i) ) continue;
        min_cut.graph[i] = vector<int>(0);
        if ( has_k_paths(min_cut,k,i + instance.N,i) ) {
            reduced = 1;
            dfvs.push_back(instance.map[i]);
            set.push_back(i);
            // delete_vertex(i + instance.N,min_cut);
            // delete_vertex(i,min_cut);
            // delete_vertex(i,instance);
        }
        else min_cut.graph[i] = vector<int>(1,i + instance.N);
    }

    assert(is_sorted(begin(set),end(set)));
    delete_vertex_set(set,instance);
    //if ( reduced ) ++Logs::flower_red;
    return reduced;
}