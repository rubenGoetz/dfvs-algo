#include <iostream>
#include <vector>
#include <csignal>
#include "Graph_reduce.h"
#include "Branch_and_bound.h"
#include "Parser.h"
using namespace std;
using namespace Parser;
using namespace Branch_and_bound;

vector<int> dfvs_upper_bound(0);

void signalHandler( int signum ) {
    write_output(dfvs_upper_bound);
    exit(signum);
}

int main(int argc, char *argv[]) {
    // register signal SIGINT and signal handler
    signal(SIGINT, signalHandler);

    int setrlimit(int RLIMIT_STACK, const struct rlimit *RLIM_INFINITY);
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    // get Input
    Graph_Instance instance(0);
    read_input(instance);

    // init
    vector<int> current_dfvs(0);
    dfvs_upper_bound = instance.map;

    // solve
    Graph_reduce::counting_sort(instance);
    solve(current_dfvs,instance,dfvs_upper_bound);

    // print output
    write_output(dfvs_upper_bound);
    return 0;
}