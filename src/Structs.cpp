//
// Created by Ruben Götz on 11.02.2022.
//

#ifndef SOME_STRING_H
#define SOME_STRING_H
#include <vector>
#include <numeric>

using namespace std;

struct Graph_Instance {
    int N;
    vector<vector<int>> graph;
    vector<int> map;
    Graph_Instance(int n) : graph(n), N(n) {};
};

struct DSF {
    vector<int> parent;
    DSF(int n) : parent(n) { iota(begin(parent),end(parent),0); }
    void join(int u, int v) { parent[find(u)] = find(v); }
    int find(int v) {
        if(parent[v] == v) return v;
        return parent[v] = find(parent[v]);
    }
};

#endif