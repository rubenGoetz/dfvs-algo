//
// Created by Ruben Götz on 20.12.2021.
//

#ifndef DFVS_GRAPH_REDUCE_H
#define DFVS_GRAPH_REDUCE_H
#include <vector>
#include <ctime>
#include <chrono>
#include "Vertex_op.h"
using namespace std;

// nach der Idee von https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=833199
namespace Graph_reduce {
    // development
    int reduce_graph(Graph_Instance &instance, vector<int> &dfvs, vector<int> &upper_bound,
                     chrono::steady_clock::time_point &timeout);
    // production
    int reduce_graph(Graph_Instance &instance, vector<int> &dfvs, vector<int> &upper_bound);
    // no checks for bounds
    int reduce_graph(Graph_Instance &instance, vector<int> &dfvs);
    int in0(Graph_Instance &instance);
    int out0(Graph_Instance &instance);
    int loop(Graph_Instance &instance, vector<int> &dfvs);
    int in1(Graph_Instance &instance);
    int out1(Graph_Instance &instance);
    int pie_operation(Graph_Instance &instance);
    int core_operation(Graph_Instance &instance, vector<int> &dfvs);
    int dome_operation(Graph_Instance &instance);

    void counting_sort(Graph_Instance &instance);
    void calculate_pie(Graph_Instance &instance, Graph_Instance &g_PIE);
    void calculate_notpie(Graph_Instance &instance, Graph_Instance &g_not_PIE);
    void calculate_pie_notpie(Graph_Instance &instance, Graph_Instance &g_PIE, Graph_Instance &g_not_PIE);

    // ======================
    int path_finder(Graph_Instance &instance, int s, int t, vector<int> &parent);
    int has_unique_path(Graph_Instance &instance, int s, int t);
    int shortcut(Graph_Instance &instance);
    int has_k_paths(Graph_Instance &instance, int k, int s, int t);
    int flower(Graph_Instance &instance, int k, vector<int> &dfvs);
    // ======================

    int is_clique(int v, Graph_Instance &g_PIE);
    int isin(vector<int> &vertex, int e);
    int isin(vector<int> &vertex, int e, int &pos);
    int is_subset(vector<int> &set, vector<int> &subset);
    void remove_duplicates(vector<int> &v);

    int has_k_cycle(Graph_Instance &instance, int start, int k, vector<int> &cycle);
    void calc_common_neighbor(Graph_Instance &undirected, vector<pair<int,int>> &edges, vector<int> &comm_neigh);
    void update_edges(vector<pair<int,int>> &edges, int index);
    void update_node_list(vector<vector<int>> &node_list, Graph_Instance &undirected, int i, int j);
    int remove_max_clique(Graph_Instance &instance, Graph_Instance &gPIE);
    int lower_bound_reduction(Graph_Instance &instance);
    int lower_bound_matching(Graph_Instance &instance);
};


#endif //DFVS_GRAPH_REDUCE_H
