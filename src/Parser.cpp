//
// Created by Ruben Götz on 06.01.2022.
//

#include "Parser.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>

using namespace std;

int Parser::extract_graph(string path, Graph_Instance &instance){
    ifstream graph_file;
    graph_file.open(path, fstream::in);
    string current_line = "";
    string next_number = "";
    int n, e;

    // handle header lines
    if ( graph_file ) getline(graph_file, current_line);
    while ( current_line[0] == '%' ) {
        getline(graph_file, current_line);      // skips comments
    }

    int i = -1;
    while ( current_line[++i] != ' ' ) {              // reads out number of vertices
        if ( !isdigit(current_line[i]) ) return 0;
        next_number += current_line[i];;
    }
    n = stoi(next_number);
    int n_actual = 0;

    next_number = "";
    while ( current_line[++i] != ' ' ) {              // reads out number of edges
        if ( !isdigit(current_line[i]) ) return 0;
        next_number += current_line[i];
    }
    e = stoi(next_number);
    int e_actual = 0;

    if ( current_line[++i] != '0') return 0;            // identifier t has to be 0

    getline(graph_file, current_line);
    while ( graph_file ) {                          // for each line
        if ( current_line[0] != '%' ){              // if not a comment create new vertex and add to graph
            ++n_actual;
            vector<int> current_vertex(0);
            next_number = "";
            for (int j = 0; j < current_line.size(); ++j) {
                switch (current_line[j]) {
                    case ' ':
                        current_vertex.push_back(stoi(next_number) - 1);
                        next_number = "";
                        ++e_actual;
                        break;
                    default:
                        if ( !isdigit(current_line[j]) ) return 0;
                        next_number += current_line[j];
                        break;
                }
            }
            if ( next_number != "" ) {
                current_vertex.push_back(stoi(next_number) - 1);
                ++e_actual;
            }
            instance.graph.push_back(current_vertex);
        }
        getline(graph_file, current_line);
    }

    instance.map.resize(n_actual);
    for (int j = 0; j < n_actual; ++j) {
        instance.map[j] = j;
    }

    instance.N = n_actual;

    if ( e != e_actual || n != n_actual) return 0;
    return 1;
}

int Parser::output_dfvs(string path, vector<int> &dfvs)  {
    ifstream check;
    check.open(path);
    if ( check ) return 0;      // checks if file already exists

    ofstream output;
    output.open(path);
    if ( !output ) return 0;    // checks if file is open

    for (int i = 0; i < dfvs.size(); ++i) {
        output << dfvs[i] + 1 << endl;
    }
    output.close();

    return 1;
}

void Parser::read_input(Graph_Instance &instance) {
    string current_line = "";
    string next_number = "";
    int n, e;

    // handle header lines
    getline(cin,current_line);
    while ( current_line[0] == '%' ) {
        getline(cin, current_line);      // skips comments
    }

    // get n and e
    istringstream ss(current_line);
    ss >> n >> e;

    // create instance
    instance = Graph_Instance(n);
    instance.map.resize(n);

    // get edges
    int i = 0;
    while ( i < n ) {
        getline(cin, current_line);
        if ( current_line[0] == '%') continue;      // check for comment
        istringstream ss(current_line);

        instance.graph[i] = vector<int>(istream_iterator<int>{ss}, istream_iterator<int>{});
        for ( int &edge : instance.graph[i] ) --edge;     // correct indizes

        instance.map[i] = i;
        ++i;
    }
}

void Parser::write_output(vector<int> &solution) {
    for ( int e : solution )
        cout << ++e << "\n";
}

// does not check for correct input format!
void Parser::get_solution_file(string path, vector<int> &solution) {
    fstream file;
    file.open(path);
    while ( file ) {
        string number;
        file >> number;
        if ( number == "") break;
        solution.resize(solution.size() + 1);
        solution.at(solution.size() - 1) = stoi(number) - 1;
    }
    file.close();
}