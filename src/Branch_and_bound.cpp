//
// Created by Ruben Götz on 19.01.2022.
//

#include "Branch_and_bound.h"
#include "Graph_reduce.h"
#include "Vertex_op.h"
#include "SCC_compute.h"
//#include "../unit_tests/Logs.h"
#include <assert.h>
#include <algorithm>
#include <vector>
#include <ctime>
#include <chrono>
#include <stack>
#include "iostream"

using namespace std;
using namespace Graph_reduce;
using namespace Vertex_op;

int Branch_and_bound::in_times_out(Graph_Instance &instance) {
    vector<int> out_degrees(instance.N);
    vector<int> in_degrees(instance.N,0);

    for (int i = 0; i < instance.N; ++i) {
        out_degrees[i] = instance.graph[i].size();
        for (int j = 0; j < out_degrees[i]; ++j) {
            ++in_degrees[instance.graph[i][j]];
        }
    }

    int index = 0;
    int max_weight = 0;
    for (int i = 0; i < instance.N; ++i) {
        if ( max_weight < in_degrees[i] * out_degrees[i] ) {
            max_weight = in_degrees[i] * out_degrees[i];
            index = i;
        }
    }

    return index;
}

int Branch_and_bound::in_times_out(vector<int> &alive,Graph_Instance &instance) {
    vector<int> out_degrees(instance.N);
    vector<int> in_degrees(instance.N,0);

    for (int i = 0; i < instance.N; ++i) {
        if ( !alive[i] ) continue;
        out_degrees[i] = instance.graph[i].size();
        for (int j = 0; j < out_degrees[i]; ++j) {
            ++in_degrees[instance.graph[i][j]];
        }
    }

    int index = 0;
    int max_weight = 0;
    for (int i = 0; i < instance.N; ++i) {
        if ( alive[i] && max_weight < in_degrees[i] * out_degrees[i] ) {
            max_weight = in_degrees[i] * out_degrees[i];
            index = i;
        }
    }

    return index;
}

int Branch_and_bound::get_highest_degree_vertex(Graph_Instance &instance) {
    vector<int> degrees(instance.N, 0);
    for (int i = 0; i < instance.N; ++i) {
        degrees[i] += instance.graph[i].size();
        for (int j = 0; j < instance.graph[i].size(); ++j) {
            degrees[instance.graph[i][j]]++;
        }
    }

    int index_max = 0;
    for (int i = 0; i < degrees.size(); ++i) {
        if ( degrees[i] > degrees[index_max] ) index_max = i;
    }
    return index_max;
}

int Branch_and_bound::get_highest_degree_vertex(vector<int> &alive, Graph_Instance &instance) {
    vector<int> degrees(instance.N, 0);
    for (int i = 0; i < instance.N; ++i) {
        if ( !alive[i] ) continue;
        for (int j = 0; j < instance.graph[i].size(); ++j) {
            if ( !alive[instance.graph[i][j]] ) continue;
            ++degrees[i];
            ++degrees[instance.graph[i][j]];
        }
    }

    int index_max = 0;
    for (int i = 0; i < degrees.size(); ++i) {
        if ( alive[i] && degrees[i] > degrees[index_max] ) index_max = i;
    }
    return index_max;
}

int Branch_and_bound::get_branching_index(Graph_Instance &instance) {
    //++Logs::get_branching_index;
    return get_highest_degree_vertex(instance);
}

int Branch_and_bound::get_branching_index(vector<int> &alive, Graph_Instance &instance) {
    //++Logs::get_branching_index_greedy;
    return in_times_out(alive,instance);
}

int get_lowest_branching_index(Graph_Instance &instance) {
    vector<int> degrees(instance.N, 0);
    for (int i = 0; i < instance.N; ++i) {
        degrees[i] += instance.graph[i].size();
        for (int j = 0; j < instance.graph[i].size(); ++j) {
            degrees[instance.graph[i][j]]++;
        }
    }

    int index_min = 0;
    for (int i = 0; i < degrees.size(); ++i) {
        if ( degrees[i] < degrees[index_min] ) index_min = i;
    }
    return index_min;
}

int Branch_and_bound::is_acyclic_util(int v, vector<int> &visited, vector<int> &s, vector<int> on_stack,
                                      vector<int> &alive, Graph_Instance &instance) {
    visited[v] = 1;
    s.push_back(v);
    on_stack[v] = 1;
    vector<int> &node = instance.graph[v];
    for (int i = 0; i < node.size(); ++i) {
        if ( !alive[node[i]] ) continue;
        if ( on_stack[node[i]] ) return 0;
        if ( !is_acyclic_util(node[i],visited,s,on_stack,alive,instance) ) return 0;
    }
    on_stack[s.back()] = 0;
    s.pop_back();
    return 1;
}

int Branch_and_bound::is_acyclic(vector<int> &alive, Graph_Instance &instance) {
    //++Logs::is_acyclic;
    vector<int> visited(instance.N,0);
    vector<int> s;
    vector<int> on_stack(instance.N,0);

    for (int i = 0; i < instance.N; ++i) {
        if ( alive[i] && !visited[i] ) {
            if ( !is_acyclic_util(i,visited,s,on_stack,alive,instance) ) return 0;
        }
    }

    return 1;
}

void Branch_and_bound::solve_greedy(Graph_Instance &instance, vector<int> &dfvs,
                                    chrono::steady_clock::time_point &timeout) {
    //++Logs::solve_greedy;
    if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");
    vector<int> alive(instance.N, 1);

    while ( true ) {
        // find highest degree vertex and "delete"
        int v = get_branching_index(alive,instance);
        alive[v] = 0;
        dfvs.push_back(instance.map[v]);
        // check if acyclic
        if ( is_acyclic(alive,instance) ) break;
    }
}

void Branch_and_bound::solve_greedy(Graph_Instance &instance, vector<int> &dfvs) {
    //++Logs::solve_greedy;
    vector<int> alive(instance.N, 1);

    while ( true ) {
        // find highest degree vertex and "delete"
        int v = get_branching_index(alive,instance);
        alive[v] = 0;
        dfvs.push_back(instance.map[v]);
        // check if acyclic
        if ( is_acyclic(alive,instance) ) break;
    }
}

// works on a copy of instance
void Branch_and_bound::initial_solution(Graph_Instance &instance, vector<int> &dfvs) {
    Graph_Instance copy = instance;
    while ( instance.N > 0 ) {
        reduce_graph(copy,dfvs);
        if ( copy.N <= 0 ) break;
        replace_vertex(get_lowest_branching_index(copy),copy);
    }
}

void Branch_and_bound::solve(vector<int> current_dfvs, Graph_Instance instance,
                             vector<int> &upper_bound, chrono::steady_clock::time_point &timeout) {
    //++Logs::solve;
    //Logs::current_upper_bound_size(upper_bound.size());
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
    // for development purposes only
    if ( chrono::steady_clock::now() >= timeout ) throw ("timeout");

    //
    // if ( lower_bound + current_dfvs.size() >= upper_bound.size() ) return;
    int lower_bound;
    if ( reduce_graph(instance,current_dfvs,upper_bound,timeout) ) return;

    if ( instance.N <= 0 ) {
        if ( current_dfvs.size() < upper_bound.size() ) upper_bound = current_dfvs;
        return;
    }

    // calculate SCC
    SCC_compute sc = SCC_compute(instance);
    vector<int> coloration(instance.graph.size(), 0);
    sc.getSCCcoloration(coloration);

    // biggest SCC selected as last
    vector color_count(sc.scc_count,0);
    for ( int i = 0; i < instance.N; ++i )
        ++color_count[coloration[i]];
    int index_max = 0;
    for ( int i = 0; i < sc.scc_count; ++i )
        if ( color_count[index_max] < color_count[i] ) index_max = i;
    for (int i = 0; i < instance.N; ++i) {
        if ( coloration[i] == index_max ) coloration[i] = sc.scc_count - 1;
        else if ( coloration[i] == sc.scc_count - 1 ) coloration[i] = index_max;
    }

    Graph_Instance component(0);
    vector<int> current_dfvs_component;
    vector<int> upper_bound_component;
    // if multiple SCCs exist solve them independently
    vector<int> tbd_comp;
    vector<int> tbd_inst;
    vector<int> tbd_col;

    // calculates dfvs for each component except for last one
    for ( int i = 0; i < sc.scc_count - 1; ++i ){
        component = instance;
        current_dfvs_component = {};
        tbd_comp = {};
        tbd_inst = {};
        int color_count = 0;
        for (int j = 0; j < coloration.size(); ++j) {
            if (coloration[j] == i) {
                tbd_inst.push_back(j);
                ++color_count;
            } else tbd_comp.push_back(j);
        }

        remove(begin(coloration),end(coloration),i);
        coloration.resize(coloration.size() - color_count);
        delete_vertex_set(tbd_comp, component);
        delete_vertex_set(tbd_inst, instance);
        upper_bound_component = {};
        solve_greedy(component,upper_bound_component);
        solve(current_dfvs_component, component, upper_bound_component, timeout);
        if ( current_dfvs.size() + current_dfvs_component.size() >= upper_bound.size() ) break;
        move(begin(upper_bound_component),end(upper_bound_component),back_inserter(current_dfvs));
    }

    // select vertex
    int vertex = get_branching_index(instance);

    // create left branch
    Graph_Instance component_left = instance;
    vector<int> current_dfvs_left = current_dfvs;
    current_dfvs_left.push_back(instance.map[vertex]);

    Vertex_op::delete_vertex(vertex,component_left);
    solve(current_dfvs_left,component_left,upper_bound,timeout);

    // create right branch
    Graph_Instance &component_right = instance;
    vector<int> current_dfvs_right = current_dfvs;

    Vertex_op::replace_vertex(vertex,component_right);
    solve(current_dfvs_right,component_right,upper_bound,timeout);
}

void Branch_and_bound::solve(vector<int> current_dfvs, Graph_Instance instance,
                             vector<int> &upper_bound) {
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    // if ( lower_bound + current_dfvs.size() >= upper_bound.size() ) return;
    int lower_bound;
    if ( reduce_graph(instance,current_dfvs,upper_bound) ) return;

    if ( instance.N <= 0 ) {
        if ( current_dfvs.size() < upper_bound.size() ) upper_bound = current_dfvs;
        return;
    }

    // calculate SCC
    SCC_compute sc = SCC_compute(instance);
    vector<int> coloration(instance.graph.size(), 0);
    sc.getSCCcoloration(coloration);

    // biggest SCC selected as last
    vector color_count(sc.scc_count,0);
    for ( int i = 0; i < instance.N; ++i )
        ++color_count[coloration[i]];
    int index_max = 0;
    for ( int i = 0; i < sc.scc_count; ++i )
        if ( color_count[index_max] < color_count[i] ) index_max = i;
    for (int i = 0; i < instance.N; ++i) {
        if ( coloration[i] == index_max ) coloration[i] = sc.scc_count - 1;
        else if ( coloration[i] == sc.scc_count - 1 ) coloration[i] = index_max;
    }

    Graph_Instance component(0);
    vector<int> current_dfvs_component;
    vector<int> upper_bound_component;
    // if multiple SCCs exist solve them independently
    vector<int> tbd_comp;
    vector<int> tbd_inst;
    vector<int> tbd_col;

    // calculates dfvs for each component except for last one
    for ( int i = 0; i < sc.scc_count - 1; ++i ){
        component = instance;
        current_dfvs_component = {};
        tbd_comp = {};
        tbd_inst = {};
        int color_count = 0;
        for (int j = 0; j < coloration.size(); ++j) {
            if (coloration[j] == i) {
                tbd_inst.push_back(j);
                ++color_count;
            } else tbd_comp.push_back(j);
        }

        remove(begin(coloration),end(coloration),i);
        coloration.resize(coloration.size() - color_count);
        delete_vertex_set(tbd_comp, component);
        delete_vertex_set(tbd_inst, instance);
        upper_bound_component = {};
        solve_greedy(component,upper_bound_component);
        solve(current_dfvs_component, component, upper_bound_component);
        if ( current_dfvs.size() + current_dfvs_component.size() >= upper_bound.size() ) break;
        move(begin(upper_bound_component),end(upper_bound_component),back_inserter(current_dfvs));
    }

    // select vertex
    int vertex = get_branching_index(instance);

    // create left branch
    Graph_Instance component_left = instance;
    vector<int> current_dfvs_left = current_dfvs;
    current_dfvs_left.push_back(instance.map[vertex]);

    Vertex_op::delete_vertex(vertex,component_left);
    solve(current_dfvs_left,component_left,upper_bound);

    // create right branch
    Graph_Instance &component_right = instance;
    vector<int> current_dfvs_right = current_dfvs;

    Vertex_op::replace_vertex(vertex,component_right);
    solve(current_dfvs_right,component_right,upper_bound);
}
