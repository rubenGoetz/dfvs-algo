//
// Created by Ruben Götz on 20.12.2021.
//

#include "Vertex_op.h"
#include "Graph_reduce.h"
#include "Structs.cpp"
//#include "../unit_tests/Logs.h"
#include <assert.h>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
using namespace Graph_reduce;


// returns iterater to first element >= e in range
// deprecated
vector<int>::iterator Vertex_op::get_pos(int e, vector<int>::iterator low, vector<int>::iterator high) {
    auto mid = low + (high - low) / 2;
    if ( *mid == e ) return mid;
    if ( mid == low ) {
        if ( *low > e ) return low;
        return high;
    }
    if ( *mid < e ) return get_pos(e,mid,high);
    if ( *mid > e ) return get_pos(e,low,mid);
    // should never happen
    return low;
}

// naive Implementierung
void Vertex_op::delete_vertex(int v, Graph_Instance &instance) {
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
    instance.graph.erase(instance.graph.begin() + v);
    if ( !instance.map.empty() ) instance.map.erase(instance.map.begin() + v);
    --instance.N;
    for (int i = 0; i < instance.graph.size(); ++i) {
        auto &curr_vertex = instance.graph[i];
        if ( curr_vertex.empty() ) continue;
        // find first element >= v
        auto it = lower_bound(begin(curr_vertex), end(curr_vertex),v);
        //get_pos(v,begin(curr_vertex), end(curr_vertex));
        if ( it == end(curr_vertex) ) continue;
        if ( *it == v ) it = curr_vertex.erase(it);
        while ( it != end(curr_vertex) ) --(*it++);
    }
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
}

void Vertex_op::delete_vertex(int v, vector<vector<int>> &graph) {
    //++Logs::delete_vertex;
    graph.erase(graph.begin() + v);
    for (int i = 0; i < graph.size(); ++i) {
        for (int j = graph[i].size() - 1; j >= 0; --j) {
            if ( graph[i][j] == v) graph[i].erase(graph[i].begin() + j);
            else if ( graph[i][j] > v ) --graph[i][j];
        }
    }
}

// elements in set have to be unique
// set & instance have to be sorted
void Vertex_op::delete_vertex_set(vector<int> &set, Graph_Instance &instance) {
    //++Logs::delete_vertex_set;
    assert(is_sorted(begin(set),end(set)));
    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));

    if ( instance.N <= 0 ) return;
    if ( set.size() <= 0 ) return;

    vector<int> alive(instance.N,1);
    for (int i = 0; i < set.size(); ++i)
        alive[set[i]] = 0;

    vector pref(instance.N+1, 0); // pref[i] is the num of alive nodes <i
    partial_sum(begin(alive),end(alive),begin(pref)+1);

    vector<vector<int>> new_graph(instance.N - set.size());
    for (int i = 0; i < instance.N; ++i) {
        if ( !alive[i] ) continue;
        auto &v = instance.graph[i];
        for (int j = 0; j < v.size(); ++j) {
            if ( !alive[v[j]] ) continue;
            new_graph[pref[i]].push_back(pref[v[j]]);
        }
    }

    if ( instance.map.size() > 0 ) {
        vector<int> new_map;
        for (int i = 0; i < instance.N; ++i)
            if (alive[i]) new_map.push_back(instance.map[i]);

        instance.map = new_map;
    }

    instance.graph = new_graph;
    instance.N -= set.size();

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
}

// merge v ino u
// creates possibly multiple selfloops
// mehrfachkanten werden nicht abgefangen
void Vertex_op::merge_vertices(int v, int u, Graph_Instance &instance) {
    //++Logs::merge_vertices;
    // ausgehende Kanten von v zu u hinzufügen
    for (int i = 0; i < instance.graph[v].size(); ++i) {
        instance.graph[u].push_back(instance.graph[v][i]);
    }

    // eingehende Kanten von v auf u umbiegen
    for (int i = 0; i < instance.graph.size(); ++i) {
        vector<int>::iterator edge = instance.graph[i].begin();
        for (int j = 0; j < instance.graph[i].size(); ++j) {
            if (instance.graph[i][j] == v) instance.graph[i][j] = u;
        }
    }

    //löschen von v
    delete_vertex(v, instance);
}

void Vertex_op::merge_vertices(const vector<pair<int, int>> &merges, Graph_Instance &instance) {
    //++Logs::merge_vertices_set;
    DSF co(instance.N);
    for(auto [u,v] : merges) co.join(u,v);
    // calc new ids
    vector alive(instance.N,0);
    for(int i=0; i<instance.N; ++i)
        alive[i] = (co.find(i)==i);
    vector pref(instance.N+1, 0); // pref[i] is the num of alive nodes <i
    partial_sum(begin(alive),end(alive),begin(pref)+1);

    // rebuild edges
    // build vector with all edges
    vector<pair<int,int>> edges;
    for(int v=0; v<instance.N; ++v)
        for(auto u : instance.graph[v])
            edges.emplace_back(pref[co.find(v)],pref[co.find(u)]);

    // sort vector
    sort(begin(edges),end(edges)); // TODO maybe c-sort
    // remove duplicates
    edges.erase(unique(begin(edges),end(edges)),end(edges));

    // build adj list again
    auto newN = pref.back();
    vector<vector<int>> adj(newN);
    for(auto [u,v] : edges)
        adj[u].push_back(v);
    instance.graph = adj;

    // update map
    vector<int> new_map;
    for(int i=0; i<instance.N; ++i)
        if(alive[i])
            new_map.push_back(instance.map[i]);
    instance.map = new_map;
    instance.N = newN;

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
}

// replaces a Vertex with edges
void Vertex_op::replace_vertex(int v, Graph_Instance &instance) {
    //++Logs::replace_vertex;
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));

    vector<int> predecessors(0);
    // find every predecessor
    for (int i = 0; i < instance.graph.size(); ++i) {
        auto it = begin(instance.graph[i]);
        while ( it != end(instance.graph[i]) ) {
            if ( *it == v ) {
                predecessors.push_back(i);
                it = instance.graph[i].erase(it);
            }
            else {
                if ( *it > v ) --(*it);
                ++it;
            }
        }
    }
    
    // add every successor to predecessor
    vector<int> &successors = instance.graph[v];
    for ( int p : predecessors ) merge(instance.graph[p],successors);

    // delete v, no selfloops allowed
    instance.graph.erase(begin(instance.graph) + v);
    if (!instance.map.empty()) instance.map.erase(begin(instance.map) + v);
    --instance.N;

    for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
    for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
}

// merges sorted v2 into sorted v1, so that v1 remains sorted
void Vertex_op::merge(vector<int> &v1, vector<int> &v2) {
    auto v1_original = v1;
    auto v2_original = v2;
    assert(is_sorted(begin(v1),end(v1)));
    assert(is_sorted(begin(v2),end(v2)));

    vector<int> new_v(v1.size() + v2.size());
    auto it1 = begin(v1);
    auto it2 = begin(v2);
    for (int i = 0; i < new_v.size(); ++i) {
        if ( it1 == end(v1) ) new_v[i] = *(it2++);
        else if ( it2 == end(v2) ) new_v[i] = *(it1++);

        else if ( *it1 > *it2 ) new_v[i] = *(it2++);
        else if ( *it1 < *it2 ) new_v[i] = *(it1++);

        else {      // does not generate parallel edges
            new_v[i] = *(it1++);
            ++it2;
            new_v.resize(new_v.size() - 1);
        }
    }

    v1 = new_v;

    assert(is_sorted(begin(v1),end(v1)));
    assert(unique(begin(v1), end(v1)) == end(v1));
    for ( int i : v1_original ) assert(isin(v1,i));
    for ( int i : v2_original ) assert(isin(v1,i));
    assert(v2 == v2_original);
}
