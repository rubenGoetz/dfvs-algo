//
// Created by Ruben Götz on 05.01.2022.
//

#include "catch.hpp"
#include "Test_helper.h"
#include "../src/SCC_compute.h"
#include <vector>
#include <map>

using namespace std;
using namespace Test_helper;

TEST_CASE ( "getSCCcoloration_test", "[SCC_compute]" ) {

    SECTION ( "single node" ) {
        vector<int> v0 = {};
        vector<vector<int>> g = {v0};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;

        SCC_compute sc = SCC_compute(instance);
        vector<int> coloration(g.size());

        vector<int> coloration_control = {0};

        sc.getSCCcoloration(coloration);

        REQUIRE ( coloration_equal(coloration, coloration_control) );
        REQUIRE ( coloration_is_normed(coloration) );
    }

    SECTION ( "single node with self loop" ) {
        vector<int> v0 = {0};
        vector<vector<int>> g = {v0};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;

        SCC_compute sc = SCC_compute(instance);
        vector<int> coloration(g.size());

        vector<int> coloration_control = {0};

        sc.getSCCcoloration(coloration);

        REQUIRE ( coloration_equal(coloration, coloration_control) );
        REQUIRE ( coloration_is_normed(coloration) );
    }

    SECTION ( "unconnected nodes" ) {
        vector<int> v0 = {};
        vector<int> v1 = {};
        vector<int> v2 = {};
        vector<int> v3 = {};
        vector<int> v4 = {};
        vector<vector<int>> g = {v0,v1,v2,v3,v4};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;

        SCC_compute sc = SCC_compute(instance);
        vector<int> coloration(g.size());

        vector<int> coloration_control = {0,1,2,3,4};

        sc.getSCCcoloration(coloration);

        REQUIRE ( coloration_equal(coloration, coloration_control) );
        REQUIRE ( coloration_is_normed(coloration) );
    }

    SECTION ( "simple chain" ) {
        vector<int> v0 = {};
        vector<int> v1 = {0};
        vector<int> v2 = {1};
        vector<int> v3 = {2};
        vector<int> v4 = {3};
        vector<vector<int>> g = {v0,v1,v2,v3,v4};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;

        SCC_compute sc = SCC_compute(instance);
        vector<int> coloration(g.size());

        vector<int> coloration_control = {0,1,2,3,4};

        sc.getSCCcoloration(coloration);

        REQUIRE ( coloration_equal(coloration, coloration_control) );
        REQUIRE ( coloration_is_normed(coloration) );
    }

    SECTION ( "simple circle" ) {
        vector<int> v0 = {4};
        vector<int> v1 = {0};
        vector<int> v2 = {1};
        vector<int> v3 = {2};
        vector<int> v4 = {3};
        vector<vector<int>> g = {v0,v1,v2,v3,v4};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;

        SCC_compute sc = SCC_compute(instance);
        vector<int> coloration(g.size());

        vector<int> coloration_control = {0,0,0,0,0};

        sc.getSCCcoloration(coloration);

        REQUIRE ( coloration_equal(coloration, coloration_control) );
        REQUIRE ( coloration_is_normed(coloration) );
    }

    SECTION ( "more complex case" ) {
        vector<int> v0 = {2};
        vector<int> v1 = {0,4};
        vector<int> v2 = {0,1,3};
        vector<int> v3 = {4};
        vector<int> v4 = {3};
        vector<vector<int>> g = {v0,v1,v2,v3,v4};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;

        SCC_compute sc = SCC_compute(instance);
        vector<int> coloration(g.size());

        vector<int> coloration_control = {0,0,0,1,1};

        sc.getSCCcoloration(coloration);

        REQUIRE ( coloration_equal(coloration, coloration_control) );
        REQUIRE ( coloration_is_normed(coloration) );
    }

    SECTION ( "even more complex case" ) {
        vector<int> v0 = {1};
        vector<int> v1 = {2,4,5};
        vector<int> v2 = {3,6};
        vector<int> v3 = {2,7};
        vector<int> v4 = {0,5};
        vector<int> v5 = {6};
        vector<int> v6 = {5};
        vector<int> v7 = {6,3};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5,v6,v7};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;

        SCC_compute sc = SCC_compute(instance);
        vector<int> coloration(g.size());

        vector<int> coloration_control = {0,0,1,1,0,2,2,1};

        sc.getSCCcoloration(coloration);

        REQUIRE ( coloration_equal(coloration, coloration_control) );
        REQUIRE ( coloration_is_normed(coloration) );
    }
}

TEST_CASE ( "clean_coloration_test", "[SCC_compute]") {
    vector<vector<int>> graph = {};

    vector<int> coloration1 = {11,11,2,2,11,5,5,2};
    vector<int> coloration2 = {4,4,4,4};
    vector<int> coloration3 = {};

    vector<int> cleaned_coloration1 = {0,0,1,1,0,2,2,1};
    vector<int> cleaned_coloration2 = {0,0,0,0};

    REQUIRE ( SCC_compute::clean_coloration(coloration1) == 3 );
    REQUIRE ( Test_helper::vector_equal(coloration1,cleaned_coloration1) );

    REQUIRE ( SCC_compute::clean_coloration(coloration2) == 1 );
    REQUIRE ( Test_helper::vector_equal(coloration2,cleaned_coloration2) );

    REQUIRE ( SCC_compute::clean_coloration(coloration3) == 0 );
}