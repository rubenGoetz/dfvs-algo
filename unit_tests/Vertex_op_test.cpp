//
// Created by Ruben Götz on 04.01.2022.
//

#include "catch.hpp"
#include "../src/Vertex_op.h"
#include "Test_helper.h"
#include <vector>
#include <random>
#include <set>
#include <iostream>
using namespace std;
using namespace Test_helper;
using namespace Vertex_op;

TEST_CASE ( "delete_vertex_test", "[Vertex_op]" ) {
    SECTION ( "complex Graph" ) {
        // testgraph
        vector<int> v0 = {2};
        vector<int> v1 = {0, 4};
        vector<int> v2 = {0, 1, 3};
        vector<int> v3 = {4};
        vector<int> v4 = {3};
        vector<int> v5 = {};
        vector<int> v6 = {5, 6};
        vector<vector<int>> g = {v0, v1, v2, v3, v4, v5, v6};

        vector<int> map = {0, 1, 2, 3, 4, 5, 6};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        SECTION ("delete v2") {
            // testgraph without v2
            vector<int> v0_control = {};
            vector<int> v1_control = {0, 3};
            vector<int> v2_control = {3};
            vector<int> v3_control = {2};
            vector<int> v4_control = {};
            vector<int> v5_control = {4, 5};
            vector<vector<int>> g_control = {v0_control, v1_control, v2_control,
                                             v3_control, v4_control, v5_control};

            vector<int> map_control = {0, 1, 3, 4, 5, 6};

            delete_vertex(2,instance);

            REQUIRE ( graph_equal(instance.graph,g_control) );
            REQUIRE ( vector_equal(instance.map,map_control) );
            REQUIRE ( instance.N == g.size() - 1 );
        }

        SECTION ("delete v4") {
            // testgraph without v4
            vector<int> v0_control = {2};
            vector<int> v1_control = {0};
            vector<int> v2_control = {0, 1, 3};
            vector<int> v3_control = {};
            vector<int> v4_control = {};
            vector<int> v5_control = {4, 5};
            vector<vector<int>> g_control = {v0_control, v1_control, v2_control,
                                             v3_control, v4_control, v5_control};

            vector<int> map_control = {0, 1, 2, 3, 5, 6};

            delete_vertex(4,instance);

            REQUIRE ( graph_equal(instance.graph,g_control) );
            REQUIRE ( vector_equal(instance.map,map_control) );
            REQUIRE ( instance.N == g.size() - 1 );
        }

        SECTION ("delete v5") {
            // testgraph without v4
            vector<int> v0_control = {2};
            vector<int> v1_control = {0, 4};
            vector<int> v2_control = {0, 1, 3};
            vector<int> v3_control = {4};
            vector<int> v4_control = {3};
            vector<int> v5_control = {5};
            vector<vector<int>> g_control = {v0_control, v1_control, v2_control,
                                             v3_control, v4_control, v5_control};

            vector<int> map_control = {0, 1, 2, 3, 4, 6};

            delete_vertex(5,instance);

            REQUIRE ( graph_equal(instance.graph,g_control) );
            REQUIRE ( vector_equal(instance.map,map_control) );
            REQUIRE ( instance.N == g.size() - 1 );
        }
    }

    SECTION( "delete only vertex" ) {
        SECTION( "without selfloop" ) {
            vector<vector<int>> g = {{}};

            vector<int> map = {0};

            Graph_Instance instance = Graph_Instance(0);
            instance.graph = g;
            instance.map = map;
            instance.N = g.size();

            vector<vector<int>> g_control = {};

            vector<int> map_control = {};

            delete_vertex(0,instance);

            REQUIRE ( graph_equal(instance.graph,g_control) );
            REQUIRE ( vector_equal(instance.map,map_control) );
            REQUIRE ( instance.N == g.size() - 1 );
        }

        SECTION( "with selfloop" ) {
            vector<vector<int>> g = {{0}};

            vector<int> map = {0};

            Graph_Instance instance = Graph_Instance(0);
            instance.graph = g;
            instance.map = map;
            instance.N = g.size();

            vector<vector<int>> g_control = {};

            vector<int> map_control = {};

            delete_vertex(0,instance);

            REQUIRE ( graph_equal(instance.graph,g_control) );
            REQUIRE ( vector_equal(instance.map,map_control) );
            REQUIRE ( instance.N == g.size() - 1 );
        }
    }
}

TEST_CASE ( "delete_vertex_set_test", "[Vertex_op]" ) {
    SECTION ( "simple case" ) {
        Graph_Instance instance = Graph_Instance(5);
        instance.graph[0] = {0};
        instance.graph[1] = {1};
        instance.graph[2] = {2};
        instance.graph[3] = {3};
        instance.graph[4] = {4};

        instance.map.resize(instance.N);
        for (int i = 0; i < instance.N; ++i) {
            instance.map[i] = i;
        }

        Graph_Instance instance_control = Graph_Instance(3);
        instance_control.graph[0] = {0};
        instance_control.graph[1] = {1};
        instance_control.graph[2] = {2};

        instance_control.map.resize(instance_control.N);
        instance_control.map = {0,2,4};

        vector<int> set = {1, 3};

        delete_vertex_set(set, instance);

        REQUIRE ( instance.N == instance_control.N );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        REQUIRE ( vector_same(instance.map,instance_control.map) );
    }

    SECTION ( "empty set" ) {
        Graph_Instance instance = Graph_Instance(5);
        instance.graph[0] = {0};
        instance.graph[1] = {0,1};
        instance.graph[2] = {2,5};
        instance.graph[3] = {3};
        instance.graph[4] = {0,1,2,3,4};

        instance.map.resize(instance.N);
        for (int i = 0; i < instance.N; ++i) {
            instance.map[i] = i;
        }

        Graph_Instance instance_control = instance;

        vector<int> set(0);

        delete_vertex_set(set, instance);

        REQUIRE ( instance.N == instance_control.N );
        // somehow fails in run/debug but not in debug/debug?!
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        REQUIRE ( vector_same(instance.map,instance_control.map) );
    }

    SECTION ( "empty graph" ) {
        Graph_Instance instance = Graph_Instance(0);

        vector<int> set1 = {};
        vector<int> set2 = {3,5};

        delete_vertex_set(set1,instance);

        REQUIRE ( instance.N == 0 );

        delete_vertex_set(set2,instance);

        REQUIRE ( instance.N == 0 );
    }

    SECTION ( "delete single vertex" ) {
        Graph_Instance instance = Graph_Instance(5);
        instance.graph[0] = {0,1,2,3,4};
        instance.graph[1] = {0,1,2,3,4};
        instance.graph[2] = {0,1,2,3,4};
        instance.graph[3] = {0,1,2,3,4};
        instance.graph[4] = {0,1,2,3,4};

        instance.map.resize(instance.N);
        for (int i = 0; i < instance.N; ++i) {
            instance.map[i] = i;
        }

        Graph_Instance instance_control = Graph_Instance(4);
        instance_control.graph[0] = {0,1,2,3};
        instance_control.graph[1] = {0,1,2,3};
        instance_control.graph[2] = {0,1,2,3};
        instance_control.graph[3] = {0,1,2,3};

        instance_control.map.resize(instance_control.N);
        instance_control.map = {0,1,3,4};

        vector<int> set = {2};

        delete_vertex_set(set, instance);

        REQUIRE ( instance.N == instance_control.N );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        REQUIRE ( vector_same(instance.map,instance_control.map) );
    }

    SECTION ( "delete all vertices" ) {
        Graph_Instance instance = Graph_Instance(5);
        instance.graph[0] = {0,1,2,3,4};
        instance.graph[1] = {0,1,2,3,4};
        instance.graph[2] = {0,1,2,3,4};
        instance.graph[3] = {0,1,2,3,4};
        instance.graph[4] = {0,1,2,3,4};

        instance.map.resize(instance.N);
        for (int i = 0; i < instance.N; ++i) {
            instance.map[i] = i;
        }

        Graph_Instance instance_control = Graph_Instance(0);

        vector<int> set = {0,1,2,3,4};

        delete_vertex_set(set, instance);

        REQUIRE ( instance.N == instance_control.N );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        REQUIRE ( vector_same(instance.map,instance_control.map) );
    }

    SECTION ( "complex case" ) {
        Graph_Instance instance = Graph_Instance(6);
        instance.graph[0] = {2, 3, 4};
        instance.graph[1] = {0, 2, 3, 4};
        instance.graph[2] = {0, 1, 4, 5};
        instance.graph[3] = {0, 2, 4};
        instance.graph[4] = {0, 1, 2};
        instance.graph[5] = {};

        instance.map.resize(instance.N);
        for (int i = 0; i < instance.N; ++i) {
            instance.map[i] = i;
        }

        Graph_Instance instance_control = Graph_Instance(4);
        instance_control.graph[0] = {1, 2};
        instance_control.graph[1] = {0, 3};
        instance_control.graph[2] = {1};
        instance_control.graph[3] = {};

        instance_control.map.resize(instance_control.N);
        instance_control.map = {1, 2, 3, 5};

        vector<int> set = {0, 4};

        delete_vertex_set(set, instance);

        REQUIRE (instance.N == instance_control.N);
        REQUIRE (graph_equal(instance.graph, instance_control.graph));
        REQUIRE (vector_same(instance.map, instance_control.map));
    }
}

/*
TEST_CASE ( "merge_vertices_test", "[Vertex_op]" ) {
    // testgraph
    vector<int> v0 = {2};
    vector<int> v1 = {0,4};
    vector<int> v2 = {0,1,3};
    vector<int> v3 = {4};
    vector<int> v4 = {3};
    vector<vector<int>> g = {v0,v1,v2,v3,v4};

    vector<int> map = {0,1,2,3,4};

    Graph_Instance instance = Graph_Instance(0);
    instance.graph = g;
    instance.map = map;
    instance.N = g.size();

    SECTION ( "merge v0 into v2" ) {
        // testgraph with v0 merged into v2
        vector<int> v0_control = {1,3};
        vector<int> v1_control = {0,1,1,2};
        vector<int> v2_control = {3};
        vector<int> v3_control = {2};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,v3_control};

        vector<int> map_control = {1,2,3,4};

        merge_vertices(0,2,instance);

        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 1 );
    }

    SECTION ( "merge v1 into v0" ) {
        // testgraph with v0 merged into v2
        vector<int> v0_control = {0,1,3};
        vector<int> v1_control = {0,0,2};
        vector<int> v2_control = {3};
        vector<int> v3_control = {2};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,v3_control};

        vector<int> map_control = {0,2,3,4};

        merge_vertices(1,0,instance);

        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 1 );
    }

    SECTION ( "merge (v3 into v4) into v1" ) {
        // testgraph with v0 merged into v2
        vector<int> v0_control = {2};
        vector<int> v1_control = {0,1,1,1};
        vector<int> v2_control = {0,1,1};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control};

        vector<int> map_control = {0,1,2};

        merge_vertices(3,4,instance);
        merge_vertices(3,1,instance);

        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 2 );
    }

    SECTION ( "merge v1 into v3" ) {
        // testgraph with v0 merged into v2
        vector<int> v0_control = {1};
        vector<int> v1_control = {0,2,2};
        vector<int> v2_control = {0,3,3};
        vector<int> v3_control = {2};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,v3_control};

        vector<int> map_control = {0,2,3,4};

        merge_vertices(1,3,instance);

        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 1 );
    }
}
*/

TEST_CASE ( "merge_many_vertices_test", "[Vertex_op]" ) {
    const int N = 100;
    const int avg_deg = 10;

    mt19937 gen(378);
    uniform_int_distribution node_dist(0,N-1);
    uniform_int_distribution map_dist(0,N*N-1);

    // generate graph
    Graph_Instance inst(N);
    for(int i=0;i<avg_deg*N; ++i) {
        auto a = node_dist(gen);
        inst.graph[a].push_back(node_dist(gen));
    }
    set<int> reps;
    while(size(reps)<N)
        reps.insert(map_dist(gen));
    for(auto rep : reps)
        inst.map.push_back(rep);

    /*
    vector<int> merges((N-20)/3);
    for (int i = 0; i < merges.size(); ++i) merges[i] = i;

    merges[3] = 2;  // path
    merges[4] = 3;
    merges[0] = 4;
    merges[1] = 0;

    merges[7] = 6;  // cycle
    merges[5] = 7;
    merges[6] = 5;

    merges[3] = 10;    // some leaves
    merges[0] = 11;
    merges[11] = 12;

    merges[7] = 15;    // more leaves
    merges[7] = 16;
    merges[16] = 17;

    uniform_int_distribution node_dist2(20,N-1);
    while(size(merges)<(N-20)/3)
        merges[node_dist2(gen)] = node_dist2(gen);
    */

    vector<pair<int,int>> merges{
            {2,3}, {3,4}, {4,0}, {0,1}, // path
            {6,7}, {7,5}, {5,6}, // cycle
            {10,3}, {11,0}, {12,11}, // some leaves
            {15,7}, {16,7}, {17,16}, // more leaves
    };
    uniform_int_distribution node_dist2(20,N-1);
    while(size(merges)<(N-20)/3)
        merges.emplace_back(node_dist2(gen), node_dist2(gen));

    SECTION ( "idk" ) {
        auto old_inst = inst;
        Vertex_op::merge_vertices(merges,inst);

        // check DS
        DSF co(N);
        for(auto [u,v] : merges)
            co.join(u,v);
        int nodes_after_merge = 0;
        for(int v =0; v<N; ++v)
            nodes_after_merge += co.find(v) == v;
        REQUIRE(nodes_after_merge == inst.N);

        vector<pair<int,int>> old_edges, new_edges;
        for(int v =0; v<old_inst.N; ++v)
            for(int u : old_inst.graph[v])
                old_edges.emplace_back(old_inst.map[co.find(v)], old_inst.map[co.find(u)]);
        for(int v =0; v<inst.N; ++v)
            for(int u : inst.graph[v])
                new_edges.emplace_back(inst.map[v], inst.map[u]);
        sort(begin(old_edges), end(old_edges));
        old_edges.erase(unique(begin(old_edges),end(old_edges)), end(old_edges));
        sort(begin(new_edges), end(new_edges));

        REQUIRE(old_edges == new_edges);
        for(auto& row : inst.graph)
            for(auto v : row)
                REQUIRE((0<=v && v<inst.N));
    }
}

TEST_CASE ( "replace_vertex_test", "[Vertex_op]" ) {
    SECTION ( "simple case" ) {
        vector<int> v0 = {6};
        vector<int> v1 = {6};
        vector<int> v2 = {6};
        vector<int> v3 = {};
        vector<int> v4 = {};
        vector<int> v5 = {};
        vector<int> v6 = {3,4,5};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5,v6};

        vector<int> map = {0,1,2,3,4,5,6};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {4,5,3};
        vector<int> v1_control = {4,5,3};
        vector<int> v2_control = {4,5,3};
        vector<int> v3_control = {};
        vector<int> v4_control = {};
        vector<int> v5_control = {};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,v3_control,v4_control,v5_control};

        vector<int> map_control = {0,1,2,3,4,5};

        replace_vertex(6,instance);

        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 1 );
    }

    SECTION ( "more complex case" ) {
        vector<int> v0 = {1,3};
        vector<int> v1 = {2};
        vector<int> v2 = {0};
        vector<int> v3 = {1,4};
        vector<int> v4 = {3,5};
        vector<int> v5 = {4,3};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5};

        vector<int> map = {0,1,2,3,4,5};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {1,3};
        vector<int> v1_control = {2};
        vector<int> v2_control = {0};
        vector<int> v3_control = {3,4,1};
        vector<int> v4_control = {1,3};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,v3_control,v4_control};

        vector<int> map_control = {0,1,2,4,5};

        replace_vertex(3,instance);

        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 1 );
    }
}

TEST_CASE ( "merge_test", "[Vertex_op]" ) {
    SECTION ( "empty vectors" ) {
        vector<int> empty(0);
        vector<int> empty1(0);
        vector<int> empty2(0);
        vector<int> non_empty = {2,4,6};
        vector<int> non_empty_control = {2,4,6};

        merge(empty1,empty2);
        REQUIRE ( empty1 == empty );

        merge(empty1,non_empty);
        REQUIRE ( empty1 == non_empty );

        merge(non_empty,empty2);
        REQUIRE ( non_empty == non_empty_control );
    }

    SECTION ( "double elements" ) {
        vector<int> v1 = {1,3,4,5,7,9};
        vector<int> v2 = {2,4,6,8};
        vector<int> v3 = {14};
        vector<int> control1 = {1,2,3,4,5,6,7,8,9};
        vector<int> control2= {1,2,3,4,5,6,7,8,9,14};

        merge(v1,v2);
        REQUIRE ( v1 == control1 );

        merge(v1,v3);
        REQUIRE ( v1 == control2 );
    }

    SECTION ( "normal use-case" ) {
        vector<int> v1 = {1,3,5,7,9};
        vector<int> v2 = {2,4,6,8};
        vector<int> control1 = {1,2,3,4,5,6,7,8,9};

        merge(v1,v2);
        REQUIRE ( v1 == control1 );
    }
}

TEST_CASE ( "get_pos_test", "[Vertex_op]" ) {
    vector<int> v = {0,1,2,3,4,5,6,7,8,9};
    vector<int> u = {0,1,2,5,6,7};
    vector<int> w = {4};
    auto fst = begin(v);
    auto fin = end(v);
    auto it_u = begin(u) + 3;

    REQUIRE ( get_pos(0,begin(v),end(v)) == fst );
    REQUIRE ( get_pos(9,begin(v),end(v)) == fin - 1 );
    REQUIRE ( get_pos(4,begin(v),end(v)) == fst + 4 );
    REQUIRE ( get_pos(10,begin(v),end(v)) == fin );
    REQUIRE ( get_pos(3, begin(u),end(u)) == it_u );
    REQUIRE ( get_pos(4, begin(u),end(u)) == it_u );
    REQUIRE ( get_pos(2, begin(w),end(w)) == begin(w) );
}