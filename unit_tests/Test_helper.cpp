//
// Created by Ruben Götz on 06.01.2022.
//

#include "Test_helper.h"
#include "../src/SCC_compute.h"
#include "../src/Parser.h"
#include "../src/Vertex_op.h"
#include <vector>
#include <algorithm>
#include <map>

using namespace std;
using namespace Parser;

int Test_helper::graph_equal(vector<vector<int>> &g1, vector<vector<int>> &g2) {
    if ( g1.size() != g2.size() ) return 0;                // REQUIRE ( g == g_control )
    for (int i = 0; i < g1.size(); ++i) {
        if ( g1[i].size() != g2[i].size() ) return 0;
        sort(g1[i].begin(), g1[i].end());
        sort(g2[i].begin(), g2[i].end());
        for (int j = 0; j < g1[i].size(); ++j) {
            if ( g1[i][j] != g2[i][j] ) return 0;
        }
    }
    return 1;
}

int Test_helper::instance_same(Graph_Instance i1, Graph_Instance i2) {
    // check N
    if ( i1.N != i2.N ) return 0;

    // check map
    for ( int i = 0; i < i1.map.size(); ++i ) {
        if ( i1.map[i] != i2.map[i] ) return 0;
    }

    // check graph
    for ( int i = 0; i < i1.N; ++i ) {
        if ( i1.graph[i].size() != i2.graph[i].size() ) return 0;
        for ( int j = 0; j < i1.graph[i].size(); ++j ) {
            if ( i1.graph[i][j] != i2.graph[i][j] ) return 0;
        }
    }
    return 1;
}

int Test_helper::coloration_equal(vector<int> &c1, vector<int> &c2) {
    if ( c1.size() != c2.size() ) return 0;

    map<int,int> c1_map;
    map<int,int> c2_map;
    int color_count1 = 0;
    int color_count2 = 0;

    for (int i = 0; i < c1.size(); ++i) {
        // adds color to map if necessary
        if ( c1_map.count(c1[i]) == 0 ) c1_map.insert({c1[i],color_count1++});
        if ( c2_map.count(c2[i]) == 0 ) c2_map.insert({c2[i],color_count2++});

        // checks if cleaned colors are the same
        if ( c1_map.at(c1[i]) != c2_map.at(c2[i]) ) return 0;
    }
    return 1;
}

int Test_helper::coloration_is_normed(vector<int> &coloration) {
    int highest_color(0);
    for (int i = 0; i < coloration.size(); ++i) {
        highest_color = max(coloration[i], highest_color);
    }
    vector<int> exists(highest_color + 1,0);
    for (int i = 0; i < coloration.size(); ++i) {
        exists[coloration[i]] = 1;
    }
    for (int i = 0; i < exists.size(); ++i) {
        if ( exists[i] == 0 ) return 0;
    }
    return 1;
}

int Test_helper::vector_equal(vector<int> &v1, vector<int> &v2) {
    sort(v1.begin(),v1.end());
    sort(v2.begin(),v2.end());
    if ( v1.size() != v2.size() ) return 0;
    for (int i = 0; i < v1.size(); ++i) {
        if ( v1[i] != v2[i] ) return 0;
    }
    return 1;
}


int Test_helper::vector_same(vector<int> &v1, vector<int> &v2) {
    if ( v1.size() != v2.size() ) return 0;
    for (int i = 0; i < v1.size(); ++i) {
        if ( v1[i] != v2[i] ) return 0;
    }
    return 1;
}

int Test_helper::is_acyclic(Graph_Instance &instance) {
    SCC_compute *sc = new SCC_compute(instance);
    vector<int> coloration(instance.graph.size(),0);
    sc->getSCCcoloration(coloration);
    if ( sc->clean_coloration(coloration) == coloration.size() ) return 1;
    return 0;
}

int Test_helper::is_solution(string solution_file, string graph_file) {
    Graph_Instance instance = Graph_Instance(0);
    vector<int> dfvs(0);

    extract_graph(graph_file,instance);
    get_solution_file(solution_file,dfvs);

    sort(dfvs.begin(),dfvs.end());
    for (int i = dfvs.size() - 1; i >= 0; --i) {
        Vertex_op::delete_vertex(dfvs[i], instance.graph);
    }

    return is_acyclic(instance);
}

int Test_helper::is_solution(vector<int> &solution, string graph_file) {
    Graph_Instance instance(0);

    extract_graph(graph_file,instance);

    sort(solution.begin(),solution.end());
    for (int i = solution.size() - 1; i >= 0; --i) {
        Vertex_op::delete_vertex(solution[i], instance.graph);
    }

    return is_acyclic(instance);
}