#include "catch.hpp"
#include "../src/Graph_reduce.h"
#include "Test_helper.h"
#include <vector>

using namespace std;
using namespace Test_helper;
using namespace Graph_reduce;

TEST_CASE ( "isclique_test", "[Graph_reduce]" ) {
    SECTION ( "all vertices alive" ) {
        // test graph 1
        vector<int> v0 = {1};
        vector<int> v1 = {0};
        vector<vector<int>> g1 = {v0, v1};
        Graph_Instance inst1(2);
        inst1.graph = g1;
        vector<int> alive1 = {1, 1};

        // test graph 2
        vector<int> v2 = {1, 2, 3, 4};
        vector<int> v3 = {0, 2, 3, 4};
        vector<int> v4 = {0, 1, 3, 4};
        vector<int> v5 = {0, 1, 2, 4};
        vector<int> v6 = {0, 1, 2, 3};
        vector<vector<int>> g2 = {v2, v3, v4, v5, v6};
        Graph_Instance inst2(5);
        inst2.graph = g2;
        vector<int> alive2 = {1, 1, 1, 1, 1};

        // test graph 3 (g1 with added vertex)
        vector<int> v7 = {1};
        vector<int> v8 = {0, 2};
        vector<int> v9 = {};
        vector<vector<int>> g3 = {v7, v8, v9};
        Graph_Instance inst3(3);
        inst3.graph = g3;
        vector<int> alive3 = {1, 1, 1};

        // test graph 4 (g3 with added edge)
        vector<int> v10 = {1};
        vector<int> v11 = {0, 2};
        vector<int> v12 = {1};
        vector<vector<int>> g4 = {v10, v11, v12};
        Graph_Instance inst4(3);
        inst4.graph = g4;
        vector<int> alive4 = {1, 1, 1};

        REQUIRE(is_clique(0, inst1));
        REQUIRE(is_clique(1, inst1));

        REQUIRE(is_clique(0, inst2));
        REQUIRE(is_clique(1, inst2));
        REQUIRE(is_clique(2, inst2));
        REQUIRE(is_clique(3, inst2));
        REQUIRE(is_clique(4, inst2));

        REQUIRE(is_clique(0, inst3));
        REQUIRE(!is_clique(1, inst3));
        REQUIRE(is_clique(2, inst3));

        REQUIRE(is_clique(0, inst4));
        REQUIRE(!is_clique(1, inst4));
        REQUIRE(is_clique(2, inst4));
    }

    /*
    SECTION ( "alive vector" ) {
        Graph_Instance instance(4);
        instance.graph[0] = {1,3};
        instance.graph[1] = {0,2,3};
        instance.graph[2] = {1,3};
        instance.graph[3] = {0,1,2};

        vector<int> alive1 = {1,1,0,1};
        vector<int> alive2 = {1,1,1,1};

        REQUIRE ( is_clique(0,instance,alive1) );
        REQUIRE ( is_clique(1,instance,alive1) );
        REQUIRE ( is_clique(3,instance,alive1) );

        REQUIRE ( is_clique(0,instance,alive2) );
        REQUIRE ( !is_clique(1,instance,alive2) );
        REQUIRE ( !is_clique(3,instance,alive2) );
    }
     */
}

TEST_CASE ( "isin_test", "[Graph_reduce]" ) {
    vector<int> v = {0,3,4,42,87,1337};
    int pos;

    REQUIRE ( isin(v,0) );
    REQUIRE ( isin(v,0,pos) );
    REQUIRE ( pos == 0);

    REQUIRE ( isin(v,3) );
    REQUIRE ( isin(v,3,pos) );
    REQUIRE ( pos == 1);

    REQUIRE ( isin(v,1337) );
    REQUIRE ( isin(v,1337,pos) );
    REQUIRE ( pos == 5);

    REQUIRE ( !isin(v,420) );
    REQUIRE ( !isin(v,420,pos) );
}

TEST_CASE ( "is_subset_test", "[Graph_reduce]" ) {
    vector<int> set = {0,1,2,3,4,5,6,7,8,9};
    vector<int> empty_set = {};
    vector<int> subset = {1,3,4,7,8};
    vector<int> no_subset = {0,1,5,8,10};

    REQUIRE ( is_subset(set,empty_set) );
    REQUIRE ( is_subset(set,subset) );
    REQUIRE ( !is_subset(set,no_subset) );
}

TEST_CASE ( "remove_duplicates_test", "[Graph_reduce]" ) {
    vector<int> v1 = {4,5,8,7,6,2,1,30};
    vector<int> v2 = {1,15,1,2,4,9,1,15,15};

    vector<int> v1_control = v1;
    vector<int> v2_control = {1,2,4,9,15};

    remove_duplicates(v1);
    remove_duplicates(v2);

    REQUIRE( vector_equal(v1,v1_control) );
    REQUIRE( vector_equal(v2,v2_control) );
}

TEST_CASE ( "lower_bound_matching_test", "[Graph_reduce]" ) {
    SECTION ( "empty graph" ) {
        Graph_Instance instance(0);

        REQUIRE( lower_bound_matching(instance) == 0 );
    }

    SECTION ( "simple chain" ) {
        Graph_Instance instance(5);
        instance.graph[0] = {2};
        instance.graph[1] = {2,3};
        instance.graph[2] = {0,1};
        instance.graph[3] = {1,4};
        instance.graph[4] = {3};

        int lb = lower_bound_matching(instance);

        REQUIRE ( lb >= 1 );
        REQUIRE ( lb <= 2 );
    }

    SECTION ( "simple circle" ) {
        Graph_Instance instance(5);
        instance.graph[0] = {2,4};
        instance.graph[1] = {2,3};
        instance.graph[2] = {0,1};
        instance.graph[3] = {1,4};
        instance.graph[4] = {0,3};

        int lb = lower_bound_matching(instance);

        REQUIRE ( lb >= 1 );
        REQUIRE ( lb <= 2 );
    }

    SECTION ( "circle with leaf" ) {
        Graph_Instance instance(6);
        instance.graph[0] = {2,4};
        instance.graph[1] = {2,3};
        instance.graph[2] = {0,1,5};
        instance.graph[3] = {1,4};
        instance.graph[4] = {0,3};
        instance.graph[5] = {2};

        int lb = lower_bound_matching(instance);

        REQUIRE ( lb >= 2 );
        REQUIRE ( lb <= 3 );
    }

    SECTION ( "complex case" ) {
        Graph_Instance instance(6);
        instance.graph[0] = {1,2};
        instance.graph[1] = {0,2,3};
        instance.graph[2] = {0,1,4};
        instance.graph[3] = {1,4,5};
        instance.graph[4] = {2,3,5};
        instance.graph[5] = {3,4};

        int lb = lower_bound_matching(instance);

        REQUIRE ( lb >= 2 );
        REQUIRE ( lb <= 3 );
    }
}

TEST_CASE ( "in0_test", "[Graph_reduce]" ) {
    // test graph g1
    vector<int> v0 = {3};
    vector<int> v1 = {3};
    vector<int> v2 = {3};
    vector<int> v3 = {5};
    vector<int> v4 = {5,6};
    vector<int> v5 = {6};
    vector<int> v6 = {4};
    vector<int> v7 = {};
    vector<int> v8 = {8};
    vector<vector<int>> g1 = {v0,v1,v2,v3,v4,v5,v6,v7,v8};

    vector<int> map1 = {0,1,2,3,4,5,6,7,8};

    Graph_Instance instance1 = Graph_Instance(0);
    instance1.graph = g1;
    instance1.map = map1;
    instance1.N = g1.size();

    // test graph g2
    vector<int> v9  ={1};
    vector<int> v10 ={2};
    vector<int> v11 ={3,0};
    vector<int> v12 ={};
    vector<int> v13 ={4};
    vector<vector<int>> g2 = {v9,v10,v11,v12,v13};

    vector<int> map2 = {0,1,2,3,4};

    Graph_Instance instance2 = Graph_Instance(0);
    instance2.graph = g2;
    instance2.map = map2;
    instance2.N = g2.size();

    vector<int> v0_control = {2};
    vector<int> v1_control = {2,3};
    vector<int> v2_control = {3};
    vector<int> v3_control = {1};
    vector<int> v4_control = {4};
    vector<vector<int>> g1_control = {v0_control,v1_control,v2_control,v3_control,v4_control};

    vector<int> map1_control = {3,4,5,6,8};

    vector<vector<int>> g2_control = {v9,v10,v11,v12,v13};

    vector<int> map2_control = {0,1,2,3,4};

    REQUIRE ( in0(instance1) );
    REQUIRE ( graph_equal(instance1.graph,g1_control) );
    REQUIRE ( vector_equal(instance1.map,map1_control) );
    REQUIRE ( instance1.N == g1.size() - 4 );

    REQUIRE ( !in0(instance2) );
    REQUIRE ( graph_equal(instance2.graph,g2_control) );
    REQUIRE ( vector_equal(instance2.map,map2_control) );
    REQUIRE ( instance2.N == g2.size() );
}

TEST_CASE ( "out0_test", "[Graph_reduce]" ) {
    // test graph g1
    vector<int> v0 = {3};
    vector<int> v1 = {3};
    vector<int> v2 = {3};
    vector<int> v3 = {5};
    vector<int> v4 = {5,6};
    vector<int> v5 = {6};
    vector<int> v6 = {4};
    vector<int> v7 = {};
    vector<int> v8 = {7,8};
    vector<int> v9 = {};
    vector<vector<int>> g1 = {v0,v1,v2,v3,v4,v5,v6,v7,v8,v9};

    vector<int> map1 = {0,1,2,3,4,5,6,7,8,9};

    Graph_Instance instance1 = Graph_Instance(0);
    instance1.graph = g1;
    instance1.map = map1;
    instance1.N = g1.size();

    // test graph g2
    vector<int> v10 = {0};
    vector<int> v11 = {2};
    vector<int> v12 = {3};
    vector<int> v13 = {2,1};
    vector<vector<int>> g2 = {v10,v11,v12,v13};

    vector<int> map2 = {0,1,2,3};

    Graph_Instance instance2 = Graph_Instance(0);
    instance2.graph = g2;
    instance2.map = map2;
    instance2.N = g2.size();

    vector<int> v0_control = {3};
    vector<int> v1_control = {3};
    vector<int> v2_control = {3};
    vector<int> v3_control = {5};
    vector<int> v4_control = {5,6};
    vector<int> v5_control = {6};
    vector<int> v6_control = {4};
    vector<int> v7_control = {7};
    vector<vector<int>> g1_control = {v0_control,v1_control,v2_control,v3_control,
                                     v4_control,v5_control,v6_control,v7_control};

    vector<int> map1_control = {0,1,2,3,4,5,6,8};

    vector<vector<int>> g2_control = {v10,v11,v12,v13};

    vector<int> map2_control = {0,1,2,3};

    REQUIRE ( out0(instance1) );
    REQUIRE ( graph_equal(instance1.graph,g1_control) );
    REQUIRE ( vector_equal(instance1.map,map1_control) );
    REQUIRE ( instance1.N == g1.size() - 2 );

    REQUIRE ( !out0(instance2) );
    REQUIRE ( graph_equal(instance2.graph,g2_control) );
    REQUIRE ( vector_equal(instance2.map,map2_control) );
    REQUIRE ( instance2.N == g2.size() );
}

TEST_CASE ( "loop_test", "[Graph_reduce]" ) {
    // test graph
    vector<int> v0 = {};
    vector<int> v1 = {2};
    vector<int> v2 = {1,9};
    vector<int> v3 = {3};
    vector<int> v4 = {5};
    vector<int> v5 = {5};
    vector<int> v6 = {};
    vector<int> v7 = {6,7};
    vector<int> v8 = {1,8,9};
    vector<int> v9 = {8};
    vector<vector<int>> g = {v0,v1,v2,v3,v4,v5,v6,v7,v8,v9};

    vector<int> map = {0,1,2,3,4,5,6,7,8,9};

    Graph_Instance instance = Graph_Instance(0);
    instance.graph = g;
    instance.map = map;
    instance.N = g.size();

    vector<int> v0_control = {};
    vector<int> v1_control = {2};
    vector<int> v2_control = {1,5};
    vector<int> v3_control = {};
    vector<int> v4_control = {};
    vector<int> v5_control = {};
    vector<vector<int>> g_control = {v0_control,v1_control,v2_control,
                                     v3_control,v4_control,v5_control};

    vector<int> map_control = {0,1,2,4,6,9};

    vector<int> pos_control = {3,5,7,8};

    vector<int> pos;

    REQUIRE ( loop(instance, pos) );
    REQUIRE ( graph_equal(instance.graph,g_control) );
    REQUIRE ( vector_equal(pos,pos_control) );
    REQUIRE ( vector_equal(instance.map,map_control) );
    REQUIRE ( instance.N == g.size() - 4 );
    REQUIRE ( !loop(instance,pos) );
}

TEST_CASE ( "in1_test", "[Graph_reduce]" ) {
    SECTION ( "trivial case" ) {
        vector<int> v0 = {1};
        vector<int> v1 = {};
        vector<vector<int>> g = {v0,v1};

        vector<int> map = {0,1};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {};
        vector<vector<int>> g_control = {v0_control};

        vector<int> map_control = {0};

        REQUIRE ( in1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 1 );
        REQUIRE ( !in1(instance) );
    }

    SECTION ( "self-loop" ) {
        Graph_Instance instance = Graph_Instance(2);
        instance.graph[0] = {0};
        instance.graph[1] = {};

        instance.map = {0,1};

        instance.N = 2;

        vector<vector<int>> graph_control = {{0}, {}};

        vector<int> map_control = {0,1};

        REQUIRE ( !in1(instance) );
        REQUIRE ( graph_equal(instance.graph,graph_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == 2 );
    }

    SECTION ( "simple chain case" ) {
        vector<int> v0 = {1};
        vector<int> v1 = {2};
        vector<int> v2 = {};
        vector<vector<int>> g = {v0,v1,v2};

        vector<int> map = {0,1,2};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {};
        vector<vector<int>> g_control = {v0_control};

        vector<int> map_control = {0};

        REQUIRE ( in1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 2 );
        REQUIRE ( !in1(instance) );
    }

    SECTION ( "simple circle case" ) {
        vector<int> v0 = {1};
        vector<int> v1 = {2};
        vector<int> v2 = {0};
        vector<vector<int>> g = {v0,v1,v2};

        vector<int> map = {0,1,2};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {0};
        vector<vector<int>> g_control = {v0_control};

        REQUIRE ( in1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( instance.map.size() == 1 );
        REQUIRE ( instance.N == g.size() - 2 );
    }

    SECTION ( "simple circle case 2" ) {
        vector<int> v0 = {3};
        vector<int> v1 = {2};
        vector<int> v2 = {0};
        vector<int> v3 = {1};
        vector<vector<int>> g = {v0,v1,v2,v3};

        vector<int> map = {0,1,2,3};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {0};
        vector<vector<int>> g_control = {v0_control};

        REQUIRE ( in1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( instance.map.size() == 1 );
        REQUIRE ( instance.N == 1 );
    }

    SECTION ( "circle with leaf" ) {
        vector<int> v0 = {1};
        vector<int> v1 = {2,3};
        vector<int> v2 = {0};
        vector<int> v3 = {};
        vector<vector<int>> g = {v0,v1,v2,v3};

        vector<int> map = {0,1,2,3};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {0};
        vector<vector<int>> g_control = {v0_control};

        REQUIRE ( in1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( instance.map.size() == 1 );
        REQUIRE ( instance.map[0] != 3 );
        REQUIRE ( instance.N == g.size() - 3 );
    }

    SECTION ( "complex case" ) {
        vector<int> v0 = {4};
        vector<int> v1 = {0};
        vector<int> v2 = {3,5};
        vector<int> v3 = {};
        vector<int> v4 = {0,2,1};
        vector<int> v5 = {3};
        vector<int> v6 = {1,4,5};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5,v6};

        vector<int> map = {0,1,2,3,4,5,6};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {3};
        vector<int> v1_control = {0};
        vector<int> v2_control = {};
        vector<int> v3_control = {0,2,4,1};
        vector<int> v4_control = {2};
        vector<int> v5_control = {3,1,4};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,
                                         v3_control,v4_control,v5_control};

        vector<int> map_control = {0,1,3,4,5,6};

        REQUIRE ( in1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 1 );
        REQUIRE ( !in1(instance) );
    }

    SECTION ( "even more complex case" ) {
        vector<int> v0 = {4};
        vector<int> v1 = {0};
        vector<int> v2 = {3,5};
        vector<int> v3 = {};
        vector<int> v4 = {0,7,1};
        vector<int> v5 = {3,10};
        vector<int> v6 = {1,4,5,8};
        vector<int> v7 = {2};
        vector<int> v8 = {};
        vector<int> v9 = {5};
        vector<int> v10 = {9};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10};

        vector<int> map = {0,1,2,3,4,5,6,7,8,9,10};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {3};
        vector<int> v1_control = {0};
        vector<int> v2_control = {};
        vector<int> v3_control = {0,2,4,1};
        vector<int> v4_control = {2,4};
        vector<int> v5_control = {1,3,4};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,
                                         v3_control,v4_control,v5_control};

        vector<int> map_control = {0,1,3,4,5,6};

        REQUIRE ( in1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 5 );
        REQUIRE ( !in1(instance) );
    }
}

TEST_CASE ( "out1_test", "[Graph_reduce]" ) {
    SECTION ( "trivial case" ) {
        vector<int> v0 = {1};
        vector<int> v1 = {};
        vector<vector<int>> g = {v0,v1};

        vector<int> map = {0,1};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {};
        vector<vector<int>> g_control = {v0_control};

        vector<int> map_control = {1};

        REQUIRE ( out1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 1 );
        REQUIRE ( !out1(instance) );
    }

    SECTION ( "simple chain case" ) {
        vector<int> v0 = {1};
        vector<int> v1 = {2};
        vector<int> v2 = {};
        vector<vector<int>> g = {v0,v1,v2};

        vector<int> map = {0,1,2};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {};
        vector<vector<int>> g_control = {v0_control};

        vector<int> map_control = {2};

        REQUIRE ( out1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 2 );
        REQUIRE ( !out1(instance) );
    }

    SECTION ( "simple circle case" ) {
        vector<int> v0 = {1};
        vector<int> v1 = {2};
        vector<int> v2 = {0};
        vector<vector<int>> g = {v0,v1,v2};

        vector<int> map = {0,1,2};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {0};
        vector<vector<int>> g_control = {v0_control};

        vector<int> map_control = {0};

        REQUIRE ( out1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( instance.map.size() == 1 );
        REQUIRE ( instance.N == g.size() - 2 );
    }

    SECTION ( "circle with leaf" ) {
        vector<int> v0 = {1};
        vector<int> v1 = {2};
        vector<int> v2 = {0};
        vector<int> v3 = {1};
        vector<vector<int>> g = {v0,v1,v2,v3};

        vector<int> map = {0,1,2,3};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {0};
        vector<vector<int>> g_control = {v0_control};

        REQUIRE ( out1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( instance.map.size() == 1 );
        REQUIRE ( instance.map[0] != 3 );
        REQUIRE ( instance.N == g.size() - 3 );
    }

    SECTION ( "complex case" ) {
        vector<int> v0  = {2};
        vector<int> v1  = {4,0};
        vector<int> v2  = {5,7};
        vector<int> v3  = {0,1};
        vector<int> v4  = {6};
        vector<int> v5  = {2,4,1};
        vector<int> v6  = {};
        vector<int> v7  = {5,2};
        vector<int> v8  = {};
        vector<int> v9  = {8};
        vector<int> v10 = {};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10};

        vector<int> map = {0,1,2,3,4,5,6,7,8,9,10};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {1,4};
        vector<int> v1_control = {3,5};
        vector<int> v2_control = {0,1};
        vector<int> v3_control = {0,1,4};
        vector<int> v4_control = {};
        vector<int> v5_control = {1,3};
        vector<int> v6_control = {};
        vector<int> v7_control = {};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,v3_control,
                                         v4_control,v5_control,v6_control,v7_control};

        vector<int> map_control = {1,2,3,5,6,7,8,10};

        REQUIRE ( out1(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 3 );
        REQUIRE ( !out1(instance) );
    }
}

TEST_CASE ( "calculate_pie_test", "[Graph_reduce]" ) {
    SECTION ( "empty graph" ) {
        Graph_Instance instance = Graph_Instance(0);
        Graph_Instance g_PIE = Graph_Instance(0);

        calculate_pie(instance, g_PIE);

        REQUIRE ( g_PIE.N == 0 );
        REQUIRE ( g_PIE.graph.size() == 0 );
    }

    SECTION ( "selfloop" ) {
        Graph_Instance instance = Graph_Instance(2);
        instance.graph[0] = {0,1};
        instance.graph[1] = {};

        Graph_Instance g_PIE_control = Graph_Instance(2);

        Graph_Instance g_PIE = Graph_Instance(2);

        calculate_pie(instance,g_PIE);

        REQUIRE ( g_PIE.N == g_PIE_control.N );
        REQUIRE ( graph_equal(g_PIE.graph,g_PIE_control.graph) );
    }

    SECTION ( "graph without PIE" ) {
        Graph_Instance instance = Graph_Instance(5);
        instance.graph[0] = {3,4};
        instance.graph[1] = {0};
        instance.graph[2] = {4,0};
        instance.graph[3] = {2,1};
        instance.graph[4] = {3};

        Graph_Instance g_PIE = Graph_Instance(5);

        Graph_Instance g_PIE_control = Graph_Instance(5);

        calculate_pie(instance,g_PIE);

        REQUIRE ( g_PIE.N == g_PIE_control.N );
        REQUIRE ( graph_equal(g_PIE.graph,g_PIE_control.graph) );
    }

    SECTION ( "simple case" ) {
        Graph_Instance instance = Graph_Instance(3);
        instance.graph[0] = {1,2};
        instance.graph[1] = {0,2};
        instance.graph[2] = {};

        Graph_Instance g_PIE_control = Graph_Instance(3);
        g_PIE_control.graph[0] = {1};
        g_PIE_control.graph[1] = {0};
        g_PIE_control.graph[2] = {};

        Graph_Instance g_PIE = Graph_Instance(3);

        calculate_pie(instance,g_PIE);

        REQUIRE ( g_PIE.N == g_PIE_control.N );
        REQUIRE ( graph_equal(g_PIE.graph,g_PIE_control.graph) );
    }

    SECTION ( "chain" ) {
        Graph_Instance instance = Graph_Instance(3);
        instance.graph[0] = {2};
        instance.graph[1] = {2};
        instance.graph[2] = {0,1};

        Graph_Instance g_PIE_control = instance;

        Graph_Instance g_PIE = Graph_Instance(3);

        calculate_pie(instance,g_PIE);

        REQUIRE ( g_PIE.N == g_PIE_control.N );
        REQUIRE ( graph_equal(g_PIE.graph,g_PIE_control.graph) );
    }

    SECTION ( "complex case" ) {
        Graph_Instance instance = Graph_Instance(8);
        instance.graph[0] = {1,2,3};
        instance.graph[1] = {0,3};
        instance.graph[2] = {0,6};
        instance.graph[3] = {1,2,4};
        instance.graph[4] = {5,7};
        instance.graph[5] = {4,7};
        instance.graph[6] = {4,7};
        instance.graph[7] = {5,6};

        Graph_Instance g_PIE_control = Graph_Instance(8);
        g_PIE_control.graph[0] = {1,2};
        g_PIE_control.graph[1] = {0,3};
        g_PIE_control.graph[2] = {0};
        g_PIE_control.graph[3] = {1};
        g_PIE_control.graph[4] = {5};
        g_PIE_control.graph[5] = {4,7};
        g_PIE_control.graph[6] = {7};
        g_PIE_control.graph[7] = {5,6};

        Graph_Instance g_PIE = Graph_Instance(8);

        calculate_pie(instance,g_PIE);

        REQUIRE ( g_PIE.N == g_PIE_control.N );
        REQUIRE ( graph_equal(g_PIE.graph,g_PIE_control.graph) );
    }
}

TEST_CASE ( "calculate_notpie_test", "[Graph_reduce]" ) {
    SECTION ( "empty graph" ) {
        Graph_Instance instance = Graph_Instance(0);
        Graph_Instance g_not_PIE = Graph_Instance(0);

        calculate_notpie(instance, g_not_PIE);

        REQUIRE ( g_not_PIE.N == 0 );
        REQUIRE ( g_not_PIE.graph.size() == 0 );
    }

    SECTION ( "selfloop" ) {
        Graph_Instance instance = Graph_Instance(2);
        instance.graph[0] = {0,1};
        instance.graph[1] = {};

        Graph_Instance g_not_PIE_control = instance;

        Graph_Instance g_not_PIE = Graph_Instance(2);

        calculate_notpie(instance,g_not_PIE);

        REQUIRE ( g_not_PIE.N == g_not_PIE_control.N );
        REQUIRE ( graph_equal(g_not_PIE.graph,g_not_PIE_control.graph) );
    }

    SECTION ( "graph without PIE" ) {
        Graph_Instance instance = Graph_Instance(5);
        instance.graph[0] = {3,4};
        instance.graph[1] = {0};
        instance.graph[2] = {4,0};
        instance.graph[3] = {2,1};
        instance.graph[4] = {3};

        Graph_Instance g_not_PIE = Graph_Instance(5);

        Graph_Instance g_not_PIE_control = instance;

        calculate_notpie(instance,g_not_PIE);

        REQUIRE ( g_not_PIE.N == g_not_PIE_control.N );
        REQUIRE ( graph_equal(g_not_PIE.graph,g_not_PIE_control.graph) );
    }

    SECTION ( "simple case" ) {
        Graph_Instance instance = Graph_Instance(3);
        instance.graph[0] = {1,2};
        instance.graph[1] = {0,2};
        instance.graph[2] = {};

        Graph_Instance g_not_PIE_control = Graph_Instance(3);
        g_not_PIE_control.graph[0] = {2};
        g_not_PIE_control.graph[1] = {2};
        g_not_PIE_control.graph[2] = {};

        Graph_Instance g_not_PIE = Graph_Instance(3);

        calculate_notpie(instance,g_not_PIE);

        REQUIRE ( g_not_PIE.N == g_not_PIE_control.N );
        REQUIRE ( graph_equal(g_not_PIE.graph,g_not_PIE_control.graph) );
    }

    SECTION ( "chain" ) {
        Graph_Instance instance = Graph_Instance(3);
        instance.graph[0] = {2};
        instance.graph[1] = {2};
        instance.graph[2] = {0,1};

        Graph_Instance g_not_PIE_control = Graph_Instance(3);

        Graph_Instance g_not_PIE = Graph_Instance(3);

        calculate_notpie(instance,g_not_PIE);

        REQUIRE ( g_not_PIE.N == g_not_PIE_control.N );
        REQUIRE ( graph_equal(g_not_PIE.graph,g_not_PIE_control.graph) );
    }

    SECTION ( "complex case" ) {
        Graph_Instance instance = Graph_Instance(8);
        instance.graph[0] = {1,2,3};
        instance.graph[1] = {0,3};
        instance.graph[2] = {0,6};
        instance.graph[3] = {1,2,4};
        instance.graph[4] = {5,7};
        instance.graph[5] = {4,7};
        instance.graph[6] = {4,7};
        instance.graph[7] = {5,6};

        Graph_Instance g_not_PIE_control = Graph_Instance(8);
        g_not_PIE_control.graph[0] = {3};
        g_not_PIE_control.graph[1] = {};
        g_not_PIE_control.graph[2] = {6};
        g_not_PIE_control.graph[3] = {2,4};
        g_not_PIE_control.graph[4] = {7};
        g_not_PIE_control.graph[5] = {};
        g_not_PIE_control.graph[6] = {4};
        g_not_PIE_control.graph[7] = {};

        Graph_Instance g_not_PIE = Graph_Instance(8);

        calculate_notpie(instance,g_not_PIE);

        REQUIRE ( g_not_PIE.N == g_not_PIE_control.N );
        REQUIRE ( graph_equal(g_not_PIE.graph,g_not_PIE_control.graph) );
    }
}

TEST_CASE ( "calculate_pie_notpie_test", "[Graph_reduce]" ) {
    SECTION ("empty graph") {
        Graph_Instance instance = Graph_Instance(0);
        Graph_Instance g_PIE = Graph_Instance(0);
        Graph_Instance g_not_PIE = Graph_Instance(0);

        calculate_pie_notpie(instance, g_PIE, g_not_PIE);

        REQUIRE (g_PIE.N == 0);
        REQUIRE (g_PIE.graph.size() == 0);
        REQUIRE (g_not_PIE.N == 0);
        REQUIRE (g_not_PIE.graph.size() == 0);
    }

    SECTION ("selfloop") {
        Graph_Instance instance = Graph_Instance(2);
        instance.graph[0] = {0, 1};
        instance.graph[1] = {};

        Graph_Instance g_PIE = Graph_Instance(2);
        Graph_Instance g_not_PIE = Graph_Instance(2);

        Graph_Instance g_PIE_control = Graph_Instance(2);
        Graph_Instance g_not_PIE_control = instance;

        calculate_pie_notpie(instance, g_PIE, g_not_PIE);

        REQUIRE (g_PIE.N == g_PIE_control.N);
        REQUIRE (graph_equal(g_PIE.graph, g_PIE_control.graph));
        REQUIRE (g_not_PIE.N == g_not_PIE_control.N);
        REQUIRE (graph_equal(g_not_PIE.graph, g_not_PIE_control.graph));
    }

    SECTION ("graph without PIE") {
        Graph_Instance instance = Graph_Instance(5);
        instance.graph[0] = {3, 4};
        instance.graph[1] = {0};
        instance.graph[2] = {4, 0};
        instance.graph[3] = {2, 1};
        instance.graph[4] = {3};

        Graph_Instance g_PIE = Graph_Instance(5);
        Graph_Instance g_not_PIE = Graph_Instance(5);

        Graph_Instance g_not_PIE_control = instance;
        Graph_Instance g_PIE_control = Graph_Instance(5);

        calculate_pie_notpie(instance, g_PIE, g_not_PIE);

        REQUIRE (g_PIE.N == g_PIE_control.N);
        REQUIRE (graph_equal(g_PIE.graph, g_PIE_control.graph));
        REQUIRE (g_not_PIE.N == g_not_PIE_control.N);
        REQUIRE (graph_equal(g_not_PIE.graph, g_not_PIE_control.graph));
    }

    SECTION ("simple case") {
        Graph_Instance instance = Graph_Instance(3);
        instance.graph[0] = {1, 2};
        instance.graph[1] = {0, 2};
        instance.graph[2] = {};

        Graph_Instance g_PIE = Graph_Instance(3);
        Graph_Instance g_not_PIE = Graph_Instance(3);

        Graph_Instance g_PIE_control = Graph_Instance(3);
        g_PIE_control.graph[0] = {1};
        g_PIE_control.graph[1] = {0};
        g_PIE_control.graph[2] = {};

        Graph_Instance g_not_PIE_control = Graph_Instance(3);
        g_not_PIE_control.graph[0] = {2};
        g_not_PIE_control.graph[1] = {2};
        g_not_PIE_control.graph[2] = {};

        calculate_pie_notpie(instance, g_PIE, g_not_PIE);

        REQUIRE (g_PIE.N == g_PIE_control.N);
        REQUIRE (graph_equal(g_PIE.graph, g_PIE_control.graph));
        REQUIRE (g_not_PIE.N == g_not_PIE_control.N);
        REQUIRE (graph_equal(g_not_PIE.graph, g_not_PIE_control.graph));
    }

    SECTION ("chain") {
        Graph_Instance instance = Graph_Instance(3);
        instance.graph[0] = {2};
        instance.graph[1] = {2};
        instance.graph[2] = {0, 1};

        Graph_Instance g_PIE = Graph_Instance(3);
        Graph_Instance g_not_PIE = Graph_Instance(3);

        Graph_Instance g_PIE_control = instance;
        Graph_Instance g_not_PIE_control = Graph_Instance(3);

        calculate_pie_notpie(instance, g_PIE, g_not_PIE);

        REQUIRE (g_PIE.N == g_PIE_control.N);
        REQUIRE (graph_equal(g_PIE.graph, g_PIE_control.graph));
        REQUIRE (g_not_PIE.N == g_not_PIE_control.N);
        REQUIRE (graph_equal(g_not_PIE.graph, g_not_PIE_control.graph));
    }

    SECTION ("complex case") {
        Graph_Instance instance = Graph_Instance(8);
        instance.graph[0] = {1, 2, 3};
        instance.graph[1] = {0, 3};
        instance.graph[2] = {0, 6};
        instance.graph[3] = {1, 2, 4};
        instance.graph[4] = {5, 7};
        instance.graph[5] = {4, 7};
        instance.graph[6] = {4, 7};
        instance.graph[7] = {5, 6};

        Graph_Instance g_PIE = Graph_Instance(8);
        Graph_Instance g_not_PIE = Graph_Instance(8);

        Graph_Instance g_PIE_control = Graph_Instance(8);
        g_PIE_control.graph[0] = {1, 2};
        g_PIE_control.graph[1] = {0, 3};
        g_PIE_control.graph[2] = {0};
        g_PIE_control.graph[3] = {1};
        g_PIE_control.graph[4] = {5};
        g_PIE_control.graph[5] = {4, 7};
        g_PIE_control.graph[6] = {7};
        g_PIE_control.graph[7] = {5, 6};

        Graph_Instance g_not_PIE_control = Graph_Instance(8);
        g_not_PIE_control.graph[0] = {3};
        g_not_PIE_control.graph[1] = {};
        g_not_PIE_control.graph[2] = {6};
        g_not_PIE_control.graph[3] = {2, 4};
        g_not_PIE_control.graph[4] = {7};
        g_not_PIE_control.graph[5] = {};
        g_not_PIE_control.graph[6] = {4};
        g_not_PIE_control.graph[7] = {};

        calculate_pie_notpie(instance, g_PIE, g_not_PIE);

        REQUIRE (g_PIE.N == g_PIE_control.N);
        REQUIRE (graph_equal(g_PIE.graph, g_PIE_control.graph));
        REQUIRE (g_not_PIE.N == g_not_PIE_control.N);
        REQUIRE (graph_equal(g_not_PIE.graph, g_not_PIE_control.graph));
    }
}

TEST_CASE ( "counting_sort_test", "[Graph_reduce]" ) {
    SECTION ( "empty graph" ) {
        Graph_Instance instance = Graph_Instance(0);

        counting_sort(instance);

        REQUIRE ( instance.N == 0 );
    }

    SECTION ( "already sorted graph" ) {
        Graph_Instance instance = Graph_Instance(5);
        instance.graph[0] = {0};
        instance.graph[1] = {};
        instance.graph[2] = {0,1,2,3,4};
        instance.graph[3] = {3,3,4,4,4};
        instance.graph[4] = {1,2,2,3,3,3,4};

        Graph_Instance instance_control = instance;

        counting_sort(instance);

        REQUIRE ( instance.N == instance_control.N );
        REQUIRE ( instance_same(instance,instance_control) );
    }

    SECTION ( "single value graph" ) {
        Graph_Instance instance = Graph_Instance(5);
        instance.graph[0] = {1,1,1,1};
        instance.graph[1] = {1,1,1,1};
        instance.graph[2] = {1,1,1,1};
        instance.graph[3] = {1,1,1,1};
        instance.graph[4] = {1,1,1,1};

        Graph_Instance instance_control = instance;

        counting_sort(instance);

        REQUIRE ( instance.N == instance_control.N );
        REQUIRE ( instance_same(instance,instance_control) );
    }

    SECTION ( "complex case" ) {
        Graph_Instance instance = Graph_Instance(5);
        instance.graph[0] = {};
        instance.graph[1] = {4,3,1,3,4,1,3,4,1};
        instance.graph[2] = {2};
        instance.graph[3] = {3,3,4,4,4};
        instance.graph[4] = {4,1,2,3,4,4,0,3,2,2,0,3,4,1,2,3,4,1,0};

        Graph_Instance instance_control = Graph_Instance(5);
        instance_control.graph[0] = {};
        instance_control.graph[1] = {1,1,1,3,3,3,4,4,4};
        instance_control.graph[2] = {2};
        instance_control.graph[3] = {3,3,4,4,4};
        instance_control.graph[4] = {0,0,0,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,4};

        counting_sort(instance);

        REQUIRE ( instance.N == instance_control.N );
        REQUIRE ( instance_same(instance,instance_control) );
    }
}

TEST_CASE ( "pie_operation_test", "[Graph_reduce]" ) {
    SECTION ( "example from original paper" ) {
        vector<int> v0 = {1,2,3};
        vector<int> v1 = {0,5};
        vector<int> v2 = {0,1};
        vector<int> v3 = {4,5};
        vector<int> v4 = {2,5};
        vector<int> v5 = {3,4};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.N = g.size();

        vector<int> v0_control = {1,2};
        vector<int> v1_control = {0};
        vector<int> v2_control = {0};
        vector<int> v3_control = {5};
        vector<int> v4_control = {5};
        vector<int> v5_control = {3,4};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,
                                         v3_control,v4_control,v5_control};

        REQUIRE ( pie_operation(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( instance.N == g.size() );
        REQUIRE ( !pie_operation(instance) );
    }

    SECTION ( "more complex case" ) {
        vector<int> v0  = {1,2,9};
        vector<int> v1  = {3,4,11};
        vector<int> v2  = {0,5,9};
        vector<int> v3  = {1,4,7};
        vector<int> v4  = {1,3,7};
        vector<int> v5  = {2,6};
        vector<int> v6  = {5,13};
        vector<int> v7  = {3,12,13};
        vector<int> v8  = {};
        vector<int> v9  = {2};
        vector<int> v10 = {0,11};
        vector<int> v11 = {10};
        vector<int> v12 = {4,13};
        vector<int> v13 = {};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.N = g.size();

        vector<int> v0_control  = {2};
        vector<int> v1_control  = {3,4};
        vector<int> v2_control  = {9,0,5};
        vector<int> v3_control  = {1,4,7};
        vector<int> v4_control  = {3,1,7};
        vector<int> v5_control  = {2,6};
        vector<int> v6_control  = {5};
        vector<int> v7_control  = {3,12};
        vector<int> v8_control  = {};
        vector<int> v9_control  = {2};
        vector<int> v10_control = {11};
        vector<int> v11_control = {10};
        vector<int> v12_control = {4};
        vector<int> v13_control = {};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,v3_control,v4_control,
                                         v5_control,v6_control,v7_control,v8_control,v9_control,
                                         v10_control,v11_control,v12_control,v13_control};

        REQUIRE ( pie_operation(instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( instance.N == g.size() );
        REQUIRE ( !pie_operation(instance) );
    }
}

TEST_CASE ( "core_operation_test", "[Graph_reduce]" ) {
    vector<int> dfvs(0);

    SECTION ( "example from original paper" ) {
        vector<int> v0 = {1,5};
        vector<int> v1 = {0,5};
        vector<int> v2 = {3,4};
        vector<int> v3 = {2,4};
        vector<int> v4 = {3,5};
        vector<int> v5 = {0,1,2};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5};

        vector<int> map = {0,1,2,3,4,5};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {1,2};
        vector<int> v1_control = {0,2};
        vector<int> v2_control = {1};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control};

        vector<int> map_control = {2,3,4};

        vector<int> dfvs_control = {1,5};

        REQUIRE ( core_operation(instance,dfvs) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(dfvs,dfvs_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 3 );
    }

    SECTION ( "linked cliques case" ) {
        vector<int> v0 = {1,2,3};
        vector<int> v1 = {0,2,3};
        vector<int> v2 = {0,1,3,4,5};
        vector<int> v3 = {0,1,2,4,5};
        vector<int> v4 = {2,3,5};
        vector<int> v5 = {2,3,4};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5};

        vector<int> map = {0,1,2,3,4,5};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<vector<int>> g_control = {};

        vector<int> map_control = {};

        vector<int> dfvs_control = {1,2,3,5};

        REQUIRE ( core_operation(instance,dfvs) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(dfvs,dfvs_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 6 );
        REQUIRE ( !core_operation(instance,dfvs) );
    }

    SECTION ( "bonus case for good measure" ) {
        vector<int> v0 = {1,2};
        vector<int> v1 = {0};
        vector<int> v2 = {1,3,4};
        vector<int> v3 = {2,4};
        vector<int> v4 = {1,2,3,6};
        vector<int> v5 = {};
        vector<int> v6 = {4};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5,v6};

        vector<int> map = {0,1,2,3,4,5,6};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        vector<int> v0_control = {1};
        vector<int> v1_control = {0};
        vector<int> v2_control = {};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control};

        vector<int> map_control = {0,1,5};

        vector<int> dfvs_control = {2,4};

        REQUIRE ( core_operation(instance,dfvs) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(dfvs,dfvs_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( instance.N == g.size() - 4 );
    }

    SECTION ( "only reduces if necessary" ) {
        vector<int> v0 = {1,2};
        vector<int> v1 = {3};
        vector<int> v2 = {1,3};
        vector<int> v3 = {2,0};
        vector<vector<int>> g = {v0,v1,v2,v3};

        vector<int> map = {0,1,2,3,4,5,6};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.map = map;
        instance.N = g.size();

        REQUIRE( !core_operation(instance,dfvs) );
    }
}

TEST_CASE ( "dome_operation_test", "[Graph_reduce]" ) {
    SECTION ( "first example from original paper" ) {
        vector<int> v0 = {2,4};
        vector<int> v1 = {0,2};
        vector<int> v2 = {0,3,5};
        vector<int> v3 = {1,4};
        vector<int> v4 = {1,5};
        vector<int> v5 = {3,4};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.N = g.size();

        vector<int> v0_control = {2,4};
        vector<int> v1_control = {0,2};
        vector<int> v2_control = {0,3};
        vector<int> v3_control = {1};
        vector<int> v4_control = {1,5};
        vector<int> v5_control = {4};
        vector<vector<int>> g_control = {v0_control, v1_control, v2_control,
                                         v3_control, v4_control, v5_control};

        REQUIRE ( dome_operation(instance) );
        REQUIRE ( graph_equal(instance.graph, g_control) );
        REQUIRE ( instance.N == g.size() );
        REQUIRE ( !dome_operation(instance) );
    }

    SECTION ( "second example from original paper") {
        vector<int> v0 = {5};
        vector<int> v1 = {2,4};
        vector<int> v2 = {8};
        vector<int> v3 = {2};
        vector<int> v4 = {2,7};
        vector<int> v5 = {1,6,7};
        vector<int> v6 = {2,4};
        vector<int> v7 = {4};
        vector<int> v8 = {9};
        vector<int> v9 = {0};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5,v6,v7,v8,v9};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.N = g.size();

        vector<int> v0_control = {5};
        vector<int> v1_control = {2};
        vector<int> v2_control = {8};
        vector<int> v3_control = {};
        vector<int> v4_control = {7};
        vector<int> v5_control = {1,6};
        vector<int> v6_control = {2};
        vector<int> v7_control = {4};
        vector<int> v8_control = {9};
        vector<int> v9_control = {0};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,v3_control,v4_control,
                                         v5_control,v6_control,v7_control,v8_control,v9_control};

        REQUIRE( dome_operation(instance) );
        REQUIRE( graph_equal(instance.graph, g_control) );
        REQUIRE ( instance.N == g.size() );
        REQUIRE( !dome_operation(instance) );
    }

    SECTION ( "third example from original paper") {
        vector<int> v0 = {5};
        vector<int> v1 = {2};
        vector<int> v2 = {0,4,6,8};
        vector<int> v3 = {4,5};
        vector<int> v4 = {0,3,8};
        vector<int> v5 = {7};
        vector<int> v6 = {};
        vector<int> v7 = {1};
        vector<int> v8 = {5};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5,v6,v7,v8};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.N = g.size();

        vector<int> v0_control = {5};
        vector<int> v1_control = {2};
        vector<int> v2_control = {8,0};
        vector<int> v3_control = {4};
        vector<int> v4_control = {3};
        vector<int> v5_control = {7};
        vector<int> v6_control = {};
        vector<int> v7_control = {1};
        vector<int> v8_control = {5};
        vector<vector<int>> g_control = {v0_control,v1_control,v2_control,v3_control,v4_control,
                                         v5_control,v6_control,v7_control,v8_control};

        REQUIRE( dome_operation(instance) );
        REQUIRE( graph_equal(instance.graph,g_control) );
        REQUIRE ( instance.N == g.size() );
        REQUIRE( !dome_operation(instance) );
    }

    SECTION ( "bonus case for good measure" ) {
        vector<int> v0 = {1};
        vector<int> v1 = {2,3};
        vector<int> v2 = {3,5};
        vector<int> v3 = {2,5};
        vector<int> v4 = {0};
        vector<int> v5 = {};
        vector<vector<int>> g = {v0,v1,v2,v3,v4,v5};

        Graph_Instance instance = Graph_Instance(0);
        instance.graph = g;
        instance.N = g.size();

        vector<int> v0_control = {1};
        vector<int> v1_control = {2,3};
        vector<int> v2_control = {3};
        vector<int> v3_control = {2};
        vector<int> v4_control = {};
        vector<int> v5_control = {};
        vector<vector<int>> g_control = {v0_control, v1_control, v2_control,
                                         v3_control, v4_control, v5_control};

        vector<int> v0_control_2 = {};
        vector<int> v1_control_2 = {};
        vector<int> v2_control_2 = {3};
        vector<int> v3_control_2 = {2};
        vector<int> v4_control_2 = {};
        vector<int> v5_control_2 = {};
        vector<vector<int>> g_control_2 = {v0_control_2, v1_control_2, v2_control_2,
                                           v3_control_2, v4_control_2, v5_control_2};

        REQUIRE ( dome_operation(instance) );
        REQUIRE ( graph_equal(instance.graph, g_control) );
        REQUIRE ( instance.N == g.size() );
        REQUIRE ( dome_operation(instance) );
        REQUIRE ( graph_equal(instance.graph, g_control_2) );
        REQUIRE ( instance.N == g.size() );
        REQUIRE ( !dome_operation(instance) );
    }
}


// =========================

TEST_CASE ( "path_finder_test" , "[Graph_reduce]" ) {
    SECTION ( "single node" ) {
        Graph_Instance instance(1);
        instance.graph[0] = {0};

        vector<int> parent(instance.N,-1);

        REQUIRE ( path_finder(instance,0,0,parent) );
        REQUIRE ( parent.size() == 1 );
        REQUIRE ( parent[0] == 0 );
    }

    SECTION ( "simple chain" ) {
        Graph_Instance instance(5);
        instance.graph[0] = {2};
        instance.graph[1] = {3};
        instance.graph[2] = {1};
        instance.graph[3] = {4};
        instance.graph[4] = {};

        vector<int> parent(instance.N,-1);

        vector<int> parent_control = {-1,2,0,1,3};

        REQUIRE ( path_finder(instance,0,4,parent) );
        REQUIRE ( parent == parent_control );

        parent = vector(instance.N,-1);

        REQUIRE ( !path_finder(instance,3,1,parent) );
    }

    SECTION ( "single solution" ) {
        Graph_Instance instance(5);
        instance.graph[0] = {1,2};
        instance.graph[1] = {};
        instance.graph[2] = {3,4};
        instance.graph[3] = {4};
        instance.graph[4] = {};

        vector<int> parent(instance.N,-1);

        REQUIRE ( path_finder(instance,0,3,parent) );
        REQUIRE ( (parent[0] == -1 && parent[2] == 0 && parent[3] == 2) );
    }

    SECTION ( "multiple possible solutions" ) {
        Graph_Instance instance(6);
        instance.graph[0] = {1,2};
        instance.graph[1] = {3,4};
        instance.graph[2] = {3,4};
        instance.graph[3] = {5};
        instance.graph[4] = {5};
        instance.graph[5] = {};

        vector<int> parent(instance.N,-1);

        REQUIRE ( path_finder(instance,0,5,parent) );
        REQUIRE ( parent[0] == -1 );
        REQUIRE ( ( ( parent[1] == 0 && parent[3] == 1 && parent[5] == 3 ) ||
                    ( parent[1] == 0 && parent[4] == 1 && parent[5] == 4 ) ||
                    ( parent[2] == 0 && parent[3] == 2 && parent[5] == 3 ) ||
                    ( parent[2] == 0 && parent[4] == 2 && parent[5] == 4 ) ) );

        parent = vector<int>(instance.N,-1);

        REQUIRE ( !path_finder(instance,1,2,parent) );
    }

    SECTION ( "loop in path" ) {
        Graph_Instance instance(4);
        instance.graph[0] = {3};
        instance.graph[1] = {0};
        instance.graph[2] = {};
        instance.graph[3] = {0,2};

        vector<int> parent(instance.N,-1);

        REQUIRE ( path_finder(instance,1,2,parent) );
    }
}

TEST_CASE ( "has_unique_path_test", "[Graph_reduce]" ) {
    SECTION ( "single path" ) {
        Graph_Instance instance(3);
        instance.graph[0] = {2};
        instance.graph[1] = {0};
        instance.graph[2] = {};

        assert(is_sorted(begin(instance.graph[2]),end(instance.graph[2])));

        Graph_Instance instance_control = instance;

        REQUIRE ( has_unique_path(instance,1,2) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );

        REQUIRE ( has_unique_path(instance,0,2) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );

        REQUIRE ( !has_unique_path(instance,2,1) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );

        REQUIRE ( !has_unique_path(instance,2,0) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
    }

    SECTION ( "disconnected vertices" ) {
        Graph_Instance instance(6);
        instance.graph[0] = {1, 2};
        instance.graph[1] = {0, 2};
        instance.graph[2] = {0, 1};
        instance.graph[3] = {4, 5};
        instance.graph[4] = {3, 5};
        instance.graph[5] = {3, 4};

        Graph_Instance instance_control = instance;

        REQUIRE ( !has_unique_path(instance,0,3) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );

        REQUIRE ( !has_unique_path(instance,1,2) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
    }
}

TEST_CASE ( "has_k_paths", "[Graph_reduce]" ) {
    SECTION ( "simple chain" ) {
        Graph_Instance instance(3);
        instance.graph[0] = {2};
        instance.graph[1] = {0};
        instance.graph[2] = {};

        Graph_Instance instance_control = instance;

        SECTION ( "" ) {
            REQUIRE ( has_k_paths(instance,1,1,2) );
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        }
        SECTION ( "" ) {
            REQUIRE ( has_k_paths(instance,1,0,2) );
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        }
        SECTION ( "" ) {
            REQUIRE ( has_k_paths(instance,1,1,0) );
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        }

        SECTION ( "" ) {
            REQUIRE ( !has_k_paths(instance,2,1,2) );
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        }
        SECTION ( "" ) {
            REQUIRE ( !has_k_paths(instance,2,0,2) );
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        }
        SECTION ( "" ) {
            REQUIRE ( !has_k_paths(instance,2,0,1) );
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        }

        SECTION ( "" ) {
            REQUIRE ( !has_k_paths(instance,1,2,1) );
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        }
        SECTION ( "" ) {
            REQUIRE ( !has_k_paths(instance,1,2,0) );
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        }
        SECTION ( "" ) {
            REQUIRE ( !has_k_paths(instance,1,0,1) );
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        }
    }

    SECTION ( "multiple disjoint paths" ) {
        Graph_Instance instance(6);
        instance.graph[0] = {};
        instance.graph[1] = {0};
        instance.graph[2] = {0};
        instance.graph[3] = {1};
        instance.graph[4] = {2, 3, 5};
        instance.graph[5] = {0};

        Graph_Instance instance_control = instance;

        SECTION ("") {
            REQUIRE (has_k_paths(instance, 1, 4, 0));
            REQUIRE (graph_equal(instance.graph, instance_control.graph));
        }SECTION ("") {
            REQUIRE (has_k_paths(instance, 2, 4, 0));
            REQUIRE (graph_equal(instance.graph, instance_control.graph));
        };
        SECTION ("") {
            REQUIRE (has_k_paths(instance, 3, 4, 0));
            REQUIRE (graph_equal(instance.graph, instance_control.graph));
        };
        SECTION ("") {
            REQUIRE (!has_k_paths(instance, 4, 4, 0));
            REQUIRE (graph_equal(instance.graph, instance_control.graph));
        };
    }

    SECTION ( "multiple joint paths" ) {
        Graph_Instance instance(7);
        instance.graph[0] = {};
        instance.graph[1] = {0};
        instance.graph[2] = {1, 6};
        instance.graph[3] = {2};
        instance.graph[4] = {3, 5};
        instance.graph[5] = {2};
        instance.graph[6] = {0};

        Graph_Instance instance_control = instance;

        SECTION ("") {
            REQUIRE (has_k_paths(instance, 1, 4, 0));
            REQUIRE (graph_equal(instance.graph, instance_control.graph));
        };
        SECTION ("") {
            REQUIRE (has_k_paths(instance, 2, 4, 0));
            REQUIRE (graph_equal(instance.graph, instance_control.graph));
        };
        SECTION ("") {
            REQUIRE (!has_k_paths(instance, 3, 4, 0));
            REQUIRE (graph_equal(instance.graph, instance_control.graph));
        };
    }
}

TEST_CASE ( "shortcut_test", "[Graph_reduce]" ) {
    SECTION ( "simple chain" ) {
        Graph_Instance instance(3);
        instance.graph[0] = {2};
        instance.graph[1] = {};
        instance.graph[2] = {1};

        Graph_Instance instance_control = instance;

        REQUIRE ( !shortcut(instance) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
    }

    SECTION ( "simple circle" ) {
        Graph_Instance instance(4);
        instance.graph[0] = {3};
        instance.graph[1] = {2};
        instance.graph[2] = {0};
        instance.graph[3] = {1};

        Graph_Instance instance_control(1);
        instance_control.graph[0] = {0};

        REQUIRE ( shortcut(instance) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
    }

    SECTION ( "circle in path" ) {
        Graph_Instance instance_org(4);
        instance_org.graph[0] = {1};
        instance_org.graph[1] = {2};
        instance_org.graph[2] = {1,3};
        instance_org.graph[3] = {};

        Graph_Instance instance = instance_org;

        Graph_Instance instance_control(3);
        instance_control.graph[0] = {1};
        instance_control.graph[1] = {1,2};
        instance_control.graph[2] = {};

        REQUIRE ( shortcut(instance) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );

        instance = instance_org;
        instance.graph[3] = {0};
        instance.map = {0,1,2,3};

        instance_control.graph.resize(1);
        instance_control.N = 1;
        instance_control.graph[0] = {0};

        REQUIRE ( shortcut(instance) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        REQUIRE ( instance.map.size() == 1 );
        REQUIRE ( instance.map[0] == 1 );
    }

    SECTION ( "Flower" ) {
        // petal(2)=3
        Graph_Instance instance(5);
        instance.graph[0] = {2};
        instance.graph[1] = {2};
        instance.graph[2] = {0,1,4};
        instance.graph[3] = {2};
        instance.graph[4] = {3};
        instance.map = {0,1,2,3,4};

        Graph_Instance instance_control(1);
        instance_control.graph[0] = {0};
        instance_control.map = {2};

        REQUIRE ( shortcut(instance) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        REQUIRE ( instance.map == instance_control.map );
    }

    SECTION ( "complex case" ) {
        Graph_Instance instance(9);
        instance.graph[0] = {2};
        instance.graph[1] = {2};
        instance.graph[2] = {0,1,4};
        instance.graph[3] = {2,5};
        instance.graph[4] = {3};
        instance.graph[5] = {6};
        instance.graph[6] = {5,8};
        instance.graph[7] = {4,8};
        instance.graph[8] = {4};

        instance.map = {0,1,2,3,4,5,6,7,8};

        Graph_Instance instance_control(4);
        instance_control.graph[0] = {0,1};
        instance_control.graph[1] = {0,2};
        instance_control.graph[2] = {1,2};
        instance_control.graph[3] = {1};

        instance_control.map = {2,3,5,7};

        REQUIRE ( shortcut(instance) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        REQUIRE ( instance.map == instance_control.map );
    }
}

TEST_CASE ( "flower_test", "[Graph_reduce]" ) {
    SECTION ( "simple chain" ) {
        Graph_Instance instance(3);
        instance.graph[0] = {2};
        instance.graph[1] = {};
        instance.graph[2] = {1};
        instance.map = {0,1,2};

        Graph_Instance instance_control = instance;
        vector<int> dfvs(0);

        REQUIRE ( !flower(instance,1,dfvs) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        REQUIRE ( instance.map == instance_control.map );
        REQUIRE ( !flower(instance,42,dfvs) );
        REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
        REQUIRE ( instance.map == instance_control.map );
    }

    SECTION ( "simple circle" ) {
        Graph_Instance instance(4);
        instance.graph[0] = {3};
        instance.graph[1] = {2};
        instance.graph[2] = {0};
        instance.graph[3] = {1};
        instance.map = {0,1,2,3};

        Graph_Instance instance_control = instance;
        vector<int> dfvs(0);

        SECTION ( "k = 1" ) {
            REQUIRE ( flower(instance,1,dfvs) );
            REQUIRE ( dfvs.size() == 1 );
            Vertex_op::delete_vertex(dfvs[0],instance_control);
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
            REQUIRE ( instance.map == instance_control.map );
        }

        SECTION ( "k > 1" ) {
            REQUIRE ( !flower(instance,2,dfvs) );
            REQUIRE ( dfvs.empty() );
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
            REQUIRE ( instance.map == instance_control.map );
        }
    }

    SECTION ( "circle in path" ) {
        Graph_Instance instance(4);
        instance.graph[0] = {1};
        instance.graph[1] = {2};
        instance.graph[2] = {1, 3};
        instance.graph[3] = {};
        instance.map = {0,1,2,3};

        Graph_Instance instance_control = instance;
        vector<int> dfvs(0);

        SECTION ( "k = 1" ) {
            REQUIRE ( flower(instance,1,dfvs) );
            REQUIRE ( ( (dfvs.size() == 1 && (dfvs[0] == 1 || dfvs[0] == 2)) ||
                        (dfvs.size() == 2) ) );
            for ( int v : dfvs ) Vertex_op::delete_vertex(v,instance_control);
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
            REQUIRE ( instance.map == instance_control.map );
        }

        SECTION ( "k > 1" ) {
            REQUIRE ( !flower(instance,2,dfvs) );
            REQUIRE ( dfvs.empty() );
        }
    }

    SECTION ( "Flower" ) {
        // petal(2)=3
        Graph_Instance instance(5);
        instance.graph[0] = {2};
        instance.graph[1] = {2};
        instance.graph[2] = {0,1,4};
        instance.graph[3] = {2};
        instance.graph[4] = {3};
        instance.map = {0,1,2,3,4};

        Graph_Instance instance_control(4);
        instance_control.graph[0] = {};
        instance_control.graph[1] = {};
        instance_control.graph[2] = {};
        instance_control.graph[3] = {2};
        instance_control.map = {0,1,3,4};
        vector<int> dfvs(0);

        REQUIRE ( !flower(instance,4,dfvs) );
        SECTION ( "k = 3" ) {
            REQUIRE ( flower(instance,3,dfvs) );
            REQUIRE ( dfvs.size() == 1 );
            REQUIRE ( dfvs[0] == 2);
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
            REQUIRE ( instance.map == instance_control.map );
        }

        SECTION ( "k = 2" ) {
            REQUIRE ( flower(instance,2,dfvs) );
            REQUIRE ( dfvs.size() == 1 );
            REQUIRE ( dfvs[0] == 2);
            REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
            REQUIRE ( instance.map == instance_control.map );
        }

        // k = 1 applies to any vertex
    }
}

// =========================

TEST_CASE ( "has_k_cycle_test", "[Graph_reduce]" ) {
    SECTION ( "simple chain" ) {
        Graph_Instance instance(3);
        instance.graph[0] = {2};
        instance.graph[1] = {0};
        instance.graph[2] = {};

        vector<int> cycle;
        REQUIRE ( !has_k_cycle(instance,1,0,cycle) );
        REQUIRE ( !has_k_cycle(instance,1,1,cycle) );
        REQUIRE ( !has_k_cycle(instance,1,2,cycle) );
        REQUIRE ( !has_k_cycle(instance,2,1,cycle) );
        REQUIRE ( !has_k_cycle(instance,1,2,cycle) );
    }

    SECTION ( "simple circle" ) {
        Graph_Instance instance(5);
        instance.graph[0] = {1};
        instance.graph[1] = {2};
        instance.graph[2] = {4};
        instance.graph[3] = {0};
        instance.graph[4] = {3};

        vector<int> cycle;

        SECTION ( "length < 5" ) {
            REQUIRE ( !has_k_cycle(instance,0,0,cycle) );
            REQUIRE ( !has_k_cycle(instance,0,1,cycle) );
            REQUIRE ( !has_k_cycle(instance,1,2,cycle) );
            REQUIRE ( !has_k_cycle(instance,3,3,cycle) );
            REQUIRE ( !has_k_cycle(instance,2,4,cycle) );
        }

        SECTION ( "length = 5" ) {
            vector<int> cycle_control = {3,4,2,1,0};
            REQUIRE ( has_k_cycle(instance,3,5,cycle) );
            REQUIRE ( cycle == cycle_control );
        }

        SECTION ( "length > 5" ) {
            vector<int> cycle_control = {3,4,2,1,0};
            REQUIRE ( has_k_cycle(instance,3,7,cycle) );
            REQUIRE ( cycle == cycle_control );
        }
    }

    SECTION ( "disconnencted graph" ) {
        Graph_Instance instance(6);
        instance.graph[0] = {1};
        instance.graph[1] = {2};
        instance.graph[2] = {};
        instance.graph[3] = {4};
        instance.graph[4] = {3,5};
        instance.graph[5] = {3};

        vector<int> cycle;

        SECTION ( "acyclic part" ) {
            REQUIRE ( !has_k_cycle(instance,0,1,cycle) );
            REQUIRE ( !has_k_cycle(instance,1,1,cycle) );
            REQUIRE ( !has_k_cycle(instance,2,1,cycle) );
        }

        SECTION ( "k = 2, start = 3" ) {
            vector<int> cycle_control = {3,4};
            REQUIRE ( !has_k_cycle(instance,5,2,cycle) );
            REQUIRE ( has_k_cycle(instance,3,2,cycle) );
            REQUIRE ( cycle == cycle_control );
        }

        SECTION ( "k = 2, start = 4" ) {
            vector<int> cycle_control = {4,3};
            REQUIRE ( !has_k_cycle(instance,5,2,cycle) );
            REQUIRE ( has_k_cycle(instance,4,2,cycle) );
            REQUIRE ( cycle == cycle_control );
        }

        SECTION ( "k = 3, start = 5" ) {
            vector<int> cycle_control = {5,4,3};
            REQUIRE ( has_k_cycle(instance,5,3,cycle) );
            REQUIRE ( cycle == cycle_control );
        }

        SECTION ( "k = 3, start = 3" ) {
            vector<int> cycle_control = {3,4};
            REQUIRE ( has_k_cycle(instance,3,2,cycle) );
            REQUIRE ( cycle == cycle_control );
        }
    }
}

TEST_CASE ( "calc_common_neighbor_test", "[Graph_reduce]" ) {
    Graph_Instance instance(10);
    instance.graph[0] = {};
    instance.graph[1] = {2,3};
    instance.graph[2] = {1,3,4};
    instance.graph[3] = {1,2,4,5};
    instance.graph[4] = {2,3,5};
    instance.graph[5] = {3,4};
    instance.graph[6] = {7};
    instance.graph[7] = {6,8,9};
    instance.graph[8] = {7};
    instance.graph[9] = {7};

    vector<pair<int,int>> edges = {{1, 2}, {1, 3}, {2, 3}, {2, 4}, {3, 4},
                                   {3, 5}, {4, 5}, {6, 7}, {7, 8}, {7, 9}};
    vector<pair<int,int>> edges_control = edges;

    vector<int> comm_neigh;
    vector<int> comm_neigh_control = {1,1,2,1,2,1,1,0,0,0};

    calc_common_neighbor(instance,edges,comm_neigh);

    REQUIRE ( edges == edges_control );
    REQUIRE ( comm_neigh == comm_neigh_control );
}

TEST_CASE ( "update_edges_test", "[Graph_reduce]" ) {
    Graph_Instance instance(10);
    instance.graph[0] = {};
    instance.graph[1] = {2,3};
    instance.graph[2] = {1,3,4};
    instance.graph[3] = {1,2,4,5};
    instance.graph[4] = {2,3,5};
    instance.graph[5] = {3,4};
    instance.graph[6] = {7};
    instance.graph[7] = {6,8,9};
    instance.graph[8] = {7};
    instance.graph[9] = {7};

    vector<pair<int,int>> edges = {{1, 2}, {1, 3}, {2, 3}, {2, 4}, {3, 4},
                                   {3, 5}, {4, 5}, {6, 7}, {7, 8}, {7, 9}};

    SECTION ( "(2,3)" ) {
        vector<pair<int, int>> edges_control = {{1, 2},{2, 4},{4, 5},
                                                {6, 7},{7, 8},{7, 9}};
        int index = 2;

        update_edges(edges, index);

        REQUIRE (edges == edges_control);
    }

    SECTION ( "(2,4)" ) {
        vector<pair<int, int>> edges_control = {{1, 3}, {2, 3}, {3, 5},
                                                {6, 7}, {7, 8}, {7, 9}};
        int index = 3;

        update_edges(edges, index);

        REQUIRE (edges == edges_control);
    }

    SECTION ( "(7,8)" ) {
        vector<pair<int, int>> edges_control = {{1, 2}, {1, 3}, {2, 3}, {2, 4},
                                                {3, 4}, {3, 5}, {4, 5}};
        int index = 8;

        update_edges(edges, index);

        REQUIRE (edges == edges_control);
    }
}

TEST_CASE ( "update_node_list_test", "[Graph_reduce]" ) {
    Graph_Instance instance(10);
    instance.graph[0] = {};
    instance.graph[1] = {2,3};
    instance.graph[2] = {1,3,4};
    instance.graph[3] = {1,2,4,5};
    instance.graph[4] = {2,3,5};
    instance.graph[5] = {3,4};
    instance.graph[6] = {7};
    instance.graph[7] = {6,8,9};
    instance.graph[8] = {7};
    instance.graph[9] = {7};

    Graph_Instance instance_final(10);
    instance_final.graph[0] = {};
    instance_final.graph[1] = {2};
    instance_final.graph[2] = {1,4};
    instance_final.graph[3] = {1,2,4,5};
    instance_final.graph[4] = {2,5};
    instance_final.graph[5] = {4};
    instance_final.graph[6] = {};
    instance_final.graph[7] = {9};
    instance_final.graph[8] = {};
    instance_final.graph[9] = {};

    vector<pair<int,int>> edges = {{1, 2}, {1, 3}, {2, 3}, {2, 4}, {3, 4},
                                   {3, 5}, {4, 5}, {6, 7}, {7, 8}, {7, 9}};

    vector<vector<int>> node_list = {{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}};
    vector<vector<int>> node_list_control1 = {{0},{1},{2,3},{4},{5},{6},{7},{8},{9}};
    vector<vector<int>> node_list_control2 = {{0},{1},{2,3},{4},{5},{6,8},{7},{9}};
    vector<vector<int>> node_list_control3 = {{0},{1},{2,3},{4},{5},{6,7,8},{9}};
    vector<vector<int>> node_list_control4 = {{0},{1},{2,3,6,7,8},{4},{5},{9}};

    update_node_list(node_list,instance,2,3);
    REQUIRE ( node_list == node_list_control1 );

    update_node_list(node_list,instance,6,8);
    REQUIRE ( node_list == node_list_control2 );

    update_node_list(node_list,instance,6,7);
    REQUIRE ( node_list == node_list_control3 );

    update_node_list(node_list,instance,2,6);
    REQUIRE ( node_list == node_list_control4 );
    REQUIRE ( graph_equal(instance.graph,instance_final.graph) );
}

TEST_CASE ( "remove_max_clique_test", "[Graph_reduce]" ) {
    Graph_Instance instance(10);
    instance.graph[0] = {};
    instance.graph[1] = {2,3};
    instance.graph[2] = {1,3,4};
    instance.graph[3] = {1,2,4,5};
    instance.graph[4] = {2,3,5};
    instance.graph[5] = {3,4};
    instance.graph[6] = {7};
    instance.graph[7] = {6,8,9};
    instance.graph[8] = {7};
    instance.graph[9] = {7};
    instance.map = {0,1,2,3,4,5,6,7,8,9};

    Graph_Instance g_PIE = instance;

    Graph_Instance instance_control(7);
    instance_control.graph[0] = {};
    instance_control.graph[1] = {2};
    instance_control.graph[2] = {1};
    instance_control.graph[3] = {4};
    instance_control.graph[4] = {3,5,6};
    instance_control.graph[5] = {4};
    instance_control.graph[6] = {4};
    instance_control.map = {0,4,5,6,7,8,9};

    REQUIRE ( remove_max_clique(instance,g_PIE) );
    REQUIRE ( graph_equal(instance.graph,instance_control.graph) );
    REQUIRE ( instance.map == instance_control.map );
}