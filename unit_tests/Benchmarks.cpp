//
// Created by Ruben Götz on 30.01.2022.
//

#include "catch.hpp"
#include "../src/Branch_and_bound.h"
#include "../src/Parser.h"
#include "../src/SCC_compute.h"
#include "Test_helper.h"
#include "Logs.h"
#include "iostream"
#include <ctime>
#include <chrono>
#include <fstream>
#include <string>
using namespace std;
using namespace Test_helper;
using namespace Parser;

#define GRAPH_DIR "../../example_instances/exact_public/"
#define SOLUTION_DIR "../../example_instances/exact_public_solution/"
#define LOG_DIR "../../example_instances/logs/"
#define BENCHMARK_FILE "../../example_instances/exact_public_solution/benchmarks"
#define INITIAL_SOLUTIONS_FILE "../../example_instances/exact_public_solution/initial_solutions"
#define REDUCTION_FILE "../../example_instances/exact_public_solution/reductions"
#define FIRST_INSTANCE 1
#define LAST_INSTANCE 199
#define TIMEOUT_SEC_BENCHMARKS 30
#define TIMEOUT_SEC_CORRECTNESS 3
#define SOLVED_BENCHMARKS 42
#define SOLVED_CORRECTNESS 40

TEST_CASE ( "Benchmarks for all public Instances", "[Benchmarks],[turtle]") {
    int setrlimit(int RLIMIT_STACK, const struct rlimit *RLIM_INFINITY);
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    string file_name;
    chrono::steady_clock::time_point timeout;
    chrono::steady_clock::time_point t1;
    chrono::steady_clock::time_point t2;

    Graph_Instance instance = Graph_Instance(0);
    vector<int> current_dfvs;
    vector<int> upper_bound;
    int timeout_occurred;
    double calculation_time;
    int count_solved = 0;

    ofstream output;
    output.open(BENCHMARK_FILE);
    REQUIRE ( output );

    output << "Timeout is set to: " << TIMEOUT_SEC_BENCHMARKS << "s" << endl;
    output << "Instance" << "\t" << "|dfvs|" << "\t\t" << "correct" << "\t\t" << "seconds" << endl;

    for (int i = FIRST_INSTANCE; i <= LAST_INSTANCE; i = i + 2) {
        // create filename
        if ( i < 10 ) file_name = "e_00" + to_string(i);
        else if (i < 100) file_name = "e_0" + to_string(i);
        else file_name = "e_" + to_string(i);

        Logs::reset();
        Logs::bound_path = LOG_DIR + file_name + "_upper_bounds";
        Logs::open_bound_file();

        instance.graph = {};
        string graph_path = GRAPH_DIR + file_name;
        string solution_path = SOLUTION_DIR + file_name + "_solution";
        if ( Parser::extract_graph(graph_path,instance) ) {
            current_dfvs = {};
            upper_bound = instance.map;
            //upper_bound = {};

            output << file_name << "\t\t";

            timeout = chrono::steady_clock::now() + chrono::seconds(TIMEOUT_SEC_BENCHMARKS);
            timeout_occurred = 0;
            try {
                t1 = chrono::steady_clock::now();
                //Branch_and_bound::solve_greedy(instance,upper_bound, timeout);
                Graph_reduce::counting_sort(instance);
                for ( auto &v : instance.graph ) assert(is_sorted(begin(v), end(v)));
                for ( auto &v : instance.graph ) assert(unique(begin(v), end(v)) == end(v));
                Branch_and_bound::solve(current_dfvs, instance, upper_bound,timeout);
                t2 = chrono::steady_clock::now();
                chrono::duration<double> time_span = chrono::duration_cast<chrono::duration<double>>(t2 - t1);
                calculation_time = time_span.count();
                ++count_solved;
            }
            catch (...) {
                timeout_occurred = 1;
            }

            Parser::output_dfvs(solution_path,upper_bound);

            if ( upper_bound.size() > 999 ) output << upper_bound.size() << "\t\t";
            else output << upper_bound.size() << "\t\t\t";

            if ( timeout_occurred ) output << "-" << "\t\t\t" << "timeout";
            else if ( is_solution(solution_path, graph_path) )
                output << "yes" << "\t\t\t" << calculation_time;
            else output << "no" << "\t\t\t" << calculation_time;
            output << endl;
        }

        Logs::close_bound_file();
        Logs::print_log(LOG_DIR + file_name + "_logs");
    }
    output.close();
    REQUIRE ( count_solved == SOLVED_BENCHMARKS );
}

TEST_CASE ( "Check for correctness of small instances", "[Benchmarks]" ) {
    // -1 marks not solved, -2 marks filler element
    vector<int> solutions = {
            -2,
            2,-2,
            674,-2,
            631,-2,
            276,-2,
            450,-2,
            1917,-2,
            547,-2,
            788,-2,
            364,-2,
            1137,-2,
            510,-2,
            452,-2,
            273,-2,
            575,-2,
            715,-2,
            2079,-2,
            277,-2,
            2179,-2,
            -1,-2,
            400,-2,
            -1,-2,
            521,-2,
            -1,-2,
            -1,-2,
            2043,-2,
            2086,-2,
            2628,-2,
            1948,-2,
            1955,-2,
            214,-2,
            554,-2,
            2267,-2,
            3709,-2,
            513,-2,
            2092,-2,
            675,-2,
            4420,-2,
            4069,-2,
            5292,-2,
            4906,-2,
            4887,-2,
            745,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            755,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            4752,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1,-2,
            -1
    };

    string file_name;
    chrono::steady_clock::time_point timeout;
    int count_solved = 0;

    Graph_Instance instance = Graph_Instance(0);
    vector<int> current_dfvs;
    vector<int> upper_bound;

    for (int i = FIRST_INSTANCE; i <= LAST_INSTANCE; i = i + 2) {
        // create filename
        if ( i < 10 ) file_name = "e_00" + to_string(i);
        else if (i < 100) file_name = "e_0" + to_string(i);
        else file_name = "e_" + to_string(i);

        instance.graph = {};
        string graph_path = GRAPH_DIR + file_name;
        string solution_path = SOLUTION_DIR + file_name + "_solution";
        if ( extract_graph(graph_path,instance) ) {
            current_dfvs = {};
            upper_bound.resize(instance.N);
            for (int j = 0; j < instance.N; ++j) {
                upper_bound[j] = j;
            }

            timeout = chrono::steady_clock::now() + chrono::seconds(TIMEOUT_SEC_CORRECTNESS);
            try {
                Branch_and_bound::solve(current_dfvs, instance, upper_bound,timeout);
                ++count_solved;
                REQUIRE ( upper_bound.size() == solutions[i] );
            }
            catch (...) {
                REQUIRE ( solutions[i] == -1 );
            }
        }
    }

    REQUIRE ( count_solved == SOLVED_CORRECTNESS );
}

TEST_CASE ( "Calculate initial solutions", "[Benchmarks],[turtle]" ) {
    int setrlimit(int RLIMIT_STACK, const struct rlimit *RLIM_INFINITY);

    string file_name;
    chrono::steady_clock::time_point t1;
    chrono::steady_clock::time_point t2;

    Graph_Instance instance = Graph_Instance(0);
    vector<int> dfvs;
    int initial_N;
    double calculation_time;

    ofstream output;
    output.open(INITIAL_SOLUTIONS_FILE);
    REQUIRE ( output );

    output << "Instance" << "\t" << "|dfvs|" << "\t\t" << "N" << "\t\t" << "seconds" << endl;

    for (int i = FIRST_INSTANCE; i <= LAST_INSTANCE; i = i + 2) {
        // create filename
        if ( i < 10 ) file_name = "e_00" + to_string(i);
        else if (i < 100) file_name = "e_0" + to_string(i);
        else file_name = "e_" + to_string(i);

        instance.graph = {};
        string graph_path = GRAPH_DIR + file_name;
        if ( Parser::extract_graph(graph_path,instance) ) {
            dfvs = {};
            Graph_Instance initial_instance = instance;
            initial_N = instance.N;

            output << file_name << "\t\t";

            t1 = chrono::steady_clock::now();
            Branch_and_bound::solve_greedy(instance,dfvs);
            t2 = chrono::steady_clock::now();
            chrono::duration<double> time_span = chrono::duration_cast<chrono::duration<double>>(t2 - t1);
            calculation_time = time_span.count();

            // checks whether dfvs actually is a dfvs
            vector<int> alive(initial_N,1);
            for ( int v : dfvs )
               alive[v] = 0;
            REQUIRE( Branch_and_bound::is_acyclic(alive,initial_instance) );
            //Vertex_op::delete_vertex_set(dfvs,initial_instance);
            //vector<int> coloration(initial_instance.N,-1);
            //SCC_compute sc = SCC_compute(initial_instance);
            //sc.getSCCcoloration(coloration);
            //REQUIRE( sc.scc_count == initial_instance.N );

            if ( dfvs.size() > 999 ) output << dfvs.size() << "\t\t";
            else output << dfvs.size() << "\t\t\t";

            if ( initial_N > 999 ) output << initial_N << "\t";
            else output << initial_N << "\t\t";

            output << calculation_time << endl;
        }
    }
    output.close();
}

TEST_CASE ( "Reduce once", "[Benchmarks]" ) {
    int setrlimit(int RLIMIT_STACK, const struct rlimit *RLIM_INFINITY);
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    string file_name;
    chrono::steady_clock::time_point t1;
    chrono::steady_clock::time_point t2;

    Graph_Instance instance = Graph_Instance(0);
    vector<int> dfvs, upper_bound;
    int initial_N, initial_E, new_E;
    double calculation_time;

    ofstream output;
    output.open(REDUCTION_FILE);
    REQUIRE ( output );

    output << "Instance" << "\t" << "N_reduced" << "\t" << "N_initial"<< "\t" << "E_reduced" << "\t" << "E_initial"
            << "\t" << "|dfvs|"<< "\t" << "seconds" << endl;

    for (int i = FIRST_INSTANCE; i <= LAST_INSTANCE; i = i + 2) {
        // create filename
        if ( i < 10 ) file_name = "e_00" + to_string(i);
        else if (i < 100) file_name = "e_0" + to_string(i);
        else file_name = "e_" + to_string(i);

        instance.graph = {};
        string graph_path = GRAPH_DIR + file_name;
        if ( Parser::extract_graph(graph_path,instance) ) {
            dfvs = {};
            upper_bound = instance.map;
            initial_N = instance.N;
            initial_E = 0;
            new_E = 0;
            for (auto v : instance.graph)
                for (int i : v) ++initial_E;

            output << file_name << "\t\t";

            t1 = chrono::steady_clock::now();
            Graph_reduce::reduce_graph(instance,dfvs,upper_bound);
            t2 = chrono::steady_clock::now();
            chrono::duration<double> time_span = chrono::duration_cast<chrono::duration<double>>(t2 - t1);
            calculation_time = time_span.count();

            for (auto v : instance.graph)
                for (int i : v) ++new_E;

            output << instance.N << "\t\t";
            if ( instance.N < 1000) output << "\t";
            output << initial_N << "\t";
            if (initial_N < 10000) output <<"\t";
            if (initial_N < 1000) output <<"\t";
            output << new_E << "\t\t";
            if (new_E < 1000) output <<"\t";
            output << initial_E << "\t\t";
            if (initial_E < 1000) output <<"\t";
            output << dfvs.size() << "\t";
            if (dfvs.size() < 1000) output <<"\t";
            output << calculation_time << "\n";
            output.flush();
        }
    }
    output.close();
}

TEST_CASE ( "ja", "" ) {
    vector<int> v0 = {};
    vector<int> v1 = {1,1,1};
    vector<int> v2 = {1,2,3,4};
    vector<int> v = {1,2,5,6,9};
    int j = 6;
    v0.erase(remove(begin(v0), end(v0),5),end(v0));
    v1.erase(remove(begin(v1), end(v1),1),end(v1));
    v2.erase(remove(begin(v2), end(v2),12),end(v2));

    v.erase(remove(begin(v), end(v),j),end(v));
}