//
// Created by Ruben Götz on 22.01.2022.
//

#include "catch.hpp"
#include "../src/Branch_and_bound.h"
#include "Test_helper.h"
#include "../src/Parser.h"
#include <vector>
using namespace std;
using namespace Branch_and_bound;

#define E_001 "../../unit_tests/test_graphs/e_001"

TEST_CASE ( "get_highest_degree_vertex_test", "[Branch_and_bound]" ) {
    SECTION ( "empty graph" ) {
        Graph_Instance instance(0);
        vector<int> alive(0);

        REQUIRE ( get_highest_degree_vertex(instance) == 0 );
        REQUIRE ( get_highest_degree_vertex(alive,instance) == 0 );
    }

    SECTION ( "single node" ) {
        Graph_Instance instance(1);
        instance.graph[0] = {};

        vector<int> alive1(1,1);
        vector<int> alive2(1,0);

        REQUIRE ( get_highest_degree_vertex(instance) == 0 );
        REQUIRE ( get_highest_degree_vertex(alive1,instance) == 0 );
        REQUIRE ( get_highest_degree_vertex(alive2,instance) == 0 );

        instance.graph[0] = {0};

        REQUIRE ( get_highest_degree_vertex(instance) == 0 );
        REQUIRE ( get_highest_degree_vertex(alive1,instance) == 0 );
        REQUIRE ( get_highest_degree_vertex(alive2,instance) == 0 );
    }

    SECTION ( "complex case" ) {
        Graph_Instance instance(5);
        instance.graph[0] = {3};
        instance.graph[1] = {3};
        instance.graph[2] = {1,4};
        instance.graph[3] = {4,2};
        instance.graph[4] = {};

        vector<int> alive1(5,1);
        vector<int> alive2 = {1,1,0,1,1};
        vector<int> alive3 = {1,1,1,0,1};

        REQUIRE( get_highest_degree_vertex(instance) == 3 );
        REQUIRE( get_highest_degree_vertex(alive1,instance) == 3 );
        REQUIRE( get_highest_degree_vertex(alive2,instance) == 3 );
        REQUIRE( get_highest_degree_vertex(alive3,instance) == 2 );
    }
}

TEST_CASE ( "in_times_out_test", "[Branch_and_bound]" ) {
    SECTION ( "empty graph" ) {
        Graph_Instance instance(0);
        vector<int> alive(0);

        REQUIRE ( in_times_out(instance) == 0 );
        REQUIRE ( in_times_out(alive,instance) == 0 );
    }

    SECTION ( "single node" ) {
        Graph_Instance instance(1);
        instance.graph[0] = {};

        vector<int> alive1(1,1);
        vector<int> alive2(1,0);

        REQUIRE ( in_times_out(instance) == 0 );
        REQUIRE ( in_times_out(alive1,instance) == 0 );
        REQUIRE ( in_times_out(alive2,instance) == 0 );

        instance.graph[0] = {0};

        REQUIRE ( in_times_out(instance) == 0 );
        REQUIRE ( in_times_out(alive1,instance) == 0 );
        REQUIRE ( in_times_out(alive2,instance) == 0 );
    }

    SECTION ( "complex case" ) {
        Graph_Instance instance(5);
        instance.graph[0] = {3};
        instance.graph[1] = {3};
        instance.graph[2] = {1,4};
        instance.graph[3] = {4,2};
        instance.graph[4] = {};

        vector<int> alive1(5,1);
        vector<int> alive2 = {1,1,0,1,1};
        vector<int> alive3 = {1,1,1,0,1};

        REQUIRE( in_times_out(instance) == 3 );
        REQUIRE( in_times_out(alive1,instance) == 3 );
        REQUIRE( in_times_out(alive2,instance) == 3 );
        int ito = in_times_out(alive3,instance);
        REQUIRE( (ito == 0 || ito == 1 || ito == 2 || ito == 4) );
    }
}

/*
TEST_CASE ( "append_vector_test", "[Branch_and_bound]" ) {
    SECTION ( "simple case" ) {
        vector<int> v1 = {0,1,2};
        vector<int> v2 = {3,4,5};

        vector<int> v_control = {0,1,2,3,4,5};

        Branch_and_bound::append_vector(&v1,&v2);

        REQUIRE( Test_helper::vector_equal(v1,v_control) );
    }

    SECTION ( "append empty vector" ) {
        vector<int> v1 = {0,1,2};
        vector<int> v2 = {};

        vector<int> v_control = {0,1,2};

        Branch_and_bound::append_vector(&v1,&v2);

        REQUIRE( Test_helper::vector_equal(v1,v_control) );
    }

    SECTION ( "append to empty vector" ) {
        vector<int> v1 = {};
        vector<int> v2 = {3,4,5};

        vector<int> v_control = {3,4,5};

        Branch_and_bound::append_vector(&v1,&v2);

        REQUIRE( Test_helper::vector_equal(v1,v_control) );
    }

    SECTION ( "append two empty vectors" ) {
        vector<int> v1 = {};
        vector<int> v2 = {};

        vector<int> v_control = {};

        Branch_and_bound::append_vector(&v1,&v2);

        REQUIRE( Test_helper::vector_equal(v1,v_control) );
    }
}
*/

TEST_CASE ( "is_acyclic_test", "[Branch_and_bound]" ) {
    SECTION ( "empty graph" ) {
        Graph_Instance instance(0);
        vector<int> alive(0);

        REQUIRE( is_acyclic(alive, instance) );
    }

    SECTION ( "simple circle" ) {
        Graph_Instance instance(4);
        instance.graph[0] = {3};
        instance.graph[1] = {2};
        instance.graph[2] = {1};
        instance.graph[3] = {0};

        vector<int> alive(4,1);

        REQUIRE( !is_acyclic(alive,instance) );
    }

    SECTION ( "diamond" ) {
        Graph_Instance instance(4);
        instance.graph[0] = {1,2};
        instance.graph[1] = {3};
        instance.graph[2] = {3};
        instance.graph[3] = {};
        vector<int> alive(4,1);

        REQUIRE( is_acyclic(alive,instance) );
    }

    SECTION ( "simple chain" ) {
        Graph_Instance instance(4);
        instance.graph[0] = {3};
        instance.graph[1] = {2};
        instance.graph[2] = {};
        instance.graph[3] = {1};

        vector<int> alive(4,1);

        REQUIRE( is_acyclic(alive,instance) );
    }

    SECTION ( "tree" ) {
        Graph_Instance instance(7);
        instance.graph[0] = {1};
        instance.graph[1] = {};
        instance.graph[2] = {0,3};
        instance.graph[3] = {4,5,6};
        instance.graph[4] = {};
        instance.graph[5] = {};
        instance.graph[6] = {};

        vector<int> alive(7,1);

        REQUIRE( is_acyclic(alive,instance) );
    }

    SECTION ( "complex case" ) {
        Graph_Instance instance(8);
        instance.graph[0] = {1};
        instance.graph[1] = {4};
        instance.graph[2] = {0,3};
        instance.graph[3] = {4,5,6};
        instance.graph[4] = {5,7};
        instance.graph[5] = {};
        instance.graph[6] = {};
        instance.graph[7] = {2};

        vector<int> alive1(8,1);
        vector<int> alive2 = {1,1,1,1,1,1,1,0};

        REQUIRE( !is_acyclic(alive1,instance) );
        REQUIRE( is_acyclic(alive2,instance) );
    }
}