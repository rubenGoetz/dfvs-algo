//
// Created by Ruben Götz on 27.04.2022.
//

#include "Logs.h"
#include <iostream>
#include <fstream>

string Logs::bound_path = "";
int Logs::reduce_graph = 0;
int Logs::in0 = 0;
int Logs::out0 = 0;
int Logs::loop = 0;
int Logs::in1 = 0;
int Logs::out1 = 0;
int Logs::pie_op = 0;
int Logs::core_op = 0;
int Logs::dome_op = 0;
int Logs::shortcut = 0;
int Logs::flower = 0;
int Logs::counting_sort = 0;
int Logs::calculate_pie = 0;
int Logs::calculate_notpie = 0;
int Logs::calculate_pie_notpie = 0;
int Logs::is_clique = 0;
int Logs::isin = 0;
int Logs::isin_pos = 0;
int Logs::remove_duplicates = 0;
int Logs::lower_bound_matching = 0;
int Logs::path_finder = 0;
int Logs::has_unique_path = 0;
int Logs::has_k_paths = 0;
int Logs::getSCCcoloration = 0;
int Logs::delete_vertex = 0;
int Logs::delete_vertex_set = 0;
int Logs::merge_vertices = 0;
int Logs::merge_vertices_set = 0;
int Logs::replace_vertex = 0;
int Logs::solve = 0;
int Logs::solve_greedy = 0;
int Logs::get_branching_index = 0;
int Logs::get_branching_index_greedy = 0;
int Logs::append_vector = 0;
int Logs::is_acyclic = 0;

int Logs::in0_red = 0;
int Logs::out0_red = 0;
int Logs::loop_red = 0;
int Logs::in1_red = 0;
int Logs::out1_red = 0;
int Logs::pie_op_red = 0;
int Logs::core_op_red = 0;
int Logs::dome_op_red = 0;
int Logs::shortcut_red = 0;
int Logs::flower_red = 0;

ofstream Logs::bound_file;

void Logs::open_bound_file() {
    bound_file.open(bound_path, ios_base::app);
}

void Logs::close_bound_file() {
    if ( bound_file ) bound_file.close();
}

void Logs::current_upper_bound_size(int size){
    bound_file << size << "\n";
}

void Logs::reset() {
    bound_path = "";
    reduce_graph = 0;
    in0 = 0;
    out0 = 0;
    loop = 0;
    in1 = 0;
    out1 = 0;
    pie_op = 0;
    core_op = 0;
    dome_op = 0;
    shortcut = 0;
    flower = 0;
    counting_sort = 0;
    calculate_pie = 0;
    calculate_notpie = 0;
    calculate_pie_notpie = 0;
    is_clique = 0;
    isin = 0;
    isin_pos = 0;
    remove_duplicates = 0;
    lower_bound_matching = 0;
    path_finder = 0;
    has_unique_path = 0;
    has_k_paths = 0;
    getSCCcoloration = 0;
    delete_vertex = 0;
    delete_vertex_set = 0;
    merge_vertices = 0;
    merge_vertices_set = 0;
    replace_vertex = 0;
    solve = 0;
    solve_greedy = 0;
    get_branching_index = 0;
    get_branching_index_greedy = 0;
    append_vector = 0;
    is_acyclic = 0;

    in0_red = 0;
    out0_red = 0;
    loop_red = 0;
    in1_red = 0;
    out1_red = 0;
    pie_op_red = 0;
    core_op_red = 0;
    dome_op_red = 0;
    shortcut_red = 0;
    flower_red = 0;
}

void Logs::print_log(string path) {
    ofstream output;
    output.open(path);
    if ( !output ) return;

    output << "reduce_graph\t\t\t\t\t" << reduce_graph << "\n";
    output << "in0\t\t\t\t\t\t\t\t" << in0 << "\n";
    output << "out0\t\t\t\t\t\t\t" << out0 << "\n";
    output << "loop\t\t\t\t\t\t\t" << loop << "\n";
    output << "in1\t\t\t\t\t\t\t\t" << in1 << "\n";
    output << "out1\t\t\t\t\t\t\t" << out1 << "\n";
    output << "pie_op\t\t\t\t\t\t\t" << pie_op << "\n";
    output << "core_op\t\t\t\t\t\t\t" << core_op << "\n";
    output << "dome_op\t\t\t\t\t\t\t" << dome_op << "\n";
    output << "shortcut\t\t\t\t\t\t" << shortcut << "\n";
    output << "flower\t\t\t\t\t\t\t" << flower << "\n";
    output << "counting_sort\t\t\t\t\t" << counting_sort << "\n";
    output << "calculate_pie\t\t\t\t\t" << calculate_pie << "\n";
    output << "calculate_notpie\t\t\t\t" << calculate_notpie << "\n";
    output << "calculate_pie_notpie\t\t\t" << calculate_pie_notpie << "\n";
    output << "is_clique\t\t\t\t\t\t" << is_clique << "\n";
    output << "isin\t\t\t\t\t\t\t" << isin << "\n";
    output << "isin_pos\t\t\t\t\t\t" << isin_pos << "\n";
    output << "remove_duplicates\t\t\t\t" << remove_duplicates << "\n";
    output << "lower_bound_matching\t\t\t" << lower_bound_matching << "\n";
    output << "path_finder\t\t\t\t\t\t" << path_finder << "\n";
    output << "has_unique_path\t\t\t\t\t" << has_unique_path << "\n";
    output << "has_k_paths\t\t\t\t\t\t" << has_k_paths << "\n";
    output << "getSCCcoloration\t\t\t\t" << getSCCcoloration << "\n";
    output << "delete_vertex\t\t\t\t\t" << delete_vertex << "\n";
    output << "delete_vertex_set\t\t\t\t" << delete_vertex_set << "\n";
    output << "merge_vertices\t\t\t\t\t" << merge_vertices << "\n";
    output << "merge_vertices_set\t\t\t\t" << merge_vertices_set << "\n";
    output << "replace_vertex\t\t\t\t\t" << replace_vertex << "\n";
    output << "solve\t\t\t\t\t\t\t" << solve << "\n";
    output << "solve_greedy\t\t\t\t\t" << solve_greedy << "\n";
    output << "get_branching_index\t\t\t\t" << get_branching_index << "\n";
    output << "get_branching_index_greedy\t\t" << get_branching_index_greedy << "\n";
    output << "append_vector\t\t\t\t\t" << append_vector << "\n";
    output << "is_acyclic\t\t\t\t\t\t" << is_acyclic << "\n";
    output << "in0_red\t" << in0_red << "\n";
    output << "out0_red\t" << out0_red << "\n";
    output << "loop_red\t" << loop_red << "\n";
    output << "in1_red\t" << in1_red << "\n";
    output << "out1_red\t" << out1_red << "\n";
    output << "pie_op_red\t" << pie_op_red << "\n";
    output << "core_op_red\t" << core_op_red << "\n";
    output << "dome_op_red\t" << dome_op_red << "\n";
    output << "shortcut_red\t" << shortcut_red << "\n";
    output << "flower_red\t" << flower_red << "\n";

    output.close();
}