//
// Created by Ruben Götz on 06.01.2022.
//

#ifndef DFVS_TEST_HELPER_H
#define DFVS_TEST_HELPER_H
#include "../src/Structs.cpp"
#include <vector>
#include <string>

using namespace std;


namespace Test_helper {
    int graph_equal(vector<vector<int>> &g1, vector<vector<int>> &g2);
    int instance_same(Graph_Instance i1, Graph_Instance i2);
    int coloration_equal(vector<int> &c1, vector<int> &c2);
    int coloration_is_normed(vector<int> &coloration);
    int vector_equal(vector<int> &v1, vector<int> &v2);
    int vector_same(vector<int> &v1, vector<int> &v2);
    int is_acyclic(Graph_Instance &instance);
    int is_solution(string solution_file, string graph_file);
    int is_solution(vector<int> &solution, string graph_file);
};


#endif //DFVS_TEST_HELPER_H
