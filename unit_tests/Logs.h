//
// Created by Ruben Götz on 27.04.2022.
//

#ifndef DFVS_LOGS_H
#define DFVS_LOGS_H
#include <string>
#include <fstream>

using namespace std;

class Logs {
public:
    static string bound_path;

    // counts method calls
    // reduce Graph
    static int reduce_graph;
    static int in0;
    static int out0;
    static int loop;
    static int in1;
    static int out1;
    static int pie_op;
    static int core_op;
    static int dome_op;
    static int shortcut;
    static int flower;
    static int counting_sort;
    static int calculate_pie;
    static int calculate_notpie;
    static int calculate_pie_notpie;
    static int is_clique;
    static int isin;
    static int isin_pos;
    static int remove_duplicates;
    static int lower_bound_matching;
    static int path_finder;
    static int has_unique_path;
    static int has_k_paths;

    // SCC
    static int getSCCcoloration;

    // Vertex_op
    static int delete_vertex;
    static int delete_vertex_set;
    static int merge_vertices;
    static int merge_vertices_set;
    static int replace_vertex;

    // B&B
    static int solve;
    static int solve_greedy;
    static int get_branching_index;
    static int get_branching_index_greedy;
    static int append_vector;
    static int is_acyclic;

    // counts successful reductions
    static int in0_red;
    static int out0_red;
    static int loop_red;
    static int in1_red;
    static int out1_red;
    static int pie_op_red;
    static int core_op_red;
    static int dome_op_red;
    static int shortcut_red;
    static int flower_red;


    // utility
    // file upper_bound_path has to be opened
    static void current_upper_bound_size(int size);
    static void open_bound_file();
    static void close_bound_file();
    static void reset();
    static void print_log(string path);

private:
    static ofstream bound_file;
};


#endif //DFVS_LOGS_H
