#include "catch.hpp"
#include "Test_helper.h"
#include "../src/Parser.h"
#include <fstream>
#include <vector>

using namespace std;
using namespace Parser;
using namespace Test_helper;

#define TEST_1 "../../unit_tests/test_graphs/g1"
#define TEST_2 "../../unit_tests/test_graphs/g1_with_comments"
#define TEST_3 "../../unit_tests/test_graphs/g1_with_too_small_n"
#define TEST_4 "../../unit_tests/test_graphs/g1_with_too_big_n"
#define TEST_5 "../../unit_tests/test_graphs/g1_with_wrong_e"
#define TEST_6 "../../unit_tests/test_graphs/g1_with_wrong_t"
#define TEST_7 "../../unit_tests/test_graphs/g1_with_uncommented_text"

#define OUTPUT_PATH "../../unit_tests/test_graphs/output"

#define TEST_SOLUTION_1 "../../unit_tests/test_graphs/test_solution_1"
#define TEST_SOLUTION_2 "../../unit_tests/test_graphs/test_solution_2"


TEST_CASE ( "extract_graph_test", "[Parser]" ) {
    vector<int> v0  = {1,4,5};
    vector<int> v1  = {};
    vector<int> v2  = {3,6};
    vector<int> v3  = {2,6,7};
    vector<int> v4  = {};
    vector<int> v5  = {2};
    vector<int> v6  = {9,10};
    vector<int> v7  = {16,18,11,10};
    vector<int> v8  = {4,9,13};
    vector<int> v9  = {8};
    vector<int> v10 = {11,14};
    vector<int> v11 = {15};
    vector<int> v12 = {};
    vector<int> v13 = {10};
    vector<int> v14 = {11,15};
    vector<int> v15 = {14};
    vector<int> v16 = {17,18};
    vector<int> v17 = {18};
    vector<int> v18 = {11};
    vector<vector<int>> g_control = {v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,
                                      v10,v11,v12,v13,v14,v15,v16,v17,v18};

    vector<int> map_control = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};

    Graph_Instance instance = Graph_Instance(0);

    SECTION ( "normal graph" ) {
        REQUIRE ( extract_graph(TEST_1,instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( 19 == instance.N );
    }

    SECTION ( "normal graph with comments" ) {
        REQUIRE ( extract_graph(TEST_2,instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( 19 == instance.N );
    }

    SECTION ( "graph with to small n in textfile" ) {
        REQUIRE ( !extract_graph(TEST_3,instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( 19 == instance.N );
    }

    SECTION ( "graph with to big n in textfile" ) {
        REQUIRE ( !extract_graph(TEST_4,instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( 19 == instance.N );
    }

    SECTION ( "graph with wrong e in textfile" ) {
        REQUIRE ( !extract_graph(TEST_5,instance) );
        REQUIRE ( graph_equal(instance.graph,g_control) );
        REQUIRE ( vector_equal(instance.map,map_control) );
        REQUIRE ( 19 == instance.N );
    }

    SECTION ( "graph with wrong t in textfile" ) {
        REQUIRE ( !extract_graph(TEST_6,instance) );
    }

    SECTION ( "graph with uncommented text in textfile" ) {
        REQUIRE ( !extract_graph(TEST_7,instance) );
    }
}

TEST_CASE ( "output_dfvs_test", "[Parser]" ) {
    // makes sure file does not exist already
    ifstream check;
    check.open(OUTPUT_PATH);
    if ( check ) {
        check.close();
        REQUIRE ( !remove(OUTPUT_PATH) );
    }

    SECTION ( "simple case" ) {
        vector<int> dfvs = {1,2,5,78};
        vector<int> dfvs_control(0);

        REQUIRE ( Parser::output_dfvs(OUTPUT_PATH,dfvs) );
        REQUIRE ( !Parser::output_dfvs(OUTPUT_PATH,dfvs) );

        Parser::get_solution_file(OUTPUT_PATH,dfvs_control);
        REQUIRE ( Test_helper::vector_equal(dfvs,dfvs_control) );
    }

    SECTION ( "empty dfvs" ) {
        vector<int> dfvs = {};
        vector<int> dfvs_control(0);

        REQUIRE( Parser::output_dfvs(OUTPUT_PATH,dfvs) );
        REQUIRE( !Parser::output_dfvs(OUTPUT_PATH,dfvs) );

        Parser::get_solution_file(OUTPUT_PATH,dfvs_control);
        REQUIRE ( Test_helper::vector_equal(dfvs,dfvs_control) );
    }
}

TEST_CASE ( "get_solution_file_test", "[Parser]" ) {
    vector<int> solution;
    vector<int> solution_control = {3,4,15,75,33,2};

    SECTION ( "with blanc line" ) {
        Parser::get_solution_file(TEST_SOLUTION_1,solution);

        REQUIRE ( Test_helper::vector_equal(solution,solution_control) );
    }

    SECTION ( "with blanc line" ) {
        Parser::get_solution_file(TEST_SOLUTION_2,solution);

        REQUIRE ( Test_helper::vector_equal(solution,solution_control) );
    }
}