add_executable( tests main_test.cpp
                Vertex_op_test.cpp
                SCC_compute_test.cpp
                Graph_reduce_test.cpp
                Parser_test.cpp
                Test_helper.cpp
                Test_helper.h
                Branch_and_bound_test.cpp
                Benchmarks.cpp
                Logs.cpp
                Logs.h
                Logs_test.cpp)
target_link_libraries(tests dfvs_lib)