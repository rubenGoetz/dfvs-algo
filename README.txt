///////////////////////
// Brief Information //
///////////////////////

This repository contains a solver for the Directed Feedback Vertex Set Problem. The solver is part of a bachelor thesis as well as a submission 
to the PACE Challenge 2022 (https://pacechallenge.org/2022/).

The solver follows a branch&bound approach and reduces the graph periodically.

//////////////////////////////
// Installation Description //
//////////////////////////////

Option 1 - CMake
	1. Run CMake in the root directory
	2. Run the created make file
	
	The executable will be called "dfvs".
	
Option 2 - without CMake
	Buid the file src/solver.cpp directly with g++. src/solver.cpp contains all the relevant code to build the solver in one file.

Both options will create an executable. If executed it expects a graph as described here: https://pacechallenge.org/2022/tracks/
After the execution is finished it will print out a Directed Feedback Vertex Set.

//////////////////
// Requirements //
//////////////////

	- C++ version 17 or higher
	- CMake version 3.16 or higher for option 1